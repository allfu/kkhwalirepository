<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="ME">
    <title>AFU</title>
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url().'assets/backend/img/ico/apple-icon-60.png'; ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url().'assets/backend/img/ico/apple-icon-76.png'; ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url().'assets/backend/img/ico/apple-icon-120.png'; ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url().'assets/backend/img/ico/apple-icon-152.png'; ?>">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url().'assets/backend/img/ico/favicon.ico'; ?>">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url().'assets/backend/img/ico/favicon-32.png'; ?>">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/css/bootstrap.css'; ?>">
    
    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/fonts/icomoon.css'; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/css/flag-icon.min.css'; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/vendors/css/extensions/pace.css'; ?>">
    <!-- END VENDOR CSS-->
    
    <!-- BEGIN ROBUST CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/css/bootstrap-extended.css'; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/css/app.css'; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/css/colors.css'; ?>">
    <!-- END ROBUST CSS-->
      
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/css/core/menu/menu-types/vertical-menu.css'; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/css/core/menu/menu-types/vertical-overlay-menu.css'; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/css/core/colors/palette-gradient.css'; ?>">
    <!-- END Page Level CSS-->
    
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/css/style.css'; ?>">
    <!-- END Custom CSS-->
    
    <!--jquery-->
    <script src="<?php echo base_url().'assets/backend/js/core/libraries/jquery.min.js'; ?>" type="text/javascript"></script>
      
    <!--jquery for autocomplete-->
    <link  rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/autocompleteassets/jquery.ui.css"/>
    <script type="text/javascript" src="<?php echo base_url();?>assets/autocompleteassets/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/autocompleteassets/jquery.ui.js"></script>
    
    <!--Bootstrap js-->
    <script src="<?php echo base_url().'assets/backend/js/core/libraries/bootstrap.min.js'; ?>" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.10.1/sweetalert2.all.min.js" type="text/javascript"></script>
      
  </head>
      <body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column  blank-page blank-page">

 <!-- ////////////////////////////////////////////////////////////////////////////-->
    <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><section class="flexbox-container">
    <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1">
    <div class="card-header bg-transparent no-border pb-0">
        <h2 class="error-code text-xs-center mb-2">404</h2>
        <h3 class="text-uppercase text-xs-center">Page Not Found !</h3>
    </div>
    <div class="card-body collapse in">
        <fieldset class="row py-2">
            <div class="input-group col-xs-12">
                <input type="text" class="form-control form-control-lg input-lg border-grey border-lighten-1" placeholder="Search..." aria-describedby="button-addon2">
                <span class="input-group-btn" id="button-addon2">
					<button class="btn btn-lg btn-secondary border-grey border-lighten-1" type="button"><i class="icon-ios-search-strong"></i></button>
				</span>
            </div>
        </fieldset>
        <div class="row py-2">
            <div class="col-xs-12 col-sm-6 col-md-6">
                <a href="<?php echo base_url().'admin';?>" class="btn btn-primary btn-block font-small-3"><i class="icon-home4"></i> Back to Home</a>
            </div>
            
            </div>
        </div>

    </div>
</section>

        </div>
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->
