
<style>
	.logo-blogrow {
		margin-right: -25px;
        margin-left: -25px;
		margin-top: 60px;
		margin-bottom: 20px;
	}
	.logo-blogcol {
		padding-right: 0px;
        padding-left: 0px;
		border-bottom: 1px solid #8c8c8c;
	}
	.logo-blogcolimg {
		padding-right: 0px;
		padding-left: 0px;
	}
	.logoimg-sec {
		border-radius: 50px; 
		padding: 10px;
		width: 100%;
		border: 1px solid #8c8c8c;
		margin-top: -40px;	
	}
	.content-blog {
		text-align: center;
		height: 50px;
	}
	.btn-join {
		border-radius: 30px;
		padding: 10px 30px;
	}
	.modal-title {
		text-align: center
	}
	.blogLocation {
		margin-top: 5px;
		font-size: 14px;
	}
	h4 {
		text-align: center;
		font-weight:bold;
	}
	h5 {
		text-align: center;
		text-decoration: underline;
	}
	.modal-header .close {
		margin-top: -32px;
		background-color: #000;
		padding: 3px 10px;
		border-radius: 50px;
		margin-right: -29px;
		opacity: 1;
	}
	button.close {
		-webkit-appearance: none;
		padding: 0px 15px;
		cursor: pointer;
		background: 0 0;
			background-color: rgba(0, 0, 0, 0);
		border: 0;
	}
	.close {
		float: right;
		font-size: 26px;
		font-weight: 700;
		line-height: 1;
		color: #fff;
		text-shadow: 0 1px 0 #fff;
		filter: alpha(opacity=20);
		opacity: .2;
	}
	button, input[type="submit"] {
		height: 1.25000000000001em;
	}
	.close:focus, .close:hover {
		color: #e6e6e6;
		text-decoration: none;
		cursor: pointer;
		filter: alpha(opacity=50);
		opacity: .5;
	}
	@media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
		.pricing pricing-1 boxed boxed--lg boxed--border {
			min-height:500px;
		}
	}
	@media only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : portrait) and (-webkit-min-device-pixel-ratio : 2) {
		.pricing pricing-1 boxed boxed--lg boxed--border {
			min-height:1000px;
		}
	}
   
	@media only screen  and (min-width : 1224px) {
		.pricing pricing-1 boxed boxed--lg boxed--border {
			min-height:1000px;
		}
	}
	.main-sec:hover {
		cursor : pointer;
	}
   .pricing {
		max-height: 550px;
	}
</style>

<!--<style>
	.container-sec {
	  position: relative;
	}

	.image {
	  display: block;
	  width: 100%;
	  height: auto;
	}

	.overlay {
	  position: absolute;
	  top: 0;
	  bottom: 0;
	  left: 0;
	  right: 0;
	  height: 100%;
	  width: 100%;
	  opacity: 0;
	  transition: .5s ease;
	  background-color: #e6e6e6;
	}

	.container-sec:hover .overlay {
	  opacity: 1;
	}

	.text {
	  color: white;
	  font-size: 18px;
	  position: absolute;
	  top: 50%;
	  left: 50%;
	  transform: translate(-50%, -50%);
	  -ms-transform: translate(-50%, -50%);
	}
</style>-->


<body class=" ">

<div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <ul class="nav nav-pills nav-stacked admin-menu" >
                    <li ><a href="<?php echo base_url().'leader/leader_home';?>"  data-target="manage-event">My Events</a></li>
                    <li class="active"><a href="<?php echo base_url().'leader/my_groups';?>"  data-target="manage-group"><!--<i class="glyphicon glyphicon-lock"></i>-->My Groups</a></li>
                    <!--<li><a href="<?php echo base_url().'leader/my_groups';?>"  data-target="manage-group">Group events</a></li>-->                   
                </ul>
            </div>

            <div class="col-md-9  admin-content" id="manage-event" >
                 <!--<div class="main-container">-->
            <?php ?>
            <section class="switchable feature-large bg--secondary">
                
				<?php //foreach();
				//echo "groups";?>
				
				<div class="row">
					<div class="col-md-2">
						<h4>Join Groups</h4>
					</div>
					<div class="col-md-8">
						<a class="btn btn-primary" style="float:right" href="<?php echo base_url().'leader/create_group';?>">
							<span class="btn__text">Create new group</span>
						</a>
					</div>
				</div>
				
				<br/>
				
					<hr class="style-two">
					
					<div class="row main-sec" data-toggle="modal" data-target="#myModal">
						
							<?php foreach($allgrps as $row){
								$newclubs = null;															
								?>
								
									<div class="col-sm-4 col-sm-6" style="float; inherit;">
									
										 <div class="pricing pricing-1 boxed boxed--lg boxed--border container-sec">
										 <div class="image">
											<?php  print_r($newclubs);
											//print_r($row);
												  //$q=$this->db->get_where('club_table',array('c_id'=> $row->c_id));
												  //$gname = $q->row()->c_name;?>
											 <div style="text-align:center">
											 
											 <?php 
											//$q=$this->db->get_where('club_table',array('c_id'=> $row->gclubid));
									if($row->c_logo != NULL){
												?>
												
											<img src="<?php echo base_url().$row->c_logo;?>" style="height:100px;width:150px">
											
											<?php } 
														else{?>
												
											<img src="<?php echo base_url().'assets/frontend/img/clublogos/default_logo.png';?>" style="height:100px;width:150px">
											
												<?php } ?>
											 
											 
											 
											 
												<!--<img src="<?php echo base_url().'assets/frontend/img/upload_image.png';?>" style="height:100px;width:150px">-->
											 </div>
											
											  <div class="row logo-blogrow hidden-xs">
													<div class="col-md-4 col-sm-4 logo-blogcol"></div>																										
													<div class="col-md-4 col-sm-4 col-xs-4 logo-blogcolimg">
														 <div class="logoimg-sec">
															<img src="<?php echo base_url().'assets/frontend/img/upload_image.png';?>">
														 </div>
													</div>
													<div class="col-md-4 col-sm-4 col-xs-4 logo-blogcol"></div>																								
											 </div>
											  <div class="row content-blog">
													<div class="col-md-12">
														<!-- <div>
															<?php 
																$q3=$this->db->get_where('university',array('u_id'=> $row->c_uni));
																$uni_name = $q3->row()->u_name;	
																
															?>
														</div> -->
														<div>
															
															<label style="font-weight:bold"><?php echo $row->c_name;?></label>
														</div>
														<div>
													<!--	<label style="font-weight:bold"><?php echo $uni_name;?></label>-->
															<label ><?php echo $uni_name;?></label>
														</div>
													</div>
											  </div>
										<br/>
										<br/>
										  <div class="row">
												<div class="col-md-12">
													<?php 							
														$this->db->where('gclubid',$row->c_id);
														$this->db->where('gstudid',$uid);
														$q1=$this->db->get_where('group_table');
														$newclubs = $q1->result();
														//print_r($newclubs);
														if($newclubs){?>
														<div style="text-align:center">
															<a class="btn btn-info btn-join" href="" >														
																<span class="btn__text">Joined</span>
															</a>
														</div>
													 
														<?php } 
														else{?>
														<div style="text-align:center">
															<a class="btn btn-warning btn-join" href="<?php echo base_url().'leader/join_newgroup/'.$row->c_id ;?>" >														
																<span class="btn__text">Join group</span>
														    </a>
														</div>
														
													<?php } ?>
												</div>
										  </div>
										 <br/>
										 <div class="row">
											<div class="col-md-12">
												<div style="text-align:center">
													<a href="javascript:void(0)">View More Details</a>
												</div>												
											</div>
										 </div>
										 
									 </div>	
								<!--	<div class="overlay">
										<div class="text">View More Details</div>
									</div>-->
								</div>		
								</div>
							<?php }?>						
					</div>				
            </section>	
		</div>
		<!-- Modal -->
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					  <div class="modal-dialog" role="document">
						  <div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="height:1.30em!important"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">
								 <div>School Of Physical And Mathematical Sciences Club </div>
								 <div class="blogLocation">Nanyang Technological University</div>							 
							 </h4>
						  </div>
						  <div class="modal-body">
							 <div class="row">
								<div class="col-md-12">
									<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
										  <!-- Indicators -->
										 

										  <!-- Wrapper for slides -->
										  <div class="carousel-inner" role="listbox">
												<div class="item active">
												  <img src="<?php echo base_url().'assets/frontend/img/slider1.jpg';?>" style="width:100%;height: 250px;margin-bottom:0">
												 
												</div>
											<div class="item">
												   <img src="<?php echo base_url().'assets/frontend/img/slider2.jpg';?>" style="width:100%;height: 250px;;margin-bottom:0">
												 
											</div>
											
										  </div>

										  <!-- Controls -->
										  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
											<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
											<span class="sr-only">Previous</span>
										  </a>
										  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
											<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
											<span class="sr-only">Next</span>
										  </a>
									</div>
								</div>
							 </div>
							 <div class="row">
								    <div class="col-md-5 col-xs-3 col-sm-4"></div>																										
										<div class="col-md-2 col-xs-6 col-sm-4">
											 <div class="logoimg-sec" style="border: 2px solid #a6a6a6;">
												    <img src="<?php echo base_url().'assets/frontend/img/upload_image.png';?>">
											 </div> 
										</div>
									<div class="col-md-5 col-xs-3 col-sm-4"></div>																								
							 </div>
							 <br/>
							 <div class="row">
								<div class="col-md-6">
									<h4>About Our Club</h4>
									<p>We are a forward looking club that put
										ourselves at a higher calling. We love
										science, physics, biology and all kind of
										math. Science is good for knowledge
										and it propagates a healthy civilisation
										of progressive species. Come join us
										and propagate the next wave of
										leaders.
										2000 members
									</p>
								</div>
								<div class="col-md-6">
									<h4>Past Activities</h4>
									<div class="row">
										<div class="col-md-6">
											<div><img class="img-thumbnail" src="<?php echo base_url().'assets/frontend/img/upload_image.png';?>"></div>	
											<div>
												<h5>FOC Camp</h5>
											</div>
											<div>
												<p> Here is where
													freshman bond
													and make Friends
												</p>
											</div>
										</div>
										<div class="col-md-6">
											<div><img class="img-thumbnail" src="<?php echo base_url().'assets/frontend/img/upload_image.png';?>"></div>	
											<div>
												<h5>FOC Camp</h5>
											</div>
											<div>
												<p> Here is where
													freshman bond
													and make Friends
												</p>
											</div>
										</div>
									</div>
								</div>
							 </div>
							 <br/>
							 <br/>
							 <div class="row">
								<div class="col-md-offset-2 col-md-8">
									<div style="border-bottom: 5px solid #8080ff"></div>
								</div>
							 </div>
							  <div class="row">
								<div class="col-md-12">
									<h4 style="text-align:center">Upcoming Activities</h4>
								</div>
							 </div>
							 <br/>
							  <div class="row">
								<div class="col-md-3">
									 <img class="img-thumbnail" src="<?php echo base_url().'assets/frontend/img/upload_image.png';?>">
								</div>
								<div class="col-md-9">
									<p>Dodgeball 2017 Championship</p>
								</div>
							  </div>
							  <br/>
							  <div class="row">
								<div class="col-md-12">
									<div style="text-align:center">
										<a class="btn btn-info btn-join" href="" >														
											<span class="btn__text">Be one of us</span>
										</a>
									</div>
								</div>
							  </div>
						  </div>
						 
						</div>
					  </div>
				</div>
    </div>
</div>
		 