
<body class=" ">

<div class="container-fluid">
  
        <div class="row">

            <div class="col-md-3">
                <ul class="nav nav-pills nav-stacked admin-menu" >
                    <li ><a href="<?php echo base_url().'leader/leader_home';?>"  data-target="manage-event">My Events</a></li>
                    <li class="active"><a href="<?php echo base_url().'leader/my_groups';?>"  data-target="manage-group">My Groups</a></li>
                    <!--<li><a href="<?php echo base_url().'leader/my_groups';?>"  data-target="manage-group">Group events</a></li>-->
                </ul>
            </div>
       <div class="col-md-9  admin-content" id="manage-event" >
                 <!--<div class="main-container">-->
            <?php ?>
            <section class="switchable feature-large bg--secondary">
                
				<a class="btn btn--primary" href="<?php echo base_url().'leader/join_group';?>" style="margin-top: -15%; margin-left: 5%;">
					<span class="btn btn-info">Join new group</span>
				</a>
	
                    <div class="row"  style="padding:10px; margin-left: 10px;margin-right: 10px;">
					<h4>Create New Group</h4>
						 <div class=" boxed boxed--lg boxed--border" style="">
						 <form method="post" action="" id="imageform" name="groupcreate_form">
                            <!--<hr>-->							
							<div class="form-group">
							<label>School Name:</label>
                            <select required class="form-control" name="c_uni" id="c_uni">
							<option value="none" selected="" disabled=""></option>
							   <?php $this->db->where('permit !=',0);
									 $query = $this->db->get('university');
									 foreach($query->result() as $row){
							   ?>
							   <option value="<?php echo $row->u_id;?>"><?php echo $row->u_name;?></option>
									 <?php } ?>
							</select>
							<p id="school_name_error" style="color:red;"></p>
							</div>
							<div class="form-group">
								<label>Group Name:</label>
								<input type="text" class="form-control" name="c_name" value="<?php echo set_value('c_name');?>" id="c_name" />
								<p id="group_name_error" style="color:red;"></p>
							</div>
							
							<div class="form-group">
								<label>Group Description:</label>
								<!--<input type="text-area" class="form-control" name="c_desc" value="<?php echo set_value('c_desc');?>" id="c_desc" wrap="soft"/>-->
								<textarea id="c_desc" name="c_desc" class="form-control"></textarea>
								<p id="group_desc_error" style="color:red;"></p>
							</div>
							<div class="form-group">
								<label>Group Logo:</label>
								<input type='file' class="images"  id="file-input" name="images[]" multiple />
								<div class="gallery"></div>
								<p id="group_logo_error" style="color:red;"></p>
							</div>
							<input type="button" name="sub" id="submit" class="btn btn-info pull-right" value="Save and Join" style="">
						 </form>
						 <div id="imgprv" style="display:none">
						 <table>
						 <tr>
							<?php 
								$this->load->model('mdl_leader');
								$id=$this->session->userdata('lastid');
								$query = $this->db->where('club_id',$id)->get('slider_images');  
								 foreach($query->result() as $row)
								 {
									 
									 ?>
									 <td><a href="<?=$row->id;?>"><img src="<?=base_url().$row->images;?>"/></a></td>
									 <?php
									 
								 }
							?>
							</tr>
						 </table>
						 </div>
							 <div class="form-group">
							 <label>Manage Members:</label>
							 <table class="table table-striped">
							  <thead>
								<tr>
								  <th scope="col">Student Name</th>
								  <th scope="col">Matriculation No.</th>
								  <th scope="col">Faculty</th>
								  <th scope="col">Hall</th>
								  <th scope="col">Roles</th>
								  <th scope="col">Action</th>
								</tr>
							  </thead>
							  <tbody>
								<tr>
								<th scope="row">Sam</th>
								  <td>12475</td>
								  <td>Science</td>
								  <td>(11)Hall Of Residence </td>
								  <td>Member</td>
								  <td>
								   <div class="row">
								   <div class="col-sm-6">
								   <input type="button" class="btn btn-info" value="Remove" style="color:#000000;">
								  </div>
								  <div class="col-sm-6">
								   <input type="button" class="btn btn-danger" value="Assign Admin" style="color:#000000;">
								  </div>
								   </div>
								  </td>
								</tr>
								
									</tbody>
									</table>
							</div>
							
                         </div>
                               
					</div>
                    <!--end of row-->
              
                <!--</div>*-->
                <!--end of container-->
            </section>
			              
            </div>
  </div>
  </div>
<!--ADD GROUP SCRIPT-->
 <script>
    $(document).ready(function()  {
        $("#submit").click(function(e)  {
            e.preventDefault();
           // alert('ajax call');
          var form = $('#imageform')[0]; // You need to use standart javascript object here
          var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'leader/image_submit'; ?>",
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        var parsed = JSON.parse(result);
                        console.log(parsed);
                        if(parsed.status_code == 1) {
                             swal({
                                html: parsed.status,
                                type: 'success',
                                    }).then(function() {
                                         window.location = "<?php echo base_url().'leader/my_groups'; ?>";
                                    });
                        } else  {
                            swal({
                                html: parsed.status,
                                type: 'error'
                            });
                        }
					//$("#imgprv").show();
                    }
                });
        });
    });
     </script>
    <!-- <script>
	$('#sub').click(function(e) {
        e.preventDefault(); // <------this will restrict the page refresh
        /*var form_data = {
            grpuni: $('#group_uni').val(),
            grpname: $('#group_name').val(),            
            grpdesc: $('#group_desc').val(),            
        };*/
		var form = $('#groupcreate_form');
            var formData = new FormData(form[0]);
            console.log(formData);
		
        //alert(form_data.e_name);
        $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + 'leader/group_submit',
                data: formData,
				processData: false,
				contentType: false,
                success: function(res)  {
                    var json = JSON.parse(res);
                    console.log(json);
                     if(json.st== 'fail'){
                        $('#school_name_error').html(json.c_uni);
                        $('#group_name_error').html(json.c_name);
                        $('#group_desc_error').html(json.c_desc);
                                                     
                    }
					 if(json.st_code== 3){
						 swal(
						  '',
						  'File type not allowed!',
						  'error'
						)
					 }
                   
                   if(json.st =='success'){
                        
                        //alert('Group added successfully');
						swal(
						  '',
						  'Group added successfully!',
						  'success'
						)
                        window.location.href = "<?php echo site_url('leader/my_groups'); ?>";
                    }
                   
                }
        });
    });
</script>-->
<script>
     $(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img class="gallery-image">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    $('.gallery-image').attr({ width: '110px', height: '100px' });
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#file-input').on('change', function() {
        imagesPreview(this, 'div.gallery');
    });
});
</script>