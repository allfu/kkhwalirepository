
<body class=" ">

<div class="container-fluid">
  
        <div class="row">

            <div class="col-md-3">
                <ul class="nav nav-pills nav-stacked admin-menu" >
                    <li class="active"><a href="<?php echo base_url().'leader/leader_home';?>"><!--<i class="glyphicon glyphicon-user"></i>-->My Events</a></li>
                    <li><a href="<?php echo base_url().'leader/my_groups';?>"  ><!--<i class="glyphicon glyphicon-lock"></i>-->My Groups</a></li>
                    <!--<li><a href="<?php echo base_url().'leader/my_groups';?>" ><!--<i class="glyphicon glyphicon-lock"></i>--Group events</a></li>-->
                </ul>
            </div>

            <div class="col-md-9  admin-content" id="manage-event" >
                 <!--<div class="main-container">--> 
            <section class="switchable feature-large bg--secondary">
			<div class="row">
				<div class="col-md-offset-8 col-md-2">
					<a class="btn btn-default" href="<?php echo base_url().'leader/create_event';?>">
						<span class="btn__text">Create new event</span>
				   </a>
				</div>
				<div class="col-md-2">
					<a class="btn btn-default" href="<?php echo base_url().'leader/leader_sponsors';?>">
						<span class="btn__text">Select Sponsors</span>
				    </a>
				</div>
			</div>
			<div class="row" style="margin-bottom: 10px; margin-right: 2px;">
			<div class="col-md-12">
	      		
				
				</div>
				</div>
                <!--<div class="container" style="margin-top:-10px;" >-->
				
				<?php foreach($events as $row){
					
					?>
						
                    <div class="row"  style="padding:10px; margin-left: 5px;margin-right: 5px; border: 2px solid gray;">
                        <div class="col-sm-6" style="border-right: 2px solid gray;">
							<?php if(isset($row->e_img)){?>
                            <img alt="Image" class="border--round" src="<?php echo base_url().$row->e_img;?>" />
							<?php } 
							else{?>
                            <img alt="Image" class="border--round" src="<?php echo base_url().'assets/frontend/';?>img/xyz.jpg" />
							<?php } ?>
							<h4 style="color:#000000; font:bold; "> <b>Your Club Have Worked Hard For Your Welfare
                                 Do Help Them Achieve These Objectives</b> </h4>
								 <br>
	                  <div class="progress">
				    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
					  <span class="sr-only">60% Complete</span>
					  <span class="progress-type">81 of 120 Facebook</span>
				    </div>
					</div>
                    <br>
					<div class="progress">
				    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
					  <span class="sr-only">60% Complete</span>
					  <span class="progress-type">81 of 120 Instagram</span>
				    </div>
					</div>
                    <br>
					<div class="progress">
				    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
					  <span class="sr-only">60% Complete</span>
					  <span class="progress-type">101 of 120 Facebook</span>
				    </div>
					</div>
				</div>
                        <div class="col-sm-6 col-md-5">
						<?php if($row->publish==0){?>
						<a class="btn btn-success" href="<?php echo base_url().'leader/publish_event/'.$row->e_id;?>"> Publish</a>
						

						<?php } 
						else {?>
						<a class="btn btn-warning" href="<?php echo base_url().'leader/unpublish_event/'.$row->e_id;?>"> Published</a>
						<?php } ?>
                            <div class="switchable__text">
							<center>
							<?php if(isset($row->e_logo)){?>
                            <img alt="Image" class="border--round" src="<?php echo base_url().$row->e_logo;?>" height="90" width="90"/>
							<?php } 
							else{?>
                            <img alt="Image" class="border--round" src="<?php echo base_url().'assets/frontend/';?>img/upload_image.png" height="90" width="90"/>
							<?php } ?>
							
							
							
                          <!--  <img alt="Club logo" height="90" width="90" class="border--round" src="<?php echo base_url().'assets/frontend/';?>img/upload_image.png" />-->
								<h4 style="color:#00aeff;">120 out of 155 Attending</h4>
								<h4><b><?php echo $row->e_name;?></b></h4>
								<p><?php $q=$this->db->get_where('club_table',array('c_id'=> $row->e_by));
								  $gname = $q->row()->c_name;
								  echo $gname;?>
                                    <br><br>
									<?php echo $row->e_desc;?>
                                </p>
								<br>
								<?php if(isset($row->e_start)){?>
								<p><?php
								echo date("l F jS, Y g:i a", strtotime($row->e_start));
								//echo "<br>".$row->e_start;?></p>
								<?php }?>
								<div class="row">
								<div class="col-md-6">
							
								<p style="color:black;"><b>
								<?php if(isset($row->e_type)){echo $row->e_type;} 
								else{echo "Welfare";}?>
								</b></p>
								Event 
								<br><br>
								<a class="btn btn-info" href="<?php echo base_url().'leader/edit_event/'.$row->e_id;?>"> Edit</a>
								</div>
								<div class="col-md-6">
								<p style="color:black;"><b>
								<?php if(isset($row->e_loc)){echo $row->e_loc;} 
								else{echo "Atrium A";}?>
								</b></p>
								Location
								<br><br>
								<a class="btn btn-danger" href="<?php echo base_url().'leader/delete_event/'.$row->e_id;?>"> Delete</a>
								</div>
								</div>
                                
							</center>
 
                            </div>
                        </div>
                    </div>
					<br><br><br>
				<?php } ?>
                    <!--end of row-->
              
                <!--</div>*-->
                <!--end of container-->
            </section>
           
            </div>
  </div>
          </div>

          
               
           

