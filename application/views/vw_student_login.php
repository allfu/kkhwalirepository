<!doctype html>
<html lang="en">
<?php
if (isset($this->session->userdata['logged_in'])) {

//header("location: http://localhost/login/index.php/user_authentication/user_login_process");
}
?>
    <head>
        <meta charset="utf-8">
        <title>AFU</title>
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link href="<?php echo base_url().'assets/frontend/';?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/theme.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i%7CMaterial+Icons" rel="stylesheet">
    </head>
     <body class=" ">
	 <?php
if (isset($logout_message)) {
echo "<div class='message'>";
echo $logout_message;
echo "</div>";
}
?>
<?php
if (isset($message_display)) {
echo "<div class='message'>";
echo $message_display;
echo "</div>";
}
?>
       
       
            <!--end bar-->
            <nav id="menu1" class="bar bar--sm bar-1 hidden-xs bar--transparent bar--absolute">
                <div class="container">
                    <div class="row">
                        <div class="col-md-1 col-sm-2 hidden-xs">
                            <div class="bar__module">
                                <a href="index.html">
                                    <img class="logo logo-dark" alt="AFU" src="<?php echo base_url().'assets/frontend/';?>img/logo-dark.png" />
                                    <!--<img class="logo logo-light" alt="logo" src="img/logo-light.png" />-->
                                </a>
                            </div>
                            <!--end module-->
                        </div>
                                            </div>
                                                    <!--end dropdown content-->
                                                </div>
                                                <!--end row-->
                                         
                                        
                                        <!--end dropdown container-->
                                    
                                   
                                
                          
                            <!--end module-->
</nav>        
      
        <div class="main-container">
            <section class="height-100 imagebg text-center" data-overlay="4">
                <div class="background-image-holder">
                    <img alt="background" src="<?php echo base_url().'assets/frontend/';?>img/inner-6.jpg" />
                </div>
                <div class="container pos-vertical-center">
                    <div class="row">
                        <div class="col-sm-7 col-md-5">
                             <h2> Student Login </h2>
                            <a href="<?php echo base_url()."hauth/login/Facebook"; ?>">
                                                                    
                                   <img src="<?php echo base_url().'assets/frontend/';?>img/fb-button-connect.png" style="height: 70px;width: 67%;">
                                
                            </a>
                             
                            <hr>
                     </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
       
		
        </div>
		 </div>
        <!--<div class="loader"></div>-->
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
            <i class="stack-interface stack-up-open-big"></i>
        </a>
        <script src="<?php echo base_url().'assets/frontend/';?>js/jquery-3.1.1.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/flickity.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/easypiechart.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/parallax.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/typed.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/datepicker.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/isotope.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/ytplayer.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/lightbox.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/granim.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/jquery.steps.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/countdown.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/twitterfetcher.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/spectragram.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/smooth-scroll.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/scripts.js"></script>
    </body>
</html>