
<body class=" "  onload="javascriptfunction();">

<div class="container-fluid">
  
        <div class="row">

            <div class="col-md-3">
                <ul class="nav nav-pills nav-stacked admin-menu" >
                    <li ><a href="<?php echo base_url().'leader/leader_home';?>"  data-target="manage-event">My Events</a></li>
                    <li class="active"><a href="<?php echo base_url().'leader/my_groups';?>"  data-target="manage-group">My Groups</a></li>
                    <!--<li><a href="<?php echo base_url().'leader/my_groups';?>"  data-target="manage-group">Group events</a></li>-->
                </ul>
            </div>

            <div class="col-md-9  admin-content" id="manage-event" >
                 <!--<div class="main-container">-->
            <?php ?>
            <section class="switchable feature-large bg--secondary">
                
			<!--	<a class="btn btn--primary" href="<?php echo base_url().'leader/join_group';?>" style="margin-top: -15%; margin-left: 5%;">
					<span class="btn__text">Join new group</span>-->
				</a>
	                    <div class="row"  style="padding:10px; margin-left: 10px;margin-right: 10px;">
					<h4>Edit New Group</h4>
						 <div class=" boxed boxed--lg boxed--border" style="">
						 <form method="post" action="" id="groupedit_form">
						 <input type="hidden" id="groupid" name="groupid" value="<?php echo $groupdata;?>"/>
                            <!--<hr>-->		
							<div class="form-group">
							<label>Group Name:</label>
							<?php $query1 = $this->db->get_where('club_table',array('c_id'=>$groupdata)); ?>
							<input type="text" class="form-control" id="c_name"  name="c_name" value="<?php echo $query1->row()->c_name;?>" required/>
							<p id="group_name_error" style="color:red;"></p>
							</div>	
							
							<div class="form-group">
							<label>School Name:</label>
							<?php $query2 = $this->db->get_where('university',array('u_id'=>$unidata)); ?>
                            <select required class="form-control" name="c_uni" id="c_uni" value="<?php echo $query2->row()->u_name;?>">
							<option value="<?php echo $query2->row()->u_id;?>" selected="" ><?php echo $query2->row()->u_name;?></option>
							   <?php $this->db->where('permit !=',0);
									 $query = $this->db->get('university');
									 foreach($query->result() as $row){ 
							   ?>
							   <option value="<?php echo $row->u_id;?>"><?php echo $row->u_name;?></option>
									 <?php } ?>
							</select>
							<p id="school_name_error" style="color:red;"></p>
							</div>
							
							<div class="form-group">
							<label>Group Description:</label>
							<?php //$query3 = $this->db->get_where('club_table',array('c_id'=>$groupdata)); ?>
                            <input type="text-area" class="form-control" name="c_desc" value="<?php echo $query1->row()->c_desc;?>" id="c_desc" 
								 wrap="soft"/>
							<p id="group_desc_error" style="color:red;"></p>
							</div>
							
							<div class="form-group">
							<label>Group Logo:</label>
							<input type="hidden" id="logo1" name="logo1" value="<?php echo $query1->row()->c_logo;?>">
							<input type='file' onchange="readURL(this);" id="logo" name="logo" /><br>
							<img id="blah" src="<?php echo base_url().$query1->row()->c_logo;?>" alt="Your logo" 
								 style="height:150px; width:150px; min-height:90px; min-width:90px; border:2px solid gray;"/>
								 
							<p id="group_logo_error" style="color:red;"></p>
							</div>
							<label>Manage Members:</label>
							<div style="border: 1px solid gray;" class="table-responsive">
							<table id="member_table" class="table table-bordered table-striped table-hover">
								<thead>
								<tr>
								<th>Student Name</th>
								<th>Matriculation No.</th>
								<th>Faculty</th>
								<th>Hall</th>
								<th>Roles</th>
								<th>Action</th>
								</tr>
								</thead>
								<tbody>
								<?php foreach($members as $stud){
									//print_r( $stud);
									$query1 = $this->db->get_where('user_table',array('id'=>$stud->gstudid));
									$user = $query1->row();
									?>
								     <tr>
								<td><?php echo $user->user_name;?></td>
								<td><?php echo $user->matno;?></td>
								<td><?php echo $user->faculty;$query2 = $this->db->get_where('faculty_table',array('f_id'=>$user->faculty));
										  echo $query2->row()->f_name;?></td>
								<td><?php $query3 = $this->db->get_where('hall_table',array('h_id'=>$user->hall_name));
											echo $query3->row()->h_name;?></td>
								<td><?php if($stud->type==1){ echo "Member";} else {echo "Admin";}?></td>
								<td>
								<div class="row">
								<div class="col-md-6">
								<?php if($stud->type==1){ ?>
										<a  class="btn btn-success" name="remove" id="remove" style="color:#000000;" 
										href="<?php echo base_url().'leader/remove_member/'.$stud->gid.'/'.$groupdata.'/'.$unidata;?>">Remove</a>
								</div>
							 <div class="col-md-6">
							 <a  class="btn btn-warning" name="admin" id="admin" style="color:#000000;"
										 href="<?php echo base_url().'leader/add_groupadmin/'.$stud->gid.'/'.$groupdata.'/'.$unidata;?>">
										Assign 
										</a>
								<?php } else {
									echo "Admin";
									}?>
							 </div>
							    </div>
								</td>
								 </tr>
								<?php }?>
								</tbody>
							</table>
							</div>
							<input type="submit" class="btn btn-info" value="Save and Update" style="width:150px;">
						 </form>
                         </div>
                               
					</div>
                <!--end of container-->
            </section>
			</div>
  </div>
  </div>
  <script>
$('div.dataTables_filter input').addClass('form-control form-control-md');
    $(document).ready(function() {
		 
        $('#member_table').DataTable();
		$('#member_table_filter input').removeClass('form-control-sm');
		$('#member_table_filter input').addClass('form-control-md');
		$('#member_table_length select').removeClass('form-control-sm');
		$('#emember_table_length select').addClass('form-control-md'); 
    });
</script>
 <Script>
//EDIT GROUP SCRIPT
$(function() { // <----------------missed the doc ready function
    $('#groupedit_form').submit(function(e) {
        e.preventDefault(); // <------this will restrict the page refresh
        /*var form_data = {
            grpuni: $('#group_uni').val(),
            grpname: $('#group_name').val(),            
        };*/
		var form = $('#groupedit_form');
        var formData = new FormData(form[0]);
        console.log(formData);
        //alert(form_data.e_name);
        $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + 'leader/editgroup_submit',
                data: formData,
				processData: false,  // tell jQuery not to process the data
				contentType: false,  // tell jQuery not to set contentType
                success: function(res)  {
					//alert(res);
                    var json = JSON.parse(res);
                    console.log(res);
                     if(json.st== 'fail'){
                        $('#school_name_error').html(json.c_uni);
                        $('#group_name_error').html(json.c_name);
						$('#group_desc_error').html(json.c_desc);
						}
                   
                   else if(json.st =='success'){
                        //alert('Group Updated successfully');
						swal(
						  '',
						  'Group updated successfully!',
						  'success'
						)
                        window.location.href = "<?php echo site_url('leader/my_groups'); ?>";
                    } 
                   else{
						console.log('error');
				   }
                }
        });

    });

});
</script>
<script type="text/javascript">
//window.onload = function() {
    // Do Stuff Here...
	//populate the textboxes
    //$(e.currentTarget).find('input[name="editgroup_id"]').val(editgrpid);
   // document.getElementById('editgroup_name').value = editgrpname;
    
//}
</script>	
<script>
     function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(150);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>