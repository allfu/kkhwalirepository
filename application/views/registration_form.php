<html>
<head>
    <title>Registration Form</title>
	
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Site Description Here">

    <link rel="stylesheet" type='text/css' href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
	<link href="<?php echo base_url().'assets/css/style.css'; ?>" rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script type="text/js" src="<?php echo base_url().'assets/js/';?>bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <style>
        body {
            background-image: url("<?php echo base_url().'assets/img/back.jpg'; ?>");
            background-size: cover;
        }

        #login {
			background-color: #fff;
		}    
		.form-control {
			display: block;
			width: 100%;
			height: 40px;
			padding: 6px 12px;
			font-size: 14px;
			line-height: 1.42857143;
			color: #555;
			background-color: #fff;
			background-image: none;
			border: 1px solid #ccc;
			border-radius: 4px;
			-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
			box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
			-webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
			-o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
			transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
		}
		
		.select2-container--default .select2-selection--single {
			background-color: #fff;
			border: 1px solid #aaa;
			border-radius: 4px;
			padding: 18px;
		} 
		.select2-container--default.select2-container--focus .select2-selection--multiple {
			border: 1px solid #aaa;
			outline: 0;			
		}
		.select2-container--default .select2-selection--multiple .select2-selection__rendered {
			box-sizing: border-box;
			list-style: none;
			margin: 0;
			padding: 5px 5px;
			width: 100%;
		}
		
		@media only screen and (min-device-width : 768px) and (max-device-width : 1024px) {
			.select2 select2-container select2-container--default select2-container--below select2-container--focus {
				width: 100% !important;
			}
		}
		@media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
			select2 select2-container select2-container--default select2-container--focus {
				width: 100% !important;
			}
		}
		</style>
		
</head>
<body>



<div class="container-fluid">
	<div class="row">
		<div class="col-md-offset-2 col-md-8">
			 <div id="login">
				<div class="row">
					<div class="col-md-12">
						 <h2>Leader Registration Form</h2>
					</div>	
				</div>
				
				<div class="row">
					<div class="col-md-12">
						 <?php
							echo "<div class='error_msg'>";
							echo validation_errors();
							echo "</div><br>";
							echo form_open('user_authentication/new_user_registration',array('class' => '', 'id' => 'reg_form'));?>
					</div>
				</div>
				
				<div class="row form-group">
					<div class="col-md-6">
						<input type="text" class="form-control" id= "user_name" name="user_name" placeholder="Enter User Name">
						<p style='color:red' id='username-error'></p>
					</div>
					<div class="col-md-6">
						 <input type="text" class="form-control" id= "first_name" placeholder="Enter First Name" name="first_name">
						 <p style='color:red' id='fname-error'></p>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<?php
						echo "<div class='error_msg'>";
						if (isset($message_display)) {
						echo $message_display;
						}
						echo "</div>";?>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-md-6">
						<input type="text" class="form-control" id= "last_name" placeholder="Enter Last Name" name="last_name">
						<?php echo "<p style='color:red' id='lname-error'></p>";?>
					</div>
					<div class="col-md-6">						
						 <input type="text" class="form-control" id= "contact" placeholder="Enter Contact No" name="contact_no">
						 <p style='color:red' id='contact-error'></p>				
					</div>
				</div>
				
				<div class="row form-group">
					<div class="col-md-6">
						<input type="email" class="form-control" id= "email" placeholder="Enter Email Id" name="email">
						<p style='color:red' id='email-error'></p>
					</div>
					<div class="col-md-6">						
						 <input type="text" class="form-control" id= "website" placeholder="Enter Website" name="website">
						 <p style='color:red' id='website-error'></p>				
					</div>
				</div>
				
				<div class="row form-group">
					<div class="col-md-6">
						<input type="password" class="form-control" id= "pass" placeholder="Enter Password" name="password">
						<p style='color:red' id='pass-error'></p>
					</div>
					<div class="col-md-6">						
						<input type="password" class="form-control" placeholder="Enter Confirm Password" id= "re_pass" name="re_pass">
						<p style='color:red' id='re_pass-error'></p>					
					</div>
				</div>
				
				<div class="row form-group">
					<div class="col-md-6">
						 <?php $q = $this->db->get_where('university',array('permit'=>1));
							$Pr = $q->result();?>
							<!--class="form-control university init-select"-->
							 <select class="js-example-basic-single form-control" name="school_name" id="school_name" placeholder="Select School">
							 <option value="none"></option>
							 <?php foreach($Pr as $row){?>
							 <option value="<?php echo $row->u_id;?>"><?php echo $row->u_name;?></option>
							   <?php } ?>
							 </select>
							<p style="color:red;" id="school-error"></p>
						
					</div>
					<div class="col-md-6">						
						 <select class="js-example-basic-multiple form-control" class="" name="cca[]" id="cca" multiple="multiple" placeholder="Select CCA">
						   <option value="none"></option>
							  <?php foreach($res as $row){?>
							   <option value="<?= $row->c_id;?>"><?php echo $row->c_name;?></option>							  
							<?php }?>
						</select>
						<p style='color:red' id='cca-error'></p>
					</div>
				</div>
				
				<div class="row form-group">
					<div class="col-md-6">
						<input type="text" class="form-control" id="yr" name="yr" placeholder="Enter Year Of Study">
						<?php echo "<p style='color:red' id='yr-error'></p>";?>
					</div>
					<div class="col-md-6">						
						<input type="text" class="form-control" id= "Matri" name="Matri" placeholder="Matriculation No">
						<p style='color:red' id='Matriculation-error'></p>			
					</div>
				</div>
				<br/>
				<div class="row form-group">
					<div class="col-md-offset-3 col-md-3">
						<input type="submit" class="btn btn-primary" id="reg_submit" name="reg_submit" value="Sign Up">
					</div>	
					<div class="col-md-3">
						<a href="<?php echo base_url().'user_authentication/user_login'; ?> " class="btn btn-primary btnBack">Back To Login</a>
					</div>	
				</div>
			 </div>
		</div>
	</div>  
 </div>

<br/>

                <!-- <center><a href="<?php echo base_url().'user_authentication/user_login'; ?> " style="color:#fff;">For Login Click Here</a></center>-->
    
	
	</div>
	
    <script>
        $(document).ready(function()  {
            $("#reg_submit").click(function(e)  {
                e.preventDefault(); 
                //alert('form started');
                var form_data = {
                    user_name: $('#user_name').val(),
                    first_name: $('#first_name').val(),
                    last_name: $('#last_name').val(),
                    contact: $('#contact').val(),
                    email: $('#email').val(),
                    website: $('#website').val(),
                    pass: $('#pass').val(),
                    re_pass: $('#re_pass').val(),
					school: $('#school_name').val(),
					cca: $('#cca').val(),
                    yr: $('#yr').val(),
                    matno: $('#Matri').val(),
					
                };
                //alert('here'+form_data.user_name);
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('user_authentication/new_user_registration'); ?>",
                    data: form_data,
                    success: function(msg) {
                        var json = JSON.parse(msg);
                        //alert(json.error);
                        //alert(json.username);
                        console.log(json);
                        if (json.error == 'fail') {
                            $('#username-error').html(json.user_name);
                            $('#fname-error').html(json.first_name);
                            $('#lname-error').html(json.last_name);
                            $('#contact-error').html(json.contact);
                            $('#email-error').html(json.email);
                            $('#website-error').html(json.website);
                            $('#pass-error').html(json.pass);
                            $('#re_pass-error').html(json.re_pass);
                            $('#school-error').html(json.school);
							$('#cca-error').html(json.cca);
                            $('#yr-error').html(json.yr);
                            $('#Matriculation-error').html(json.matno);
                        }
                        if (json.error == 'success') {
                            if (json.msg == true) {
                                alert('Registration successful');
                                //window.location.href = "<?php echo site_url('user_authentication/user_login'); ?>";
                            } else {
                                alert('Username already exists!');
                                //window.location.href = "<?php echo site_url('user_authentication/user_registration'); ?>"
                            }

                        }
                    }

                });

            });

        });
         $(document).ready(function() {
    $('.js-example-basic-single').select2();
});
        $(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
    </script>
</body>

</html>