<style>
	.logo-blogrow {
		margin-right: -25px;
        margin-left: -25px;
		margin-top: 60px;
		margin-bottom: 20px;
	}
	.logo-blogcol {
		padding-right: 0px;
        padding-left: 0px;
		border-bottom: 1px solid #8c8c8c;
	}
	.logo-blogcolimg {
		padding-right: 0px;
		padding-left: 0px;
	}
	.logoimg-sec {
		border-radius: 50px; 
		padding: 10px;
		width: 100%;
		border: 1px solid #8c8c8c;
		margin-top: -40px;	
	}
	.content-blog {
		text-align: center;
		height: 50px;
	}
	.btn-join {
		border-radius: 30px;
		padding: 10px 30px;
	}
	.modal-title {
		text-align: center
	}
	.blogLocation {
		margin-top: 5px;
		font-size: 14px;
	}
	h4 {
		text-align: center;
		font-weight:bold;
	}
	h5 {
		text-align: center;
		
	}
	.modal-header .close {
		margin-top: -32px;
		background-color: #000;
		padding: 3px 10px;
		border-radius: 50px;
		margin-right: -29px;
		opacity: 1;
	}
	button.close {
		-webkit-appearance: none;
		padding: 0px 15px;
		cursor: pointer;
		background: 0 0;
			background-color: rgba(0, 0, 0, 0);
		border: 0;
	}
	.close {
		float: right;
		font-size: 26px;
		font-weight: 700;
		line-height: 1;
		color: #fff;
		text-shadow: 0 1px 0 #fff;
		filter: alpha(opacity=20);
		opacity: .2;
	}
	button, input[type="submit"] {
		height: 1.25000000000001em;
	}
	.close:focus, .close:hover {
		color: #e6e6e6;
		text-decoration: none;
		cursor: pointer;
		filter: alpha(opacity=50);
		opacity: .5;
	}
	@media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
		.pricing pricing-1 boxed boxed--lg boxed--border {
			min-height:500px;
		}
	}
	@media only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : portrait) and (-webkit-min-device-pixel-ratio : 2) {
		.pricing pricing-1 boxed boxed--lg boxed--border {
			min-height:1000px;
		}
	}
   
	@media only screen  and (min-width : 1224px) {
		.pricing pricing-1 boxed boxed--lg boxed--border {
			min-height:1000px;
		}
	}
	.main-sec:hover {
		cursor : pointer;
	}
   .pricing {
		max-height: 550px;
	}
</style>

<body class=" ">

<div class="container-fluid">
	<?php //echo 'here is uri sement'.$this->uri->segment(2); ?>
        <div class="row">
            <div class="col-md-3">
                <ul class="nav nav-pills nav-stacked admin-menu" >
                    <li class="<?php if($this->uri->segment(2) == 'leader_home'){echo 'active';} ?>"><a href="<?php echo base_url().'leader/leader_home';?>"  data-target="manage-event">My Events</a></li>
                    <li class="<?php if($this->uri->segment(2) == 'my_groups'){echo 'active';} ?>"><a href="<?php echo base_url().'leader/my_groups';?>"  data-target="manage-group"><!--<i class="glyphicon glyphicon-lock"></i>-->My Groups</a></li>
                    <!--<li><a href="<?php echo base_url().'leader/my_groups';?>"  data-target="manage-group">Group events</a></li>-->                   
                </ul>
            </div>

            <div class="col-md-9  admin-content" id="manage-event" >
                 <!--<div class="main-container">-->
            <?php ?>
            <section class="switchable feature-large bg--secondary" style="padding-top: 1em;">
                <!--<div class="container" style="margin-top:-10px;" >-->
				<?php //foreach();
				//echo "groups";?>
				
				<div class="row">
					<div class="col-md-2">
						<h4>My Groups</h4>
					</div>
					<div class="col-md-offset-6 col-md-2">
						<a class="btn btn-default" href="<?php echo base_url().'leader/create_group';?>">
							<span class="btn__text">Create new group</span>
						</a>
					</div>
					<div class="col-md-2">
						<a class="btn btn-default" href="<?php echo base_url().'leader/join_group';?>">
							<span class="btn__text">Join new group</span>
						</a>
					</div>
				</div>
				<br/>
				<hr class="style-two">
	<br/>
                    <div class="row">
					
					<?php foreach($grps	 as $row){
						$q=$this->db->get_where('club_table',array('c_id'=> $row->gclubid));
						if($q->row()->permit != 0){
						?>
				  <div class="col-sm-4" style="float; inherit;">
					  <div class="pricing pricing-1 boxed boxed--lg boxed--border" style="max-height: 588px;" data-target="#myModal" data-toggle="modal">
							<?php //print_r($row);
								  $gname = $q->row()->c_name;
							?>
								  
							<h4 class="text-center" style="font-weight: 400;"><?php echo $gname;?></h4>
							<span class="h2">
							<?php 
							//$q=$this->db->get_where('club_table',array('c_id'=> $row->gclubid));
							if($q->row()->c_logo != NULL){
							?>
								<center>
								<img src="<?php echo base_url().$q->row()->c_logo;?>" style="height:150px; width:150px; min-height:90px; min-width:90px;">
								</center>
							<?php } 
							else{?>
								<center>
								<img src="<?php echo base_url().'assets/frontend/img/clublogos/default_logo.png';?>" style="height:150px; width:150px; min-height:90px; min-width:90px;">
								</center>
							<?php } ?>
							</span>
							<span class="type--fine-print">
							<center>
							<?php echo $q->row()->c_desc;?>
							</center>
							</span>
							<hr>
							<?php 
								$q2=$this->db->get_where('club_table',array('c_id'=> $row->gclubid));
								$uni_id = $q2->row()->c_uni;
								$q3=$this->db->get_where('university',array('u_id'=> $uni_id));
								$uni_name = $q3->row()->u_name; 
							?>
							<h5 style="text-align: center"><?php echo $uni_name; ?></h5>
							<div class="btn-group">
							<a class="btn btn-info" href="<?php echo base_url().'leader/edit_group/'.$row->gclubid.'/'.$uni_id;?>"><span class="btn__text" style="z-index:10">Edit Group</span></a>
							<?php $q4 =$this->db->get_where('club_table',array('c_id'=> $row->gclubid));
							$p = $q4->row()->permit;
							if($p == 1){?>
							<a class="btn btn-warning" href="<?php echo base_url().'leader/delete_group/'.$row->gclubid;?>"> Delete group</a>
							<?php  } 
							else {?>
							<a class="btn btn-warning" href="" > Deleted</a>
							<?php } ?>
							</div>
						</div>
										<!--end of pricing-->
					</div>
						<?php  }  }?>
					</div>
                    <!--end of row-->
              
                <!--</div>*-->
                <!--end of container-->
            </section>
			
           
            </div>
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index:15">
					  <div class="modal-dialog" role="document">
						  <div class="modal-content">
						  <div class="modal-header">
						  
							<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="height:1.30em!important"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">
							
								 <div>School Of Physical And Mathematical Sciences Club </div>
								
								 <div class="blogLocation">Nanyang Technological University</div>							 
							 </h4>
						  </div>
						  <div class="modal-body">
							 <div class="row">
								<div class="col-md-12">
									<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
										  <!-- Indicators -->
										 

										  <!-- Wrapper for slides -->
										  <div class="carousel-inner" role="listbox">
												<div class="item active">
												  <img src="<?php echo base_url().'assets/frontend/img/slider1.jpg';?>" style="width:100%;height: 250px;margin-bottom:0">
												 
												</div>
											<div class="item">
												   <img src="<?php echo base_url().'assets/frontend/img/slider2.jpg';?>" style="width:100%;height: 250px;;margin-bottom:0">
												 
											</div>
											
										  </div>

										  <!-- Controls -->
										  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
											<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
											<span class="sr-only">Previous</span>
										  </a>
										  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
											<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
											<span class="sr-only">Next</span>
										  </a>
									</div>
								</div>
							 </div>
							 <div class="row">
								    <div class="col-md-5 col-xs-3 col-sm-4"></div>																										
										<div class="col-md-2 col-xs-6 col-sm-4">
											 <div class="logoimg-sec" style="border: 2px solid #a6a6a6;">
												    <img src="<?php echo base_url().'assets/frontend/img/upload_image.png';?>">
											 </div> 
										</div>
									<div class="col-md-5 col-xs-3 col-sm-4"></div>																								
							 </div>
							 <br/>
							 <div class="row">
								<div class="col-md-6">
									<h4>About Our Club</h4>
									<p>We are a forward looking club that put
										ourselves at a higher calling. We love
										science, physics, biology and all kind of
										math. Science is good for knowledge
										and it propagates a healthy civilisation
										of progressive species. Come join us
										and propagate the next wave of
										leaders.
										2000 members
									</p>
								</div>
								<div class="col-md-6">
									<h4>Past Activities</h4>
									<div class="row">
										<div class="col-md-6">
											<div><img class="img-thumbnail" src="<?php echo base_url().'assets/frontend/img/upload_image.png';?>"></div>	
											<div>
												<h5>FOC Camp</h5>
											</div>
											<div>
												<p> Here is where
													freshman bond
													and make Friends
												</p>
											</div>
										</div>
										<div class="col-md-6">
											<div><img class="img-thumbnail" src="<?php echo base_url().'assets/frontend/img/upload_image.png';?>"></div>	
											<div>
												<h5>FOC Camp</h5>
											</div>
											<div>
												<p> Here is where
													freshman bond
													and make Friends
												</p>
											</div>
										</div>
									</div>
								</div>
							 </div>
							 <br/>
							 <br/>
							 <div class="row">
								<div class="col-md-offset-2 col-md-8">
									<div style="border-bottom: 5px solid #8080ff"></div>
								</div>
							 </div>
							  <div class="row">
								<div class="col-md-12">
									<h4 style="text-align:center">Upcoming Activities</h4>
								</div>
							 </div>
							 <br/>
							  <div class="row">
								<div class="col-md-3">
									 <img class="img-thumbnail" src="<?php echo base_url().'assets/frontend/img/upload_image.png';?>">
								</div>
								<div class="col-md-9">
									<p>Dodgeball 2017 Championship</p>
								</div>
							  </div>
							  <br/>
							  <div class="row">
								<div class="col-md-12">
									<div style="text-align:center">
										<a class="btn btn-info btn-join" href="" >														
											<span class="btn__text">Joined</span>
										</a>
									</div>
								</div>
							  </div>
						  </div>
						 
						</div>
						
					  </div>
				</div>
  </div>
  </div>
	 