                                        <footer class="footer-3 text-center-xs space--xs bg--dark ">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <img alt="Image" class="logo" src="<?php echo base_url().'assets/backend/';?>img/afu.png" />
                            <ul class="list-inline list--hover">
                                <li>
                                    <a href="#">
                                        
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                       
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6 text-right text-center-xs">
                            <ul class="social-list list-inline list--hover">
                                <li>
                                    <a href="#">
                                        <i class="socicon socicon-google icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="socicon socicon-twitter icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="socicon socicon-facebook icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="socicon socicon-instagram icon icon--xs"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--end of row-->
                    <div class="row">
                        <div class="col-sm-6">
                            <p class="type--fine-print">
                                
                            </p>
                        </div>
                        <div class="col-sm-6 text-right text-center-xs">
                            <span class="type--fine-print">&copy;
                                <span class="update-year"></span> DTS</span>
                            <a class="type--fine-print" href="#">Privacy Policy</a>
                            <a class="type--fine-print" href="#">Legal</a>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </footer>
        </div>
        <!--<div class="loader"></div>-->
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
            <i class="stack-interface stack-up-open-big"></i>
        </a>
        
        <script src="<?php echo base_url().'assets/frontend/';?>js/flickity.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/easypiechart.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/parallax.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/typed.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/datepicker.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/isotope.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/ytplayer.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/lightbox.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/granim.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/jquery.steps.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/countdown.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/twitterfetcher.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/spectragram.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/smooth-scroll.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/scripts.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/gmap3.min.js"></script>
		
    </body>
</html>