<!doctype html>
<html lang="en">
<?php
if (isset($this->session->userdata['logged_in'])) {

//header("location: http://localhost/login/index.php/user_authentication/user_login_process");
}
?>
    <head>
        <meta charset="utf-8">
        <title>AFU|Student Leader Sign</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url().'assets/frontend/fonts/FontAwesome.otf'; ?>">
        <link href="<?php echo base_url().'assets/frontend/';?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/socicon.css" rel="stylesheet" type="text/css" media="all" />
		<link href="<?php echo base_url().'assets/frontend/';?>css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/theme.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/custom.css" rel="stylesheet" type="text/css" media="all" />
		<link href="<?php echo base_url().'assets/frontend/';?>css/font-awesome.css" rel="stylesheet" type="text/css" media="all" />
		<link href="<?php echo base_url().'assets/frontend/';?>css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
		
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i%7CMaterial+Icons" rel="stylesheet">
    </head>
     <body class=" ">

<div class="container-fluid">
  
        <div class="row">

            <div class="col-md-3">
                <ul class="nav nav-pills nav-stacked admin-menu" >
                    <li class="active"><a href="" data-target-id="manage-event"><i class="glyphicon glyphicon-user"></i> Manage Event</a></li>
                    <li><a href="" data-target-id="manage-group"><i class="glyphicon glyphicon-lock"></i> Manage Groups</a></li>
                    
                </ul>
            </div>

            <div class="col-md-9  admin-content" id="manage-event" >
                 <!--<div class="main-container">-->
            
            <section class="switchable feature-large bg--secondary">
                <!--<div class="container" style="margin-top:-10px;" >-->
                    <div class="row"  style="padding:10px; margin-left: 10px;margin-right: 10px; border: 2px solid gray;">
                        <div class="col-sm-6" style="border-right: 2px solid gray;">
                            <img alt="Image" class="border--round" src="<?php echo base_url().'assets/frontend/';?>img/xyz.jpg" />
							<h4 style="color:#000000; font:bold; "> <b>Your Club Have Worked Hard For Your Welfare
                                 Do Help Them Achieve These Objectives</b> </h4>
								 <br>
	                  <div class="progress">
				    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
					  <span class="sr-only">60% Complete</span>
					  <span class="progress-type">81 of 120 Facebook</span>
				    </div>
					</div>
                    <br>
					<div class="progress">
				    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
					  <span class="sr-only">60% Complete</span>
					  <span class="progress-type">81 of 120 Instagram</span>
				    </div>
					</div>
                    <br>
					<div class="progress">
				    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
					  <span class="sr-only">60% Complete</span>
					  <span class="progress-type">101 of 120 Facebook</span>
				    </div>
					</div>
				</div>
                        <div class="col-sm-6 col-md-5">
                            <div class="switchable__text">
							<center>
                            <img alt="Club logo" height="90" width="90" class="border--round" src="<?php echo base_url().'assets/frontend/';?>img/upload_image.png" />
								<h4 style="color:#00aeff;">120 out of 155 Attending</h4>
								<h4><b>SPMS Exam Welfare Giveaway</b></h4>
								<p>School of Physical and Mathematical Sciences
                                    <br><br>
									We like to encourage all
									students to strive hard for this
									upcoming exam through the
									giving of a token gesture from
									our club. Knowing that the
									journey is tough, but it makes
									you a tougher person as well.
									Hope the snacks and welfare
									given put smiles in your face
                                </p>
                                
							</center>


                               
                            </div>
                        </div>
                    </div>
                    <!--end of row-->
                  <div class="row" style="margin-left: 10px; margin-right:10px;
                                          border-left:  2px solid gray;  border-right:  2px solid gray;   border-bottom: 2px solid gray;">
					<h2 align="center">Features</h2>
                        <div class="col-sm-6 col-md-3">
                           <i class="fa fa-ticket fa-4x" aria-hidden="true"></i>
	                      <h4>Ticketing</h4>
                        </div>
						 <div class="col-sm-6 col-md-3">
                          <i class="fa fa-money fa-4x" aria-hidden="true"></i>
	                      <h4>Payment</h4>
                        </div>
						 <div class="col-sm-6 col-md-3">
                       <i class="fa fa-search fa-4x" aria-hidden="true"></i>
	                      <h4>Tracking</h4>
                        </div>
						 <div class="col-sm-6 col-md-3">
                           <i class="fa fa-commenting fa-4x" aria-hidden="true"></i>
	                      <h4> Feedback</h4>
                        </div>
                        
                    </div>
                <!--</div>*-->
                <!--end of container-->
            </section>
           
            </div>
  </div>
   <div class="col-md-9  admin-content" id="manage-group">
        <div class="main-container">
            <section class="switchable feature-large bg--secondary">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            
                            <img alt="Image" class="border--round" src="<?php echo base_url().'assets/frontend/';?>img/uploadss_image.png" />
							<h3 style="color:#000000; font:bold; "> <b>Your Club Have Worked Hard For Your Welfare
                                 Do Help Them Achieve These Objectives</b> </h3>
								 <br>
	                  <div class="progress">
				    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
					  <span class="sr-only">60% Complete</span>
					  <span class="progress-type">81 of 120 Facebook</span>
				    </div>
					</div>
					<div class="progress">
				    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
					  <span class="sr-only">60% Complete</span>
					  <span class="progress-type">81 of 120 Instagram</span>
				    </div>
					</div>
					<div class="progress">
				    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;" data-toggle="tooltip" data-placement="top" title="HTML / HTML5">
					  <span class="sr-only">60% Complete</span>
					  <span class="progress-type">101 of 120 Facebook</span>
				    </div>
					</div>
				</div>
                        <div class="col-sm-6 col-md-5">
                            <div class="switchable__text">
							<center>
                            <img alt="Club_logo" height="90" width="90" class="border--round" src="<?php echo base_url().'assets/frontend/';?>img/club_logo.png" />
								<h4 style="color:#00aeff;">120 out of 155 Attending</h4>
								<h3><b>SPMS Exam Welfare Giveaway</b></h3>
								<p>School of Physical and Mathematical Sciences</p>
								                                <p class="lead">
									We will like to encourage all
									students to strive hard for this
									upcoming exam through the
									giving of a token gesture from
									our club. Knowing that the
									journey is tough, but it makes
									you a tougher person as well.
									Hope the snacks and welfare
									given put smiles in your face
                                </p>
							</center>


                               
                            </div>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <section class="switchable feature-large bg--secondary">
                <div class="container">
                    <div class="row">
					<h2 align="center">Features</h2>
                        <div class="col-sm-6 col-md-3">
                           <i class="fa fa-ticket fa-4x" aria-hidden="true"></i>
	                      <h4>Ticketing</h4>
                        </div>
						 <div class="col-sm-6 col-md-3">
                          <i class="fa fa-money fa-4x" aria-hidden="true"></i>
	                      <h4>Payment</h4>
                        </div>
						 <div class="col-sm-6 col-md-3">
                       <i class="fa fa-search fa-4x" aria-hidden="true"></i>
	                      <h4>Tracking</h4>
                        </div>
						 <div class="col-sm-6 col-md-3">
                           <i class="fa fa-commenting fa-4x" aria-hidden="true"></i>
	                      <h4> Feedback</h4>
                        </div>
                        
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            
           
        </div>
</div>

            </div>

          
                </form>
            </div>
<script>
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
});
</script>

        <script src="<?php echo base_url().'assets/frontend/';?>js/jquery-3.1.1.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/flickity.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/easypiechart.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/parallax.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/typed.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/datepicker.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/isotope.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/ytplayer.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/lightbox.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/granim.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/jquery.steps.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/countdown.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/twitterfetcher.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/spectragram.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/smooth-scroll.min.js"></script>
        <script src="<?php echo base_url().'assets/frontend/';?>js/scripts.js"></script>
		<script src="<?php echo base_url().'assets/frontend/';?>js/eventcss.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/ace/1.2.5/ace.js"></script>
    </body>
</html>