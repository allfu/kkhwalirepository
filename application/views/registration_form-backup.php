<html>
<?php
if (isset($this->session->userdata['logged_in'])) {
header("location: http://localhost/login/index.php/user_authentication/user_login_process");
}
?>
<head>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" />
<title>Registration Form</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
<script src="<?php echo base_url().'assets/js';?>bootstrap.min.js"></script>
<script src="<?php echo base_url().'assets/js';?>bootstrap-select.min.js"></script>
<script src="<?php echo base_url().'assets/js';?>bootstrap-select.js.map"></script>
<script src="<?php echo base_url().'assets/js';?>bootstrap-select.js"></script>
<script src="<?php echo base_url().'assets';?>jquery.js"></script>
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
<style>
body{
		background-image: url("<?php echo base_url().'assets/img/back.jpg'; ?>");
		background-size: cover;
	}
	#login{
		background-color: #fff;
	}
</style>
</head>
<body>
<div id="main">
<div id="login">
<h2>Registration Form</h2>
<hr/>
<?php
echo "<div class='error_msg'>";
echo validation_errors();
echo "</div><br>";
echo form_open('user_authentication/new_user_registration',array('class' => '', 'id' => 'reg_form'));
echo form_label('Username : ');
echo"<br/>";
echo form_input('user_name','',$data = array('id'=>'user_name',));/*name,value,extra*/
echo "<p style='color:red' id='username-error'></p>";
echo "<div class='error_msg'>";
if (isset($message_display)) {
echo $message_display;
}
echo "</div>";
echo"<br/>";
echo form_label('FirstName : ');
echo"<br/>";
echo form_input('first_name','',$data = array('id'=>'first_name',));
echo "<p style='color:red' id='fname-error'></p>";
echo"<br/>";
echo"<br/>";
echo form_label('LastName : ');
echo"<br/>";
echo form_input('last_name','',$data = array('id'=>'last_name',));
echo "<p style='color:red' id='lname-error'></p>";
echo"<br/>";
echo"<br/>";
echo form_label('Contact No : ');
echo"<br/>";
echo form_input('contact_no','',$data = array('id'=>'contact',));
echo "<p style='color:red' id='contact-error'></p>";
echo"<br/>";
echo"<br/>";
echo form_label('Email : ');
echo"<br/>";
$data = array(
'type' => 'email',
'name' => 'email',
'id'   => 'email',
);
echo form_input($data);
echo "<p style='color:red' id='email-error'></p>";
echo"<br/>";
echo"<br/>";
echo form_label('Website : ');
echo"<br/>";
echo form_input('website','',$data = array('id'=>'website',));
echo "<p style='color:red' id='website-error'></p>";
echo"<br/>";
echo"<br/>";
echo form_label('Password : ');
echo"<br/>";
echo form_password('password','',$data = array('id'=>'pass',));
echo "<p style='color:red' id='pass-error'></p>";
echo"<br/>";
echo"<br/>";
echo form_label('Confirm Password : ');
echo"<br/>";
echo form_password('confirm_password','',$data = array('id'=>'re_pass'));
echo "<p style='color:red' id='re_pass-error'></p>";
echo"<br/>";
echo"<br/>";
echo form_label('School : ');
echo"<br/>";
    $options = array(
        'Nanyang Technical'                 => 'Nanyang Technical',
        'National university of Singapore'  => 'National university of Singapore',
        'Singapore Institute of Technology' => 'Singapore Institute of Technology',
            );
    $js = array('class'=>'selectpicker','id'=> "school_name",'data-show-subtext'=>"true",'data-live-search'=>'true');
echo form_dropdown('school', $options,'',$js);
echo "<p style='color:red' id='school-error'></p>";
echo"<br/>";
echo"<br/>";
    echo "here<select class='selectpicker' data-show-subtext='true' data-live-search='true'>
       <option>National Universtity of Singapore</option>
        <option>Singapore Institute Of Technology</option>
        <option>Singapore Institute Of Technology & Design</option>
        <option>Singapore Management University</option>
        <option>Singapore University Of Social Science</option>
        
      </select>";
    echo "<input type='text' name='city' list='cityname'>
<datalist id='cityname'>
  <option value='Boston'>
  <option value='Cambridge'>
</datalist>";
echo form_label('Year of Study : ');
echo"<br/>";

echo form_input('yr','',$data = array('id'=>'yr',));
echo "<p style='color:red' id='yr-error'></p>";
echo"<br/>";
echo"<br/>";
echo form_submit('submit', 'Sign Up',$data=array('id'=>'reg_submit'));
echo form_close();
?>
<a href="<?php echo base_url() ?> ">For Login Click Here</a>
</div>
</div>
<script type="text/javascript">
$(function() { // <----------------missed the doc ready function
    $('#reg_form').submit(function(e) {
        e.preventDefault(); // <------this will restrict the page refresh
        //alert('form started');
        var form_data = {
            user_name: $('#user_name').val(),
            first_name: $('#first_name').val(),
            last_name: $('#last_name').val(),
            contact: $('#contact').val(),
            email: $('#email').val(),
            website: $('#website').val(),
            pass: $('#pass').val(),
            re_pass: $('#re_pass').val(),
            school: $('#school_name').val(),
            yr: $('#yr').val(),
        };
        //alert('here'+form_data.user_name);
        $.ajax({
            url: "<?php echo site_url('user_authentication/new_user_registration'); ?>",
            type: 'POST',
            data: form_data,
            success: function(msg)
            {
            var json = JSON.parse(msg);
            //alert(json.error);
            //alert(json.username);
            console.log(json);
            if (json.error == 'fail' ) 
            {
                $('#username-error').html(json.user_name);
                $('#fname-error').html(json.first_name);
                $('#lname-error').html(json.last_name);
                $('#contact-error').html(json.contact);
                $('#email-error').html(json.email);
                $('#website-error').html(json.website);
                $('#pass-error').html(json.pass);
                $('#re_pass-error').html(json.re_pass);
                $('#school-error').html(json.school);
                $('#yr-error').html(json.yr);
            }
            if(json.error == 'success' ) 
            {
                if(json.msg == true)
                {
                    alert('Registration successful');
                    window.location.href = "<?php echo site_url('user_authentication/user_login'); ?>";
                }
                else
                {
                    alert('Username already exists!');
                    window.location.href = "<?php echo site_url('user_authentication/user_registration'); ?>"
                }
                    
                
            }
            }

        });

    });

});
</script>
</body>
</html>