<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="ME">
    <title>CLUB</title>
    
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url().'assets/backend/img/ico/apple-icon-60.png'; ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url().'assets/backend/img/ico/apple-icon-76.png'; ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url().'assets/backend/img/ico/apple-icon-120.png'; ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url().'assets/backend/img/ico/apple-icon-152.png'; ?>">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url().'assets/backend/img/ico/favicon.ico'; ?>">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url().'assets/backend/img/ico/favicon-32.png'; ?>">
	
       
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">


    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/css/bootstrap.css'; ?>">
    <!--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css">-->
	
    
    <!--Datatables CSS-->
    <!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">-->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
    <!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.jqueryui.min.css">-->
    <!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.0/css/responsive.jqueryui.min.css">-->

    <!-- font icons-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/fonts/icomoon.css'; ?>">
    <link rel="stylesheet" type="text/css" href="<?php //echo base_url().'assets/backend/css/flag-icon.min.css'; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/vendors/css/extensions/pace.css'; ?>">
    <!-- END VENDOR CSS-->

    <!-- BEGIN ROBUST CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/css/bootstrap-extended.css'; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/css/app.css'; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/css/colors.css'; ?>">
    <!-- END ROBUST CSS-->

    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/css/core/menu/menu-types/vertical-menu.css'; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/css/core/menu/menu-types/vertical-overlay-menu.css'; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/css/core/colors/palette-gradient.css'; ?>">
	
	<!--Datepicker CSS-->
    <!--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.standalone.min.css">-->
	
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/backend/css/style.css'; ?>">
    <!-- END Custom CSS-->

    <!--jquery-->
    <script src="<?php echo base_url().'assets/backend/js/core/libraries/jquery.min.js'; ?>" type="text/javascript"></script>
	
	<!--Moment Js-->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js" type="text/javascript"></script>
    
	<!--Bootstrap js-->
    <script src="<?php echo base_url().'assets/backend/js/core/libraries/bootstrap.min.js'; ?>" type="text/javascript"></script>
	
    <!--Datatables Script-->
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
    <!--<script src="https://cdn.datatables.net/1.10.16/js/dataTables.jqueryui.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.0/js/dataTables.responsive.min.js" type="text/javascript"></script>-->
	
	<!--DatePicker Js-->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js" type="text/javascript"></script>-->

    <!--jquery for autocomplete-->
    <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/autocompleteassets/jquery.ui.css" />-->
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
    <script type="text/javascript" src="<?php echo base_url();?>assets/autocompleteassets/jquery.js"></script>
   <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/autocompleteassets/jquery.ui.js"></script>-->
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.10.1/sweetalert2.all.min.js" type="text/javascript"></script>    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
	<!-- Datetime picker JS-->
		 
</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">