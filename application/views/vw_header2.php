<!doctype html>
<html lang="en" xmlns:fb="http://ogp.me/ns/fb#">
    <head>
        <meta charset="utf-8">
        <title>AFU</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Site Description Here">
		
		
      	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/css/bootstrap-datetimepicker.css">

		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
			  
			
        <!--<link href="<?php //echo base_url().'assets/frontend/';?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />-->
        <link href="<?php echo base_url().'assets/frontend/';?>css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/socicon.css" rel="stylesheet" type="text/css" media="all" />
		<link href="<?php echo base_url().'assets/frontend/';?>css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/theme.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo base_url().'assets/frontend/';?>css/custom.css" rel="stylesheet" type="text/css" media="all" />
		<link href="<?php echo base_url().'assets/backend/';?>css/style.css" rel="stylesheet" type="text/css" media="all" />
		
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i%7CMaterial+Icons" rel="stylesheet">
		<!--Jquery-->
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
		<script src="<?php echo base_url().'assets/frontend/';?>js/jquery-3.1.1.min.js"></script>
		<script src="<?php echo base_url().'assets/frontend/';?>js/jquery.wallform.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">
	        <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/min/dropzone.min.js"></script>
		<!--Bootstrap Js-->
		 <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/js/bootstrap.min.js"></script>
		 <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.all.min.js"></script>
		<!-- Datetime picker JS-->
		 <style>
		@media (min-width:961px)  { 
			.pricing	{
				max-height: 450px;
			}
		}
				.add-on .input-group-btn > .btn {
		  border-left-width:0;left:-2px;
		  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		  box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
		}

		</style>
		
		<style>
			.topnav .search-container {
			  float: right;
			 
			}

			.topnav input[type=text]{
			  padding: 5px;
			  margin-top: 8px;
			  font-size: 14px;
			  border: none;
			  border: 1px solid #e6e6e6;
			  border-radius: 0px;
			}

			.topnav .search-container button {
			  float: right;
			  padding: 6px 10px;
			  margin-top: 8px;
			  margin-right: 16px;
			  background: #ddd;
			  font-size: 17px;
			  border: none;
			  cursor: pointer;
			  border: 1px solid #ccc;
			}

			.topnav .search-container button:hover {
			  background: #ccc;
			}

			@media screen and (max-width: 600px) {
			  .topnav .search-container {
				float: none;
			  }
			  .topnav a, .topnav input[type=text], .topnav .search-container button {
				float: none;
				display: block;
				text-align: left;
				width: 100%;
				margin: 0;
				padding: 14px;
			  }
			  .topnav input[type=text] {
				border: 1px solid #ccc;  
			  }
			}
			button, input[type="submit"] {
				height: 2.30em !important;
			}
		</style>
		</head>
    <body class=" " data-smooth-scroll-offset='64'>
        <a id="start"></a>
        <div class="nav-container ">
            <div class="bar bar--sm visible-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-3 col-sm-2">
                            <a href="index.html">
                              <!--
                                <img class="logo logo-light" alt="logo" src="img/logo-light.png" /> -->
                            </a>
                        </div>
                        <div class="col-xs-9 col-sm-10 text-right">
                            <a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs">
                                <i class="icon icon--sm stack-interface stack-menu"></i>
                            </a>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </div>
            <!--end bar-->
            <nav id="menu1" class="bar bar--sm bar-1 hidden-xs " data-scroll-class='366px:pos-fixed'>
                <div class="container">
                    <div class="row">
                        <div class="col-md-1 col-sm-2 hidden-xs">
                            <div class="bar__module">
                                <a href="index.html">
                                 
                                    
                                </a>
                            </div>
                            <!--end module-->
                        </div>
                        <div class="col-md-11 col-sm-12 text-right text-left-xs text-left-sm">
                            <div class="bar__module">
                                <ul class="menu-horizontal text-left">
                                    <li class="dropdown" style="top:-16px">
								
                                        <span class="dropdown__trigger">Welcome, 
										<?php if (isset($this->session->userdata['logged_in'])) { ?>
										 	<?php
       									$username = ($this->session->userdata['logged_in']['username']);
										$email = ($this->session->userdata['logged_in']['email']);
										$q=$this->db->get_where('user_table' ,array('user_name'=> $username));
										$res=$q->row()->type;
										echo $username;?>
											 <?php												
											 }?>
											 </span>
											 <?php if ($res==1)
									{ ?>  
											 <div class="dropdown__container">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="dropdown__content col-md-2 col-sm-4">
                                                        <ul class="menu-vertical">
                                                            <li class="dropdown">
                                                                <span class="dropdown__trigger"><a  href="<?php echo base_url().'student/edit_profile'?>">Edit Profile</a></span>
                                                               </li>
															   </ul>
															   </div>
															   </div>
															   </div>
															   </div>
                                         
                                    </li>

									                       </li>
														   <li class="dropdown">
                                                                <a href="<?php echo base_url().'student/showlist';?>">Home</a>
                                                               <!--end dropdown container-->
                                                            </li>
															 <li class="dropdown">
                                                                <a href="<?php echo base_url().'student/my_groups'; ?>">Group Info</a>
                                                                <!--end dropdown container-->
                                                            </li>
															
                                                           
								
								<?php
									} else{?>     
																		                   <li class="dropdown" style="top:-16px">
                                                                <a href="<?php echo base_url().'leader/leader_home';?>">Home</a>
                                                               <!--end dropdown container-->
                                                            </li> 
															<li>
															<div class="topnav">																 
																  <div class="search-container">
																	<form action="<?php echo base_url().'leader/search';?>" method="POST">
																	  <input type="text" placeholder="Search.." name="search">
																	  <button type="submit"><i class="fa fa-search"></i></button>
																	</form>
																  </div>
															</div>
														<!--	<form action="<?php echo base_url().'leader/search';?>" method="POST"> 
																<div class="input-group">
															    <input type="text" class="form-control" placeholder="Search" id="txtsearch" name="txtsearch"/>
															   
																	<button class="btn btn-primary" type="submit" style="padding: 17px">
																	<span class="glyphicon glyphicon-search" style="top:-7px;"></span>
																	</button>
															  
															   </div>														 
															</form>-->
														</li>
									   								                       
									<?php }?>   
                                                           




								
								         <!--   <a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo base_url().'student/showlist';?>">Home
														  </a>
														  <ul class="dropdown-menu">
														    <li class="dropdown-header"><a href="<?php echo base_url().'student/showlist';?>">Show Events</a></li>
															<li class="dropdown-header"><a href="#">Edit Profile</a></li>
															<li class="dropdown-header"><a href="<?php echo base_url().'student/my_groups'; ?>">Group Info</a></li>
															
														  </ul>
																				
								    <li> 
									
									   <a href="<?php echo base_url().'student/showlist';?>" >Home</a>
								     </li>
								          <li>
											 <a href="<?php echo base_url().'student/my_groups'; ?>">
                                                  Group Info    
                                                  </a>
                             
                                          <!--end dropdown container-->
										<?php if (isset($this->session->userdata['logged_in'])) {
												$usertype = ($this->session->userdata['logged_in']['sessiontype']);
												if($usertype==1){$url=base_url()."student/logout";}
												else{$url = base_url().'leader/logout';}
												}?>
                                        <li class="dropdown" style="top:-16px"><a href="<?php echo $url; ?>" >Logout</a></li>
                                    </li>
                                </ul>
                            </div>
							</div>
                            <!--end module-->
                            
                            <!--end module-->
                        </div>
                    </div>
                    <!--end of row-->
                </div>

                <!--end of container-->
            </nav>
            <!--end bar-->
        </div>
		  <body class=" ">