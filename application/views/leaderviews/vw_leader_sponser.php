
<body class=" ">

<div class="container-fluid">
  
        <div class="row">

            <div class="col-md-3">
                <ul class="nav nav-pills nav-stacked admin-menu" >
                    <li class="active"><a href="<?php echo base_url().'leader/leader_home';?>"><!--<i class="glyphicon glyphicon-user"></i>-->My Events</a></li>
                    <li><a href="<?php echo base_url().'leader/my_groups';?>"  ><!--<i class="glyphicon glyphicon-lock"></i>-->My Groups</a></li>
                    <!--<li><a href="<?php echo base_url().'leader/my_groups';?>" ><!--<i class="glyphicon glyphicon-lock"></i>--Group events</a></li>-->
                </ul>
            </div>

            <div class="col-md-9  admin-content" id="manage-event" >
                 <!--<div class="main-container">--> 
            <section class="switchable feature-large bg--secondary">
			<div class="row" style="margin-bottom: 10px; margin-right: 2px;">
			<div class="col-md-12">
	      		<a class="btn btn-primary pull-right" href="<?php echo base_url().'leader/create_event';?>">
					<span class="btn__text">Create new event</span>
				</a>
				<a class="btn btn-primary pull-right" href="<?php echo base_url().'leader/leader_sponsors';?>">
					<span class="btn__text">Select Sponsors</span>
				</a>
				</div>
				</div>
                <!--<div class="container" style="margin-top:-10px;" >-->
				<form id="sponsor_insert"  name="sponsor_insert" action="" method="POST">
				<?php foreach($eve as $row){
					?>
					<div class="row"  style="padding:10px; margin-left: 10px;margin-right: 10px; border: 2px solid gray;">
                        <div class="col-sm-6" style="border-right: 2px solid gray;">
						<input type="hidden" name="e_id" id="e_id" value="<?php echo $row->e_id;?>" class="form-control">
							<?php if(isset($row->e_img)){?>
                            <img alt="Image" class="border--round" src="<?php echo base_url().$row->e_img;?>" />
							<?php } 
							else{?>
                            <img alt="Image" class="border--round" src="<?php echo base_url().'assets/frontend/';?>img/xyz.jpg" />
							<?php } ?>
							<h4 style="color:#000000; font:bold; "> <b>Your Club Have Worked Hard For Your Welfare
                                 Do Help Them Achieve These Objectives</b> </h4>
								 <br>
								 <label >Select Sponsors</label>
		             <select class="js-example-basic-multiple form-control"  name="sponsor[]" id="sponsor" multiple="multiple" placeholder="Select Sponsors">
		                 <option value="none"></option>
							  <?php foreach($spns as $row1){?>
							   <option value="<?= $row1->s_id;?>"><?php echo $row1->s_name;?></option>
							  <?php } ?>
						</select>
	                    <br>
					     <br>
						 <br>
								<div class="col-md-6">
							 <input type="submit" value="Submit" id="upload_sponsor" name="upload_sponsor" class="btn btn-success">
								</div>
								<div class="col-md-6">
								  <!-- <input type="submit" value="Approval"  class="btn btn-Info" href="" >-->
									</div>
							</div>
				    <div class="col-sm-6 col-md-5">
						<?php if($row->publish==0){?>
						<a class="btn btn-success" href="<?php echo base_url().'leader/publish_event/'.$row->e_id;?>"> Publish</a>
						

						<?php } 
						else {?>
						<a class="btn btn-warning" href="<?php echo base_url().'leader/unpublish_event/'.$row->e_id;?>"> Published</a>
						<?php } ?>
                            <div class="switchable__text">
							<center>
                            <img alt="Club logo" height="90" width="90" class="border--round" src="<?php echo base_url().'assets/frontend/';?>img/upload_image.png" />
								<h4 style="color:#00aeff;">120 out of 155 Attending</h4>
								<h4><b><?php echo $row->e_name;?></b></h4>
								<p><?php $q=$this->db->get_where('club_table',array('c_id'=> $row->e_by));
								  $gname = $q->row()->c_name;
								  echo $gname;?>
                                    <br><br>
									<?php echo $row->e_desc;?>
                                </p>
								<br>
								<?php if(isset($row->e_start)){?>
								<p><?php
								echo date("l F jS, Y g:i a", strtotime($row->e_start));
								//echo "<br>".$row->e_start;?></p>
								<?php }?>
								<div class="row">
								<div class="col-md-6">
							
								<p style="color:black;"><b>
								<?php if(isset($row->e_type)){echo $row->e_type;} 
								else{echo "Welfare";}?>
								</b></p>
								Event 
								<br><br>
								
								</div>
								<div class="col-md-6">
								<p style="color:black;"><b>
								<?php if(isset($row->e_loc)){echo $row->e_loc;} 
								else{echo "Atrium A";}?>
								</b></p>
								Location
								<br><br>
								
								</div>
								</div>
                                
							</center>
 
                            </div>
                        </div>
                    </div>
					</form>
					<br><br><br>
				<?php } ?>
                    <!--end of row-->
              
                <!--</div>*-->
                <!--end of container-->
            </section>
           
            </div>
  </div>
          </div>
<script>
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
        $(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
</script>
	  <script>
$(document).ready(function(e)  {
        $("#upload_sponsor").click(function(e)  {
		e.preventDefault();
		//var formData = $('#sponsor_insert');
		var formData =  $('#sponsor_insert').serialize()
		//var formData =  new FormData(this);
		console.log(formData);
		$.ajax({
                type: "POST",
                url: "<?php echo base_url().'leader/sponsor_submit'; ?>",
				processData: false,
                data: formData,
                success: function(res)  {
					var parsed = JSON.parse(res);
                    console.log(parsed);
                   if(parsed.status_code == 1) {
                       // alert('data inserted successfully');
                       // window.location.href = "<?php echo site_url('leader/leader_home'); ?>";
                        swal({
                                html: parsed.status,
                                type: 'success'
                        }).then(function() {
                            window.location.href = "<?php echo site_url('leader/leader_home'); ?>";
                        });
                    }
                    else if(parsed.status_code == 0)    {
                        alert(parsed.status);
                        swal({
                                html: parsed.status,
                                type: 'error',
                            });
                        }
                    else    {
                            swal({
                                html: parsed.status,
                                type: 'error',
                            });
                            //alert(parsed.status);
                        //$('#form-error').html(parsed.status);
					
					}
				}
		});
		});
	});
</script>
