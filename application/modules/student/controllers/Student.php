<?php
Class Student extends MX_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('mdl_student');
		if(!$this->session->userdata('logged_in'))     
		{    redirect('user_authentication');  }
	
    }
    
    function index()
    {
        /*$data['res'] = $this->mdl_student->get_events();
        $this->load->view('vw_header2');
        $this->load->view('vw_listing',$data);
        // $this->load->view('vw_student');
        $this->load->view('vw_footer');*/
		$this->showlist();
    }
	function logout()
	{
		$this->session->sess_destroy();
  echo "<script>alert('You're Successfully Logged out');</script>";
  redirect('user_authentication','refresh');
	}
	function uset()
	{
		$this->session->unset_userdata('fb',0);
		$this->get_event();
	}
	function get_event()	
	{
		//if(!empty($this->session->userdata('fb')))
		//{
			//$this->session->unset_userdata('fb',0);
		//}
		
			//unset($_SESSION['fb']);	
		
        $e_id = $this->input->post('e_id');
        $e_by = $this->input->post('e_by');
		$result = $this->mdl_student->get_event($e_id);
        $res = $this->mdl_student->getclub($e_by);
		foreach($result as $r){
		
		//$var = $this->session->set_userdata('fb',$r->e_fblink);/*$result->e_fblink*/
		//echo $r->e_fblink;
		//$this->session->set_flashdata('fb1',$r->e_fblink );
			
		}
        //echo json_encode($res);
        if($result) {
            $data = array(
                'status_code' => 1,
                'status' => $result,
                'clubname' => $res->c_name,
				//'linkname'=> $result->e_fblink,
            );
            echo json_encode($data);
        }   else    {
            $data = array(
                'status_code' => 0,
                'status' => false,
                'clubname' => $res->c_name,
            );
            echo json_encode($data);
        }
		
	}
	
	function showlist()
	{
		$data['res'] = $this->mdl_student->get_events();
        $this->load->view('vw_header2');
        $this->load->view('vw_list',$data);
        $this->load->view('vw_footer');
	}
	 function fbshare()
    {
        $this->load->view('vw_header2');
        $this->load->view('vw_fbshare');
        $this->load->view('vw_footer');
    }
    function checkfbshare()
    {
        // if the facebook shared form is posted proceed
        if ($_POST['shared']) 
        {
            print_r($_POST['shared']);
        // perform whatever action you wish to do in between these brackets
        }

    }
    
    function awardpoint($p)
    {
        //echo  "Hello. in case u shared- thnk you.";
        //echo "<script>alert('1 point for sharing');</script>";
		 return json_encode(array('status'=>'done')) ;
    }
	
	function my_groups()
{
	 //if (isset($this->session->userdata['logged_in'])) 
        //{
		$username = ($this->session->userdata['logged_in']['username']);
		$email = ($this->session->userdata['logged_in']['email']);
    //}
			   
        $data['grps']=$this->mdl_student->getgroups($username);
		//print_r($data);
        
        $this->load->view('vw_header2');
        $this->load->view('vw_student_group',$data);
        //$this->load->view('vw_student_leader_localhost');
        $this->load->view('vw_footer');
}
function join_newgroup($clubid)
 {
	$username = $this->session->userdata['logged_in']['username'];
	$uid=$this->mdl_student->getuserid($username);
	$data5=array(
		'gstudid'=>$uid,
		'gclubid' => $clubid,
		'created' => date('Y-m-d'),
	);
	$this->mdl_student->join_club($data5);
	redirect('student/my_groups','refresh');	 
 }
  function join_group()
 {
	$username = $this->session->userdata['logged_in']['username'];
	$uid=$this->mdl_student->getuserid($username);
	$data['allgrps']=$this->mdl_student->getallgroups();
	$data['uid']=$uid;
	
	$this->load->view('vw_header2');
    $this->load->view('vw_student_other_grp',$data);
    $this->load->view('vw_footer');
 }
 function edit_profile()
 {
	  $username = $this->session->userdata['logged_in']['username'];
	  $uid=$this->mdl_student->getuserid($username);
      $qr=$this->db->get_where('user_table',array('id'=>$uid));
	  $data['getalldetails']=$qr->row();
	  
	 
	  //print_r($gr->user_name);
	// $data['getdetails']=$qr->
	  $this->load->view('vw_header2');
      $this->load->view('vw_edit_student_profile',$data);
      $this->load->view('vw_footer');
	 
 }
 

function update_prof()
{
	//echo json_encode($_POST);
    //$this->form_validation->set_rules('phone_number', 'Phone no', 'trim|required|numeric');
    $this->form_validation->set_rules('mat', 'Matriculation no', 'trim|required');
    $this->form_validation->set_rules('uni', 'University name', 'trim|required');
    $this->form_validation->set_rules('fac', 'Faculty name', 'trim|required');
    $this->form_validation->set_rules('hl', 'Hall name', 'trim|required');
    $this->form_validation->set_rules('grp', 'CCA name', 'trim|required');
    //$this->form_validation->set_rules('student_accommodation', 'School Accomodation', 'required|trim');
   // $this->form_validation->set_rules('school_email', 'School email', 'required|trim');


    $data = null;
    if($this->form_validation->run() == false)
    {
         $data = array(
            'st'		            => 'fail',
            //'phone_number'          => form_error('phone_number'),
            's_matri'  => form_error('s_matri_error'),
            's_uni'             => form_error('s_university-error'),
            's_fac'               => form_error('s_faculty-error'),
            's_hall'              => form_error('s_hall-error'),
            's_grp'                => form_error('s_club-error'),
            //'student_accommodation'   => form_error('student_accommodation'),
           // 'school_email'            => form_error('school_email'),

               );
          echo json_encode($data);
    }
    else {
		
		 $id= $this->input->post('id');
         $data = array(
	            
        //*send fb values from view to db/model*/
        //'contact_no'  => $this->input->post('phone_number'),
        'matno'  => $this->input->post('mat'),
        'school'  => $this->input->post('uni'),
        'faculty'  => $this->input->post('fac'),
        'hall_name'  => $this->input->post('hl'),
        'group_cca'  => $this->input->post('grp'),
        //'accomodation'  => $this->input->post('student_accommodation'),
        //'school_email'  => $this->input->post('school_email'),
            );
			$this->mdl_student->update_student($id,$data);
            
        $return_json = array('st'=> 'success');
        echo json_encode($return_json);
	}
	}


}
?>