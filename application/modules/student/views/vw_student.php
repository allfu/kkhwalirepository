<div class="main-container">
     <section class="bg--dark space--sm">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <div class="masonry masonry-demos">
                                <div class="masonry-filter-container text-center">
                                    <span>Viewing:</span>
                                    <div class="masonry-filter-holder">
                                        <div class="masonry__filters" data-filter-all-text="All Categories"></div>
                                    </div>
                                </div>
                                <div class="masonry__container">
                                    <div class="masonry__item col-md-4 col-sm-6"></div>
                                    <div class="masonry__item col-md-4 col-sm-6" data-masonry-filter="Landing">
                                        <a href="home-landing-1.html" class="block text-block">
                                            <div class="hover-shadow">
                                                <img alt="Landing 1" src="<?php echo base_url();?>assets/frontend/img/demos/landing-1.jpg" />
                                            </div>
                                        </a>    
                                        <div class="text-center">
                                            <h5>Landing 1</h5>
                                            <span>Landing Page</span>
                                        </div>
                                    </div>
                                    <!--end item-->
                                    <div class="masonry__item col-md-4 col-sm-6" data-masonry-filter="Landing">
                                        <a href="home-landing-2.html" class="block text-block">
                                            <div class="hover-shadow">
                                                   <div class="tab__content">
                            <section class=" ">
                                <div class="container">
                                    <div class="row">
                                        <div class="masonry">
                                            <div class="masonry__container">
                                                <div class="col-sm-4 masonry__item">
                                                    <div class="card card-2 text-center">
                                                        <div class="card__top">
                                                            <a href="#">
                                                                <img alt="Image" src="img/landing-8.jpg">
                                                            </a>
                                                        </div>
                                                        <div class="card__body">
                                                            <h4>Photography 101</h4>
                                                            <span class="type--fade">Understanding the fundamentals</span>
                                                            <p>
                                                                Construct mockups or production-ready pages in-browser with Variant Page Builder
                                                            </p>
                                                        </div>
                                                        <div class="card__bottom text-center">
                                                            <div class="card__action">
                                                                <span class="h6 type--uppercase">Explore</span>
                                                                <a href="#">
                                                                    <i class="material-icons">flip_to_front</i>
                                                                </a>
                                                            </div>
                                                            <div class="card__action">
                                                                <span class="h6 type--uppercase">Save</span>
                                                                <a href="#">
                                                                    <i class="material-icons">favorite_border</i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 masonry__item">
                                                    <div class="card card-2 text-center">
                                                        <div class="card__top">
                                                            <span class="label">New</span>
                                                            <a href="#">
                                                                <img alt="Image" src="img/landing-14.jpg">
                                                            </a>
                                                        </div>
                                                        <div class="card__body">
                                                            <h4>Kiln Fired Pottery</h4>
                                                            <span class="type--fade">Video Tutorial Series</span>
                                                            <p>
                                                                Construct mockups or production-ready pages in-browser with Variant Page Builder
                                                            </p>
                                                        </div>
                                                        <div class="card__bottom text-center">
                                                            <div class="card__action">
                                                                <span class="h6 type--uppercase">Explore</span>
                                                                <a href="#">
                                                                    <i class="material-icons">flip_to_front</i>
                                                                </a>
                                                            </div>
                                                            <div class="card__action">
                                                                <span class="h6 type--uppercase">Save</span>
                                                                <a href="#">
                                                                    <i class="material-icons">favorite_border</i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 masonry__item">
                                                    <div class="card card-2 text-center">
                                                        <div class="card__top">
                                                            <a href="#">
                                                                <img alt="Image" src="img/blog-6.jpg">
                                                            </a>
                                                        </div>
                                                        <div class="card__body">
                                                            <h4>Online Marketing</h4>
                                                            <span class="type--fade">Tips you'll want to know</span>
                                                            <p>
                                                                Construct mockups or production-ready pages in-browser with Variant Page Builder
                                                            </p>
                                                        </div>
                                                        <div class="card__bottom text-center">
                                                            <div class="card__action">
                                                                <span class="h6 type--uppercase">Explore</span>
                                                                <a href="#">
                                                                    <i class="material-icons">flip_to_front</i>
                                                                </a>
                                                            </div>
                                                            <div class="card__action">
                                                                <span class="h6 type--uppercase">Save</span>
                                                                <a href="#">
                                                                    <i class="material-icons">favorite_border</i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end of masonry__container-->
                                        </div>
                                        <!--end of masonry-->
                                    </div>
                                    <!--end of row-->
                                </div>
                                <!--end of container-->
                            </section>
                        </div>
                    
                                            </div>
                                        </a>
                                        <div class="text-center">
                                            <h5>Landing 2</h5>
                                            <span>Landing Page</span>
                                        </div>
                                    </div>
                                    <!--end item-->
                                    <div class="masonry__item col-md-4 col-sm-6" data-masonry-filter="Landing">
                                        <a href="home-landing-3.html" class="block text-block">
                                            <div class="hover-shadow">
                                                <img alt="Landing 3" src="img/demos/landing-3.jpg" />
                                            </div>
                                        </a>
                                        <div class="text-center">
                                            <h5>Landing 3</h5>
                                            <span>Landing Page</span>
                                        </div>
                                    </div>
                                    <!--end item-->
                                    <div class="masonry__item col-md-4 col-sm-6" data-masonry-filter="Landing">
                                        <a href="home-landing-4.html" class="block text-block">
                                            <div class="hover-shadow">
                                                <img alt="Landing 4" src="img/demos/landing-4.jpg" />
                                            </div>
                                        </a>
                                        <div class="text-center">
                                            <h5>Landing 4</h5>
                                            <span>Landing Page</span>
                                        </div>
                                    </div>
                                    <!--end item-->
                                    <div class="masonry__item col-md-4 col-sm-6" data-masonry-filter="Landing">
                                        <a href="home-landing-5.html" class="block text-block">
                                            <div class="hover-shadow">
                                                <img alt="Landing 5" src="img/demos/landing-5.jpg" />
                                            </div>
                                        </a>
                                        <div class="text-center">
                                            <h5>Landing 5</h5>
                                            <span>Landing Page</span>
                                        </div>
                                    </div>
                                    <!--end item-->
                                    <div class="masonry__item col-md-4 col-sm-6" data-masonry-filter="Landing">
                                        <a href="home-landing-6.html" class="block text-block">
                                            <div class="hover-shadow">
                                                <img alt="Landing 6" src="img/demos/landing-6.jpg" />
                                            </div>
                                        </a>
                                        <div class="text-center">
                                            <h5>Landing 6</h5>
                                            <span>Landing Page</span>
                                        </div>
                                    </div>
                                    <!--end item-->
                                 
                                    <!--end item-->
                                    
                                 
                                    <!--end item-->
                                </div>
                                <!--end of masonry container-->
                            </div>
                            <!--end masonry-->
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            
</div>