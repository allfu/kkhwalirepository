<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12">
                <h1>Group List</h1>
                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <!--<li><a href="#" data-toggle="modal" data-target="#add-product-modal"><i class="icon-plus4"></i> Add Client   </a></li>-->
                                        <li><button type="button" class="btn btn-info btn-block"
                                                    data-toggle="modal" data-target="#add-group-modal">
                                            <i class="icon-plus4" style="color:white;"></i>   Add Group</button>
                                        </li>
                                        <!--<li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                        <li><a data-action="reload"><i class="icon-reload"></i></a></li>-->
                                    </ul>
                                </div>
        <table id="group-table" class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Group Name</th>
                    <th>Of University</th>
                    <th>Logo</th>
                    <th>Description</th>
                    <th>Created by</th>
                    <th>Created on</th>
                    <th>Permit</th>
                    <th>Actions</th>
                </tr>
                
            </thead>
            <tbody>
                <?php foreach($data as $row)    {
                                                        ?>
                                                        <tr>
                                                        <td class="text-middle"><?php echo $row->c_name; ?></td>
                                                        
														<td class="text-middle">
														<?php 
														$q1 = $this->db->get_where('university',array('u_id'=> $row->c_uni));
														$universityname = $q1->row()->u_name;
														echo $universityname; 
														?>
														</td>
														
														<td>
														<img src="<?php echo base_url().$row->c_logo;?>" style="width:50px; height:50px; min-height:10px;min-width:10px;"/>
														</td>
														
														<td>
														<?php echo $row->c_desc;?>
														</td>
                                                        
														<td class="text-middle"><?php $q2 = $this->db->get_where('user_table',array('id'=> $row->c_by));
														$leadername = $q2->row()->user_name;
														echo $leadername;
														?>
														</td>
                                                        
														<td class="text-middle"><?php echo $row->created; ?></td>
                                                        
														<td class="text-middle"><?php if($row->permit == '1')  { ?>
                                                        <span class="tag tag-success">Active</span><?php } else { ?>
                                                        <span class="tag tag-danger">Banned</span>
                                                        <?php } ?>
														</td>
                                                        
														<td><span class="btn-group">
                                                            <!--<input type="button" class="btn btn-outline-info" value="View" data-toggle="modal" data-target="#view-modal"/>-->

                                                            <input type="button" class="btn btn-outline-primary" value="Edit" 
                                                            data-toggle="modal" data-target="#edit-group-modal" 
                                                            data-id="<?php echo $row->c_id;?>" data-name="<?php echo $row->c_name; ?>" 
                                                            data-uni="<?php echo $row->c_uni; ?>" data-lead="<?php echo $row->c_by; ?>"
                                                            data-desc="<?php echo $row->c_desc; ?>" data-logo="<?php echo $row->c_logo; ?>"/>

                                                            <input type="button" class="btn btn-outline-danger" 
                                                                   <?php 
                                                                    if($row->permit==0)  
                                                                    { echo "value='Undelete'"; ?>
																   data-toggle="modal" data-target="#undelete-group-modal" 
                                                                   data-id="<?php echo $row->c_id;?>" 
                                                                   data-gname="<?php echo $row->c_name; ?>" />
                                                                    <?php }
                                                                   else{															   
                                                                    echo "value='Delete'"; ?> 
																   data-toggle="modal" data-target="#delete-group-modal" 
                                                                   data-id="<?php echo $row->c_id;?>" 
                                                                   data-gname="<?php echo $row->c_name; ?>" />
                                                                   <?php } ?>
                                                            </span>
                                                        </td>
														
                                                        </tr>
                                                        <?php } ?>
            </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!--Add Modal-->
<div class="modal fade text-xs-left" id="add-group-modal" tabindex="-1" role="dialog" aria-labelledby="addModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Group Information</h4>
            </div>
            <form action="<?php //echo base_url().'clients/add_client';?>" method="post" id="addgroup_form" name="addgroup_form">
                <div class="modal-body">
                    <label>Group Name:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="c_name" id="c_name" value="<?php echo set_value('c_name');?>"  />
                        <p id="group_name_error" style="color:red;"></p>
                    </div>
					
					<label>By School:</label>
                    <div class="form-group">
                       
						 <select class="form-control" name="c_uni" id="c_uni">
						   <option value="none" selected="" disabled=""></option>
						   <?php $this->db->where('permit !=',0);
								 $query = $this->db->get('university');
								 foreach($query->result() as $row){
						   ?>
						   <option value="<?php echo $row->u_id;?>"><?php echo $row->u_name;?></option>
								 <?php } ?>
						</select>
                        <p id="uni_error" style="color:red;"></p>
                    </div> 
					
					<label>By Leader:</label>
                    <div class="form-group">
                      
						 <select class="form-control" name="c_by" id="c_by">
						   <option value="none" selected="" disabled=""></option>
						   <?php $this->db->where('permit !=',0);
								 $this->db->where('type !=',1);
								 $query = $this->db->get('user_table');
								 foreach($query->result() as $row){
						   ?>
						   <option value="<?php echo $row->id;?>"><?php echo $row->user_name;?></option>
								 <?php } ?>
						</select>
                        <p id="leader_error" style="color:red;"></p>
                    </div>
					<label>Group Description:</label>
                    <div class="form-group">
                        <input type="text-area" class="form-control" name="c_desc" id="c_desc" value="<?php echo set_value('c_desc');?>"  />
                        <p id="desc_error" style="color:red;"></p>
                    </div>
                    
					
					<label>Group Logo:</label>
					<div class="form-group">
						<input type='file' onchange="readURL(this);" id="logo" name="logo"/>
						<img id="blah" src="#" alt="Your logo" />
						<p id="group_logo_error" style="color:red;"></p>
					</div>
                    
                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-outline-primary" value="Submit" name="s1" id="s1"/>
                </div>
            </form>
        </div>
    </div>
</div>
<!--Edit Modal-->
<div class="modal fade text-xs-left" id="edit-group-modal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Group Information</h4>
            </div>
            <form method="post" id="editgroup_form" name="editgroup_form" action="">
                <div class="modal-body">
                    <input type="hidden" name="editgroup_id" id="editgroup_id" value="<?php echo set_value('editgroup_id');?>" /><!--value will be set through javascript-->
                    <label>Group Name:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="editgroup_name" id="editgroup_name" value="<?php echo set_value('editgroup_name');?>" />
                    </div>
                    <p id="editgroup_name_error" style="color:red;"></p>
					
					<label>By School:</label>
                    <div class="form-group">
                        <select class="form-control" name="editgrp_uni" id="editgrp_uni">
						   <option value="none" selected="" disabled=""></option>
						   <?php $this->db->where('permit !=',0);
								 $query = $this->db->get('university');
								 foreach($query->result() as $row){
						   ?>
						   <option value="<?php echo $row->u_id;?>"><?php echo $row->u_name;?></option>
								 <?php } ?>
						</select>
                    </div> 
                    <p id="edituni_error" style="color:red;"></p>
					
					<label>By Leader:</label>
					<div class="form-group">
                        <select class="form-control" name="editgrp_leader" id="editgrp_leader">
						   <option value="none" selected="" disabled=""></option>
						   <?php $this->db->where('permit !=',0);
								 $this->db->where('type !=',1);
								 $query = $this->db->get('user_table');
								 foreach($query->result() as $row){
						   ?>
						   <option value="<?php echo $row->id;?>"><?php echo $row->user_name;?></option>
								 <?php } ?>
						</select>
                    </div>
                    <p id="editleader_error" style="color:red;"></p>
					
					<label>Group Description:</label>
                    <div class="form-group">
                        <input type="text-area" class="form-control" name="editgrp_desc" id="editgrp_desc" value="<?php echo set_value('editgrp_desc');?>" />
                    </div>
                    <p id="editgroup_desc_error" style="color:red;"></p>
					
					<label>Group Logo:</label>
                    <div class="form-group">
					<input type="hidden" id="logo1" name="logo1" value=""/>
					<input type='file' onchange="readURL1(this);" id="logo" name="logo" />
					<img id="blah1" name="blah1" src="" alt="Your logo" 
						 style="height:150px; width:150px; min-height:90px; min-width:90px;"/>
					</div>
                    <p id="editgroup_logo_error" style="color:red;"></p>
					
					
                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="close">
                    <input type="submit" class="btn btn-outline-primary updategroup_submit" value="Submit" />
                </div>
            </form><p></p>
        </div>
    </div>
</div>
<!--Delete Modal-->
<div class="modal fade text-xs-left" id="delete-group-modal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form action="<?php echo base_url().'groups/delete'; ?>" method="POST" class="delete-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Delete Group</h4>
                </div>
                <div class="modal-body deletepara">
                    <p></p> <!--client information will go through here to jscript-->
                    <input type="hidden" name="del_grp_id" id="del_grp_id" value="" /><!--value will be set through javascript-->
                    <input type="hidden" name="del_grp_name" id="del_grp_name" value="" /><!--do  you really want to delete- set through javascript-->
                    <div id="result">
                        <div id="value"></div>
                        <div id="anothervalue"></div>
                    </div>
                </div>
                </form>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="No">
                    <input type="submit" class="btn grey btn-outline-danger delete-group-submit" value="Yes">
                </div>
            </div>
    </div>
</div>
<!--UnDelete Modal-->
<div class="modal fade text-xs-left" id="undelete-group-modal" tabindex="-1" role="dialog" aria-labelledby="undeleteModal" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form action="<?php echo base_url().'groups/delete'; ?>" method="POST" class="delete-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Activating Group</h4>
                </div>
                <div class="modal-body deletepara">
                    <p></p> <!--client information will go through here to jscript-->
                    <input type="hidden" name="undel_grp_id" id="undel_grp_id" value="" /><!--value will be set through javascript-->
                    <input type="hidden" name="del_grp_name" id="del_grp_name" value="" /><!--activating group...- set through javascript-->
                    
                </div>
                </form>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="No">
                    <input type="submit" class="btn grey btn-outline-danger undelete-group-submit" value="Yes">
                </div>
            </div>
    </div>
</div>

<script>
//DELETE GROUP SCRIPT
$('#delete-group-modal').on('show.bs.modal', function(e) {
   
    //get data-id attribute of the clicked element
    var delgrpid = $(e.relatedTarget).data('id');
    var delgrpname = $(e.relatedTarget).data('gname');
    var modal = $(this);
   
    //populate the textboxes
    $(e.currentTarget).find('input[name="del_grp_id"]').val(delgrpid);
    document.getElementById('del_grp_name').value = delgrpname;
     modal.find('.modal-body p').html('Do you really want to delete - <b>' + delgrpname + '</b>?');
   
});

$(document).ready(function()	{
    $(".delete-group-submit").click(function(event)	{
       event.preventDefault();
       
       var gid = document.getElementById('del_grp_id').value;
      var gname = document.getElementById('del_grp_name').value;
        //alert('deleting '+gname);
        
           jQuery.ajax({
               type: "POST",
               url: "<?php echo site_url('groups/delete_group'); ?>",
               dataType: "json",
               data: {c_id: gid, c_name: gname},
               success: function(res)	{
                   console.log(res);
                   if(res)
                       {   
                           //alert('Group Deleted Successfully');
						   swal(
							  '',
							  'Group deleted successfully!',
							  'success'
							);
                           //$('#delete-client-modal').modal().hide();
                           window.location.href = "<?php echo site_url('groups'); ?>";

                       }
                   }
               });
           });
       });
</script>
<script>
//UNDELETE GROUP SCRIPT
$('#undelete-group-modal').on('show.bs.modal', function(e) {
   
    //get data-id attribute of the clicked element
    var delgrpid1 = $(e.relatedTarget).data('id');
    var delgrpname1 = $(e.relatedTarget).data('gname');
    var modal = $(this);
   
    //populate the textboxes
    $(e.currentTarget).find('input[name="undel_grp_id"]').val(delgrpid1);
    document.getElementById('del_grp_name').value = delgrpname1;
     modal.find('.modal-body p').html('Activate - <b>' + delgrpname1 + '</b>?');
   
});

$(document).ready(function()	{
    $(".undelete-group-submit").click(function(event)	{
       event.preventDefault();
       
       var gid = document.getElementById('undel_grp_id').value;
      var gname = document.getElementById('del_grp_name').value;
       //alert('undelete '+gname);
        
           jQuery.ajax({
               type: "POST",
               url: "<?php echo site_url('groups/undelete_group'); ?>",
               dataType: "json",
               data: {c_id: gid, c_name: gname},
               success: function(res)	{
                   console.log(res);
                   if(res)
                       {   
                           //alert('Group Deleted Successfully');
						   swal(
							  '',
							  'Group Activated successfully!',
							  'success'
							);
                           //$('#delete-client-modal').modal().hide();
                           window.location.href = "<?php echo site_url('groups'); ?>";

                       }
                   }
               });
           });
       });
</script>

<script type="text/javascript">
//ADD GROUP SCRIPT
$(function() { // <----------------missed the doc ready function
    //$('#addgroup_form').submit(function(e) {
	$('#s1').click(function(e) {
        e.preventDefault(); // <------this will restrict the page refresh
		
        /*var form_data = {
            c_name: $('#group_name').val(),
            c_uni: $('#grp_uni').val(),
            c_by: $('#grp_leader').val(),
            };*/
		
		var form = $('#addgroup_form');
            var formData = new FormData(form[0]);
            console.log(formData);
		alert(form);
        $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + 'groups/add_group',
                data: formData,
				processData: false,
				contentType: false,
                success: function(res)  {
                    var json = JSON.parse(res);
                    console.log(json);
                     if(json.st== 'fail'){
                        $('#group_name_error').html(json.group_name);
                        $('#uni_error').html(json.group_uni);
                        $('#leader_error').html(json.group_lead);                                                
                        $('#desc_error').html(json.group_desc);                                                
                        //$('#logo_error').html(json.group_logo);                                                
                    }
					 if(json.st_code== 3){
						 swal(
						  '',
						  'File type not allowed!',
						  'error'
						)
					 }
                    if(json.st =='success') {
                        //alert('Group added successfully');
						swal(
						  '',
						  'Group added successfully!',
						  'success'
						)
                        window.location.href = "<?php echo site_url('groups'); ?>";
                    }
                   
                }
        });

    });

});
</script>

<script>
//EDIT GROUP SCRIPT
//triggered when modal is about to be shown
$('#edit-group-modal').on('show.bs.modal', function(e) {
   
    //get data-id attribute of the clicked element
    var editgrpid   = $(e.relatedTarget).data('id');
    var editgrpname = $(e.relatedTarget).data('name');
    var editgrpuni  = $(e.relatedTarget).data('uni');
    var editgrplead = $(e.relatedTarget).data('lead');
    var editgrpdesc = $(e.relatedTarget).data('desc');
    var editgrplogo = $(e.relatedTarget).data('logo');
    var logopath    = "<?php echo base_url();?>" + editgrplogo;
	//alert(logopath); 
    //populate the textboxes
    //$(e.currentTarget).find('input[name="editgroup_id"]').val(editgrpid);
    document.getElementById('editgroup_name').value  = editgrpname;
    document.getElementById('editgrp_uni').value     = editgrpuni;
    document.getElementById('editgrp_leader').value  = editgrplead;
    document.getElementById('editgrp_desc').value    = editgrpdesc;
    //$(e.currentTarget).find('input[name="logo1"]').val(editgrplogo);
    //$(e.currentTarget).find('img[name="blah1"]').val(editgrplogo); 
	$('#blah1').attr('src', logopath);	
});

$(document).ready(function()	{
$(".updategroup_submit").click(function(e)	{
        e.preventDefault(); //this will restrict the page refresh
        /* var form_data = {
            grp_id:$('#editgroup_id').val(),
            grp_name: $('#editgroup_name').val(),
            grp_uni: $('#editgrp_uni').val(),
            grp_lead: $('#editgrp_leader').val(),
           
        }; */
		    var form = $('#editgroup_form');
            var formData = new FormData(form[0]);
            console.log(formData);
			//alert(form_data.grp_id+" id "+form_data.grp_name);
			//var form1 = new FormData(this);
			//var form2 = $('#editclient_form').serialize();
    jQuery.ajax({
        type: "POST",
		processData: false,
		contentType: false,
        data:formData,
       // data: {gid: form_data.grp_id, gname:form_data.grp_name, guni:form_data.grp_uni, glead:form_data.grp_lead },
        url: "<?php echo site_url('groups/update_group'); ?>",
        success: function(res)  {
            var json = JSON.parse(res);
            console.log(json);
             if(json.st== 'fail'){
                $('#editgroup_name_error').html(json.g_name);
                $('#edituni_error').html(json.g_uni);
                $('#editleader_error').html(json.g_lead);
                $('#editgroup_desc_error').html(json.g_desc);
                }
            if(json.st =='success') {
                //alert('Group updated successfully');
				swal(
				  '',
				  'Group updated successfully!',
				  'success'
				);
				//setTimeout(function, milliseconds);
                window.location.href = "<?php echo site_url('groups'); ?>";
                }
        }
        });
    });
});
</script>
<script>
    $(document).ready(function() {
        
        $('#group-table').DataTable();
    });
</script>
<script>
     function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(150);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
<script>
     function readURL1(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah1')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(150);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>