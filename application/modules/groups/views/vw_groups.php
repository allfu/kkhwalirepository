<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-xs-12 mb-1">
                <h2 class="content-header-title">Manage Groups</h2>
            </div>
            <div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
                <div class="breadcrumb-wrapper col-xs-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url().'admin'; ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo base_url().'groups'; ?>">Groups</a></li>
                    </ol>
                </div>
            </div>
        </div> 
        <div class="content-body"><!--Main content body section start-->
            <section id="card-actions">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Group information</h4>
                                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <!--<li><a href="#" data-toggle="modal" data-target="#add-product-modal"><i class="icon-plus4"></i> Add Client   </a></li>-->
                                        <li><button type="button" class="btn btn-info btn-block"
                                                    data-toggle="modal" data-target="#add-group-modal">
                                            <i class="icon-plus4" style="color:white;"></i>   Add Group</button>
                                        </li>
                                        <!--<li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                        <li><a data-action="reload"><i class="icon-reload"></i></a></li>-->
                                    </ul>
                                </div>
                            </div>
                            <div class="card-body collapse in">
                                <div class="card-block">
                                    <div class="row">
                                        <div class="">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped ">
                                                    <thead>
                                                        <tr>
                                                            <th>Group Name</th>
                                                            <th>Created By</th>
                                                            <th>Created on</th>
                                                            <th>Permit</th>
                                                            <th>Actions</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach($groups->result() as $row)    {
                                                        ?>
                                                        <tr>
                                                        <td class="text-middle"><?php echo $row->c_name; ?></td>
                                                        <td class="text-middle"><?php echo $row->created; ?></td>
                                                        <td class="text-middle"><?php if($row->permit == '1')  { ?>
                                                        <span class="tag tag-success">Active</span><?php } else { ?>
                                                        <span class="tag tag-danger">Banned</span>
                                                        <?php } ?></td>
                                                        <td><span class="btn-group">
                                                            <!--<input type="button" class="btn btn-outline-info" value="View" data-toggle="modal" data-target="#view-modal"/>-->

                                                            <input type="button" class="btn btn-outline-primary" value="Edit" 
                                                            data-toggle="modal" data-target="#edit-group-modal" 
                                                            data-id="<?php echo $row->c_id;?>" data-name="<?php echo $row->c_name; ?>" 
                                                            data-size="<?php //echo $row->p_size; ?>"/>

                                                            <input type="button" class="btn btn-outline-danger" 
                                                                   <?php 
                                                                    if($row->permit==0)
                                                                    { echo "value='Deleted'";
                                                                    }
                                                                   else{ 
                                                                    echo "value='Delete'"; ?>
                                                                   data-toggle="modal" data-target="#delete-group-modal" 
                                                                   data-id="<?php echo $row->c_id;?>" 
                                                                   data-gname="<?php echo $row->c_name; ?>" />
                                                                   <?php } ?>
                                                            </span>
                                                        </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                                <?php echo $pagination; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!--Add Modal-->
<div class="modal fade text-xs-left" id="add-group-modal" tabindex="-1" role="dialog" aria-labelledby="addModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Group Information</h4>
            </div>
            <form action="<?php //echo base_url().'clients/add_client';?>" method="post" id="addgroup_form">
                <div class="modal-body">
                    <label>Group Name:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="group_name" value="<?php echo set_value('group_name');?>" id="group_name" />
                        <p id="group_name_error" style="color:red;"></p>
                    </div>
                    
                    
                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-outline-primary" value="Submit" />
                </div>
            </form>
        </div>
    </div>
</div>
<!--Edit Modal-->
<div class="modal fade text-xs-left" id="edit-group-modal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Group Information</h4>
            </div>
            <form method="post" id="editgroup_form" name="editgroup_form" action="">
                <div class="modal-body">
                    <input type="hidden" name="editgroup_id" id="editgroup_id" value="<?php echo set_value('editgroup_id');?>" /><!--value will be set through javascript-->
                    <label>Group Name:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="editgroup_name" id="editgroup_name" value="<?php echo set_value('editgroup_name');?>" />
                    </div>
                    <p id="editgroup_name_error" style="color:red;"></p>
                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="close">
                    <input type="submit" class="btn btn-outline-primary updategroup_submit" value="Submit" />
                </div>
            </form><p></p>
        </div>
    </div>
</div>
<!--Delete Modal-->
<div class="modal fade text-xs-left" id="delete-group-modal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form action="<?php echo base_url().'groups/delete'; ?>" method="POST" class="delete-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Delete Group</h4>
                </div>
                <div class="modal-body deletepara">
                    <p></p> <!--client information will go through here to jscript-->
                    <input type="hidden" name="del_grp_id" id="del_grp_id" value="" /><!--value will be set through javascript-->
                    <input type="hidden" name="del_grp_name" id="del_grp_name" value="" /><!--do  you really want to delete- set through javascript-->
                    <div id="result">
                        <div id="value"></div>
                        <div id="anothervalue"></div>
                    </div>
                </div>
                </form>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="No">
                    <input type="submit" class="btn grey btn-outline-danger delete-group-submit" value="Yes">
                </div>
            </div>
    </div>
</div>


<script>
//DELETE GROUP SCRIPT
$('#delete-group-modal').on('show.bs.modal', function(e) {
   
    //get data-id attribute of the clicked element
    var delgrpid = $(e.relatedTarget).data('id');
    var delgrpname = $(e.relatedTarget).data('gname');
    var modal = $(this);
   
    //populate the textboxes
    $(e.currentTarget).find('input[name="del_grp_id"]').val(delgrpid);
    document.getElementById('del_grp_name').value = delgrpname;
     modal.find('.modal-body p').html('Do you really want to delete - <b>' + delgrpname + '</b>?');
   
});

$(document).ready(function()	{
    $(".delete-group-submit").click(function(event)	{
       event.preventDefault();
       
       var gid = document.getElementById('del_grp_id').value;
      var gname = document.getElementById('del_grp_name').value;
        //alert('deleting '+gname);
        
           jQuery.ajax({
               type: "POST",
               url: "<?php echo site_url('groups/delete_group'); ?>",
               dataType: "json",
               data: {c_id: gid, c_name: gname},
               success: function(res)	{
                   console.log(res);
                   if(res)
                       {   
                           alert('Group Deleted Successfully');
                           //$('#delete-client-modal').modal().hide();
                           window.location.href = "<?php echo site_url('groups'); ?>";

                       }
                   }
               });
           });
       });
</script>

<script type="text/javascript">
//ADD GROUP SCRIPT
$(function() { // <----------------missed the doc ready function
    $('#addgroup_form').submit(function(e) {
        e.preventDefault(); // <------this will restrict the page refresh
        var form_data = {
            group_name: $('#group_name').val(),
            };
        //alert(form_data.group_name);
        $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + 'groups/add_group',
                data: form_data,
                success: function(res)  {
                    var json = JSON.parse(res);
                    console.log(json);
                     if(json.st== 'fail'){
                        $('#group_name_error').html(json.group_name);
                                                
                    }
                   
                    if(json.st =='success') {
                        alert('Group added successfully');
                        window.location.href = "<?php echo site_url('groups'); ?>";
                    }
                   
                }
        });

    });

});
</script>

<script>
//EDIT GROUP SCRIPT
//triggered when modal is about to be shown
$('#edit-group-modal').on('show.bs.modal', function(e) {
   
    //get data-id attribute of the clicked element
    var editgrpid = $(e.relatedTarget).data('id');
    var editgrpname = $(e.relatedTarget).data('name');
   
    
    //populate the textboxes
    $(e.currentTarget).find('input[name="editgroup_id"]').val(editgrpid);
    document.getElementById('editgroup_name').value = editgrpname;
    
    
});

$(document).ready(function()	{
$(".updategroup_submit").click(function(e)	{
        e.preventDefault(); //this will restrict the page refresh
        var form_data = {
            grp_id:$('#editgroup_id').val(),
            grp_name: $('#editgroup_name').val(),
           
        };
        //alert(form_data.grp_id+" id "+form_data.grp_name);
    //var form1 = new FormData(this);
    //var form2 = $('#editclient_form').serialize();
    jQuery.ajax({
        type: "POST",
        //data:form_data,
        data: {gid: form_data.grp_id, gname: form_data.grp_name},
        url: "<?php echo site_url('groups/update_group'); ?>",
        success: function(res)  {
            var json = JSON.parse(res);
            console.log(json);
             if(json.st== 'fail'){
                $('#editgroup_name_error').html(json.g_name);
                }
            if(json.st =='success') {
                alert('Group updated successfully');
                window.location.href = "<?php echo site_url('groups'); ?>";
                }
        }
        });
    });
});
</script>