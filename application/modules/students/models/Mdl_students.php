<?php 
class Mdl_Students extends CI_Model
{
    function get_students($limit, $offset) 
    {
        if($offset > 0) {
            $offset = ($offset - 1) * $limit;
        }
        $result['studs'] = $this->db->get('user_table', $limit, $offset);
        $result['rows'] = $this->db->count_all_results('user_table');
        return $result;
    }
    
	 public function get_stud()
     {
          $query= $this->db->get("user_table");
          $res['data'] = $query->result();
         return $res;
     }
	 
    function add_student($data)
    {
        $this->db->insert('user_table',$data);
        $insert_id = $this->db->insert_id();
		return  $insert_id;
        
        
    }
    function update_studentdata($id,$data)
    {
        $this->db->where('id', $id);
        $q = $this->db->update('user_table', $data);
        //alert("hieeee"+$q);
         //return $q->result();
    }
    
    function delete_student($id)
    {
        $info=array('permit'=>0,'modified'=>date('Y-m-d'));
        $this->db->where('id', $id);
        $this->db->update('user_table',$info );
    }
	
	function add_groupdata($sid,$cid)
	{
		$data= array(
				'gstudid'=>$sid,
				'gclubid'=>$cid,
				'created'=>date('Y-m-d'));
		$this->db->insert('group_table',$data);
		
	}
}
?>