<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Students extends MX_Controller
{
    public function __Construct() 
    {
        parent::__Construct();
		 $this->clear_cache();
        $this->load->model('mdl_students');
		//if (!isset($this->session->userdata['logged_in']))
		//{redirect('user_authentication','refresh');}
    }
    public function index()
    {
        redirect('students/show_data');
	}
	
	 function clear_cache()
    {
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
    }
	
	function student_home()
	{
		$this->load->view('vw_adminheader');
		$this->load->view('vw_adminmenu');
		$this->load->view('vw_adminhome');
		$this->load->view('vw_adminfooter');
	}
	function show_data()
	{
        $result = $this->mdl_students->get_stud();
        $res['data'] = $result['data'];
		$this->load->view('vw_adminheader');
		$this->load->view('vw_adminmenu');
		//$this->load->view('vw_adminhome');
		  $this->load->view("vw_search",$res);
		$this->load->view('vw_adminfooter');
	}
	
    function show_students($offset = 0)
    {
        $limit = 10;
        $result = $this->mdl_students->get_students($limit, $offset);
        $data['studs'] = $result['studs'];
        $data['rows'] = $result['rows'];
        //load pagination library
        $config = array();
        $config['base_url'] = base_url().'students/show_students';
        $config['total_rows'] = $data['rows'];
        $config['per_page'] = $limit;
        //Uri segment indicates pagination number
        $config['uri_segment'] = 3;
        $config['use_page_numbers'] = TRUE;
        //max links on a page will be shown
        $config['num_links'] = 5;
        //various pagination configuration
        $config['full_tag_open'] = '<div class="text-xs-right"><ul class="pagination text-right">';
        $config['full_tag_close'] = '</ul></div>';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['first_link'] = 'First';
        $config['last_tag_open'] = '<span class="last">';
        $config['last_tag_close'] = '</span>';
        $config['last_link'] = 'Last';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        
        $this->load->view('vw_adminheader');
        $this->load->view('vw_adminmenu');
        $this->load->view('vw_students', $data);
        $this->load->view('vw_adminfooter');

    }
     function add_student()
    {
        $this->form_validation->set_rules('stud_name', 'Username', 'trim|required');
        $this->form_validation->set_rules('stud_fname', 'Firstname', 'trim|required');
        $this->form_validation->set_rules('stud_lname', 'Lastname', 'trim|required');
        $this->form_validation->set_rules('stud_contact', 'Contact No', 'trim|required');
        $this->form_validation->set_rules('stud_email', 'Email', 'trim|required');
        $this->form_validation->set_rules('stud_website', 'Website', 'trim|required');
        $this->form_validation->set_rules('stud_password', 'Password', 'trim|required|min_length[6]');
        $this->form_validation->set_rules('stud_grp', 'Group Name', 'required|trim');
        $this->form_validation->set_rules('stud_school', 'School Information', 'required|trim');
        $this->form_validation->set_rules('stud_yr', 'Year of Study', 'required|trim|numeric');
        $this->form_validation->set_rules('stud_type', 'Type of Student', 'required|trim');
        
        
        $data = null;
        if($this->form_validation->run() == false)
        {
             $data = array(
                'st'		    => 'fail',
                'user_name'     => form_error('stud_name'),
                'first_name'    => form_error('stud_fname'),
                'last_name'     => form_error('stud_lname'),
                'contact'       => form_error('stud_contact'),
                'email'         => form_error('stud_email'),
                'website'       => form_error('stud_website'),
                'pass'          => form_error('stud_password'),
                'grp'          => form_error('stud_grp'),
                //'re_pass'       => form_error('re_pass'),
                'school'        => form_error('stud_school'),
                'yr'            => form_error('stud_yr'),
                'type'            => form_error('stud_type'),
                
                   );
		      echo json_encode($data);
        }
        else {
        $data = array(
            
            'user_name'  => $this->input->post('stud_name'),
            'user_email'  => $this->input->post('stud_email'),
            'user_password'  => $this->input->post('stud_password'),
			//'grp'          => $this->input->post('stud_grp'),
            'firstname'  => $this->input->post('stud_fname'),
            'lastname'  => $this->input->post('stud_lname'),
            'contact_no'  => $this->input->post('stud_contact'),
            'website'  => $this->input->post('stud_website'),
            'school'  => $this->input->post('stud_school'),
            'yr'  => $this->input->post('stud_yr'),
            'type'  => $this->input->post('stud_type'),
            'created'  => date('Y-m-d'),
                );
        $studid = $this->mdl_students->add_student($data);
		$this->mdl_students->add_groupdata($studid,$this->input->post('stud_grp'));
        $return_json = array('st'=> 'success');
        echo json_encode($return_json);
        
        }
    }
   function validate_email($email) 
   {
	   if(! preg_match("/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/", $email))	
	   {
		   $this->form_validation->set_message('validate_email', 'Please enter a valid Email.');			
		   return FALSE;		
	   }else{
		   return TRUE;
		   }	
	}
    
    function update_student()
    {
          //  echo json_encode(array('st'=>$_POST));
        
        $this->form_validation->set_rules('user_name', 'Username', 'trim|required');
        $this->form_validation->set_rules('firstname', 'Firstname', 'trim|required');
        $this->form_validation->set_rules('lastname', 'Lastname', 'trim|required');
        $this->form_validation->set_rules('contact_no', 'contact No', 'trim|required');
        $this->form_validation->set_rules('user_email', 'Email', 'trim|required');
        $this->form_validation->set_rules('website', 'Website', 'trim|required');
        $this->form_validation->set_rules('user_password', 'Password', 'trim|required|min_length[6]');
        //$this->form_validation->set_rules('re_pass', 'Confirm Password', 'required|trim|matches[pass]');
        $this->form_validation->set_rules('school', 'School Information', 'required|trim');
        $this->form_validation->set_rules('yr', 'Year of Study', 'required|trim');
        if($this->form_validation->run() == false)
        {
              $data = array(
                'st'		    => 'fail',
                'user_name'     => form_error('user_name'),
                'first_name'    => form_error('firstname'),
                'last_name'     => form_error('lastname'),
                'contact'       => form_error('contact_no'),
                'email'         => form_error('user_email'),
                'website'       => form_error('website'),
                'pass'          => form_error('user_password'),
                //'re_pass'       => form_error('re_pass'),
                'school'        => form_error('school'),
                'yr'            => form_error('yr'),
                );
          
		      echo json_encode($data);
            // echo json_encode(array('st'=>'fail',));
        }
        else {
            $id= $this->input->post('id');
            $data = array(
            'user_name'     => $this->input->post('user_name'),
            'firstname'     => $this->input->post('firstname'),
            'lastname'      => $this->input->post('lastname'),
            'user_email'    => $this->input->post('user_email'),
            'user_password' => $this->input->post('user_password'),
            'contact_no'    => $this->input->post('contact_no'),
            'website'       => $this->input->post('website'),
            'school'        => $this->input->post('school'),
            'yr'          => $this->input->post('yr'),
            'modified'  => date('Y-m-d'),    
                );
        $this->mdl_students->update_studentdata($id,$data);
            
        $return_json = array('st'=> 'success','name'=> $this->input->post('user_name'));
        echo json_encode($return_json);
        }
    }
    
    function delete_student()
    {
         $d = array(
            'id' => $this->input->post('uid'),
            'user_name' => $this->input->post('uname'),
                );
        $id= $this->input->post('uid');
        $query = $this->mdl_students->delete_student($id);
        echo json_encode($d);
    }
	 public function student_page()
     {

          // Datatables Variables
          $draw = intval($this->input->get("draw"));
          $start = intval($this->input->get("start"));
          $length = intval($this->input->get("length"));


          $student = $this->mdl_students->get_stud();

          $data = array();

          foreach($student->result() as $r) {

               $data[] = array(
                    $r->user_name,
                    $r->firstname,
                    $r->lastname,
                    $r->user_email ,
					$r->user_password,
					$r->contact_no,
					$r->website,
					$r->school,
					$r->yr,
					$r->modified
                  
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $student->num_rows(),
                 "recordsFiltered" => $student->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
     }
    
    
}
?>