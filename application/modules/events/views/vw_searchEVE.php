<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12">
				<button type="button" class="btn btn-info" style="float: right; margin-right: 1em;" data-toggle="modal" data-target="#add-event-modal"><i class="icon-plus4" style="color:white;"></i>Add Event</button>
                <h1 style="margin-left: 0.4em;">Event List</h1>
                 <div class="heading-elements">
                                   <!--<ul class="list-inline mb-0">
                                         
                                        <li>
                                        </li> -->
										 
                                        <!--<li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                        <li><a data-action="reload"><i class="icon-reload"></i></a></li>-->
                                    <!--</ul>-->
                                </div>
									<table id="event-table" class="table table-bordered table-striped table-hover">
											<thead>
											<tr>
                                            <th>Event Name</th>
											<th>Description</th>
											<th>Start</th>
											<th>End</th>
											<th>Location</th>	
											<th>Type</th>
											<th>FB Link</th>
											<th>Insta Link</th>
											<th>By</th> 					 
											<th>Permit</th>
											<th>Actions</th>
												</tr>
                
											</thead>
            <tbody>
                
										<?php foreach($data as $row)    {
                                                        ?>
                                                        <tr>
										<td class="text-middle"><?php echo $row->e_name; ?></td>
										<td class="text-middle"><?php echo $row->e_desc; ?></td>
										<td class="text-middle"><?php echo $row->e_start; ?></td>
										<td class="text-middle"><?php echo $row->e_end; ?></td>
										<td class="text-middle"><?php echo $row->e_loc; ?></td>
										<td class="text-middle"><?php echo $row->e_type; ?></td>
										<td class="text-middle"><?php echo $row->e_fblink; ?></td>
										<td class="text-middle"><?php echo $row->e_instalink; ?></td>
										
                                                        <?php //$this->db->where('permit!=',0);
														$query= $this->db->get_where('club_table',array('c_id' => $row->e_by));
  													    $g=$query->row()->c_name;
														?>
														<td class="text-middle"><?php echo $g; ?></td>
                                                        <td class="text-middle"><?php if($row->permit == '1')  { ?>
                                                        <span class="tag tag-success">Active</span><?php } else { ?>
                                                        <span class="tag tag-danger">Banned</span>
                                                        <?php } ?></td>
                                                        <td><span class="btn-group">
                                                            <!--<input type="button" class="btn btn-outline-info" value="View" data-toggle="modal" data-target="#view-modal"/>-->
															 <?php $query=$this->db->get_where('club_table',array('c_id'=>$row->e_by)); $i=$query->row(); 
															$grpby=$i->c_name;?>
                                                            <input type="button" class="btn btn-outline-primary" value="Edit" 
                                                            data-toggle="modal" data-target="#edit-event-modal"
                                                            data-id="<?php echo $row->e_id;?>"
															data-name="<?php echo $row->e_name; ?>" 
                                                            data-desc="<?php echo $row->e_desc; ?>"
															data-start="<?php echo $row->e_start;?>"
															data-end="<?php echo $row->e_end;?>" 
															data-loc="<?php echo $row->e_loc;?>" 
															data-type="<?php echo $row->e_type;?>" 
															data-fblink="<?php echo $row->e_fblink;?>" 
															data-instalink="<?php echo $row->e_instalink;?>"
															data-by="<?php echo $row->e_by;?>" 															
															data-grp = <?php echo $grpby;?>
															/> 
															 <input type="button" class="btn btn-outline-danger" 
                                                                   <?php 
                                                                    if($row->permit==0)
                                                                    { echo "value='Deleted'";
                                                                    }
                                                                   else{ 
                                                                    echo "value='Delete'";  ?>
                                                                   data-toggle="modal" data-target="#delete-event-modal" 
                                                                   data-id="<?php echo $row->e_id;?>" 
                                                                   data-ename="<?php echo $row->e_name; ?>" />
                                                                   <?php } ?>
                                                            </span>
                                                        </td>
														 
                                                        </tr>
                                                        <?php } ?>
														</tbody>
														</table>
														</div>
														</div>
														</div>
														</div>
<!--Add Modal-->
<div class="modal fade text-xs-left" id="add-event-modal" tabindex="-1" role="dialog" aria-labelledby="addModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Event Information</h4>
            </div>
            <form action="<?php //echo base_url().'clients/add_client';?>" method="post" id="addevent_form">
                <div class="modal-body">
                    <label> Event Name:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="event_name" value="<?php echo set_value('event_name');?>" id="event_name" />
                        <p id="event_name_error" style="color:red;"></p>
                    </div>
                    <label>Description:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="event_desc" value="<?php echo set_value('event_desc');?>" id="event_desc" />
                        <p id="event_desc_error" style="color:red;"></p>
                    </div>
                    <label>Start:</label>
                    <div class="form-group">
							
								<input type="text" class="form-control" name="event_start" id="event_start" />
								
							
						<p id="event_start_error" style="color:red;"></p>
					</div>
                    <label>End:</label>
                     <div class="form-group">
						  
								<input type="text" class="form-control" name="event_end" id="event_end" />
								
							<p id="event_end_error" style="color:red;"></p>
					</div>
					<label>Location:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="event_loc" id="event_loc" value="<?php echo set_value('event_loc');?>"  />
                      <p id="event_loc_error" style="color:red;"></p>
					</div>
                    <label>Type:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="event_type" id="event_type" value="<?php echo set_value('event_type');?>"   />
						 <p id="event_type_error" style="color:red;"></p>
                    </div>
                   
					
					<label>FB Link:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="event_fblink" id="event_fblink" value="<?php echo set_value('event_fblink');?>"   />
						   <p id="event_fb_error" style="color:red;"></p>
                    </div>
                   <label>Insta Link:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="event_instalink" id="event_instalink" value="<?php echo set_value('event_instalink');?>"  />
						 <p id="event_insta_error" style="color:red;"></p>
                    </div>
                   
                    <label>By:</label>
                    <div class="form-group">
                       <!-- <input type="text" class="form-control" name="event_by" value="<?php echo set_value('event_by');?>" id="event_by" />-->
						 <select class="form-control" name="event_by" id="event_by">
						   <option value="none" selected="" disabled=""></option>
						   <?php $this->db->where('permit !=',0);
								 $query = $this->db->get('club_table');
								 foreach($query->result() as $row){
						   ?>
						   <option value="<?php echo $row->c_id;?>"><?php echo $row->c_name;?></option>
								 <?php } ?>
						</select>
                        <p id="event_by_error" style="color:red;"></p>
                    </div>
                    
                    
                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-outline-primary" value="Submit" />
                </div>
            </form>
        </div>
    </div>
</div>
<!--Edit Modal-->
<div class="modal fade text-xs-left" id="edit-event-modal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Event Information</h4>
            </div>
            <form method="post" id="editeventform" name="editeventform" action="">
                <div class="modal-body">
                    <input type="hidden" name="editevent_id" id="editevent_id" value="<?php echo set_value('editevent_id');?>" /><!--value will be set through javascript-->
                    
					<label>Event Name:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="editevent_name" id="editevent_name" value="<?php echo set_value('editevent_name');?>" />
                    </div>
                    <p id="editevent_name_error" style="color:red;"></p>
                    
					<label>Description:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="edit_eventdesc" id="edit_eventdesc" value="<?php echo set_value('edit_eventdesc');?>" />
                    </div>
                    <p id="edit_eventdesc_error" style="color:red;"></p>
                    
					<label>Start:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="edit_eventstart" id="edit_eventstart" value="<?php echo set_value('edit_eventstart');?>" />
                    </div>
                    <p id="edit_eventstart_error" style="color:red;"></p>
                    
					<label>End:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="edit_eventend" id="edit_eventend" value="<?php echo set_value('edit_eventend');?>"  />
                    </div>
                    <p id="edit_eventend_error" style="color:red;"></p>
					
					<label>Location:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="edit_eventloc" id="edit_eventloc" value="<?php echo set_value('edit_eventloc');?>"   />
                    </div>
                    <p id="edit_eventloc_error" style="color:red;"></p>
					
					<label>Type:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="edit_eventtype" id="edit_eventtype" value="<?php echo set_value('edit_eventtype');?>"  />
                    </div>
                    <p id="edit_eventtype_error" style="color:red;"></p>
					
					<label>FB Link:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="edit_eventfblink" id="edit_eventfblink" value="<?php echo set_value('edit_eventfblink');?>"   />
                    </div>
                    <p id="edit_eventfblink_error" style="color:red;"></p>
					
					<label>Insta Link:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="edit_eventinstalink" id="edit_eventinstalink" value="<?php echo set_value('edit_eventinstalink');?>"  />
                    </div>
                    <p id="edit_eventinsta_error" style="color:red;"></p>
					
					<label>By:</label>
                    <div class="form-group">
                        <!--<input type="text" class="form-control" name="edit_eventby" id="edit_eventby" value="<?php echo set_value('edit_eventby');?>"   />-->
						 <select class="form-control" name="edit_eventby" id="edit_eventby">
						   <option value="none" selected="" disabled=""></option>
						   <?php $this->db->where('permit !=',0);
								 $query = $this->db->get('club_table');
								 foreach($query->result() as $row){
						   ?>
						   <option value="<?php echo $row->c_id;?>"><?php echo $row->c_name;?></option>
								 <?php } ?>
						</select>
                    </div>
                    <p id="edit_eventby_error" style="color:red;"></p>
                    
                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="close">
                    <input type="submit" class="btn btn-outline-primary updateevent_submit" value="Submit" />
                </div>
            </form><p></p>
        </div>
    </div>
</div>
<!--Delete Modal-->
<div class="modal fade text-xs-left" id="delete-event-modal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form action="<?php echo base_url().'events/delete'; ?>" method="POST" class="delete-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Delete Event</h4>
                </div>
                <div class="modal-body deletepara">
                    <p></p> <!--client information will go through here to jscript-->
                    <input type="hidden" name="del_eid" id="del_eid" value="" /><!--value will be set through javascript-->
                    <input type="hidden" name="del_ename" id="del_ename" value="" /><!--value will be set through javascript-->
                    <div id="result">
                        <div id="value"></div>
                        <div id="anothervalue"></div>
                    </div>
                </div>
                </form>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="No">
                    <input type="submit" class="btn grey btn-outline-danger delete-event-submit" value="Yes">
                </div>
            </div>
    </div>
</div>
		 <script>
		  $(document).ready(function()
		  {
			   var minDate = new Date();
			  $("#event_start").datepicker({
				   dateFormat:"yy-mm-dd",
				   showAnim:'drop',
				   minDate :minDate,
				   onClose:function(selectedDate)
				   {
					   $('#event_end').datepicker("option","minDate",selectedDate);
				   }
				   
				   });
		  });
		  
		  $(function()
		  { 
		  var minDate1 = new Date();
			  $("#event_end").datepicker(
			  { 
				   dateFormat:"yy-mm-dd",
				   showAnim:'drop',
				   minDate :minDate1,
				   onClose:function(selectedDate1)
				   {
					   $('#event_start').datepicker("option","minDate1",selectedDate1);
				   }
			  });
		  });
		  </script>
<script>
             
//DELETE EVENT SCRIPT
$('#delete-event-modal').on('show.bs.modal', function(e) {
   
    //get data-id attribute of the clicked element
    var deleid = $(e.relatedTarget).data('id');
    var delename = $(e.relatedTarget).data('ename');
    var modal = $(this);
   
    //populate the textboxes
    $(e.currentTarget).find('input[name="del_eid"]').val(deleid);
    document.getElementById('del_ename').value = delename;
     modal.find('.modal-body p').html('Do you really want to delete - <b>' + delename + '</b>?');
   
});

$(document).ready(function()	{
    $(".delete-event-submit").click(function(event)	{
       event.preventDefault();
       
       var e_id = document.getElementById('del_eid').value;
        var e_name = document.getElementById('del_ename').value;
        alert('deleting');
           jQuery.ajax({
               type: "POST",
               url: "<?php echo site_url('events/delete_event'); ?>",
               dataType: "json",
               data: {id: e_id, name: e_name},
               success: function(res)	{
                   console.log(res);
                   if(res)
                       {   
                           alert('Event Deleted Successfully');
                           //$('#delete-client-modal').modal().hide();
                           window.location.href = "<?php echo site_url('events'); ?>";

                       }
                   else{
                       alert('Delete not successful');
                       //window.location.href = "<?php echo site_url('events'); ?>";
                   }
                   }
               });
           });
       });
</script>

<script type="text/javascript">
//ADD EVENT SCRIPT
$(function() { // <----------------missed the doc ready function
    $('#addevent_form').submit(function(e) {
		
        e.preventDefault(); // <------this will restrict the page refresh
        var form_data = {
              e_name: $('#event_name').val(),
              e_desc: $('#event_desc').val(),
              e_start: $('#event_start').val(),
              e_end: $('#event_end').val(),
              e_loc: $('#event_loc').val(),
              e_type: $('#event_type').val(),
              e_fblink: $('#event_fblink').val(),
              e_instalink: $('#event_instalink').val(),
			  e_by: $('#event_by').val(),
            
        };
        //alert(form_data.e_name);
        $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + 'events/add_event',
                data: form_data,
                success: function(res)  {
                    var json = JSON.parse(res);
                    console.log(json);
                     if(json.st== 'fail'){
                        $('#event_name_error').html(json.e_name);
                        $('#event_desc_error').html(json.e_desc);
                        $('#event_start_error').html(json.e_start);
                        $('#event_end_error').html(json.e_end);
                        $('#event_loc_error').html(json.e_loc);
                        $('#event_type_error').html(json.e_type);
                        $('#event_fb_error').html(json.e_fblink);
                        $('#event_insta_error').html(json.e_instalink);
                        $('#event_by_error').html(json.e_by);
                                                       
                    }
                   
                   if(json.st =='success'){
                        
                        alert('Event added successfully');
                        window.location.href = "<?php echo site_url('events'); ?>";
                    }
                   
                }
        });

    });

});
</script>

<script>
//EDIT EVENT SCRIPT
//triggered when modal is about to be shown
$('#edit-event-modal').on('show.bs.modal', function(e) {
   
    //get data-id attribute of the clicked element
    var editeventid = $(e.relatedTarget).data('id');
    var editeventname = $(e.relatedTarget).data('name');
    var editeventdesc = $(e.relatedTarget).data('desc');
    var editeventstart = $(e.relatedTarget).data('start');
    var editeventlend = $(e.relatedTarget).data('end');
    var editeventloc = $(e.relatedTarget).data('loc');
    var editeventtype = $(e.relatedTarget).data('type');
    var editeventfblink = $(e.relatedTarget).data('fblink');
    var editeventinstalink = $(e.relatedTarget).data('instalink');
    var editeventby = $(e.relatedTarget).data('by');
    var editeventbygrp = $(e.relatedTarget).data('grp');
   
    //alert(editeventid);
    //populate the textboxes
     /* $(e.currentTarget).find('input[name="editevent_id"]').val(editeventid);
    document.getElementById('editevent_nam1').value = editeventname;
    document.getElementById('edit_eventname').value = editeventdesc;
    document.getElementById('edit_eventdesc').value = editeventdesc;
    document.getElementById('edit_eventstart').value = editeventstart;
    document.getElementById('edit_eventend').value = editeventlend;  */
	$('#editevent_id').val(editeventid);
	$('#editevent_name').val(editeventname);
	$('#edit_eventdesc').val(editeventdesc);
	$('#edit_eventstart').val(editeventstart);
	$('#edit_eventend').val(editeventlend);
	$('#edit_eventloc').val(editeventloc);
	$('#edit_eventtype').val(editeventtype);
	$('#edit_eventfblink').val(editeventfblink);
	$('#edit_eventinstalink').val(editeventinstalink);
	$('#edit_eventby').val(editeventby);
	$('#edit_eventbygrp').val(editeventbygrp);
	

    
});

$(document).ready(function()	{
$(".updateevent_submit").click(function(e)	{
        e.preventDefault(); //this will restrict the page refresh
        var form_data = {
            e_id:$('#editevent_id').val(),
            e_name: $('#editevent_name').val(),
            e_desc: $('#edit_eventdesc').val(),
            e_start: $('#edit_eventstart').val(),
            e_end: $('#edit_eventend').val(),
            e_loc: $('#edit_eventloc').val(),
            e_type: $('#edit_eventtype').val(),
            e_fblink: $('#edit_eventfblink').val(),
            e_instalink: $('#edit_eventinstalink').val(),
            e_by: $('#edit_eventby').val(),
            
        };
    //alert(form_data.stud_id+" id "+form_data.stud_uname + form_data.stud_email + form_data.stud_website);
    //var form1 = new FormData(this);working till here.
    //var form2 = $('#editclient_form').serialize();
    $.ajax({
        type: "POST",
        //data:form_data,
        data: {
            e_id: form_data.e_id, 
            e_name: form_data.e_name, 
            e_desc:form_data.e_desc, 
            e_start: form_data.e_start,
            e_end:form_data.e_end,
            e_loc:form_data.e_loc,
            e_type:form_data.e_type,
            e_fblink:form_data.e_fblink,
            e_instalink:form_data.e_instalink,
            e_by:form_data.e_by,
            
        },
        
        url: "<?php echo site_url('events/update_event'); ?>",
        
        success: function(res)  {
        var json = JSON.parse(res);
        console.log(json);
        //alert('herehere');
        
        if(json.st== 'fail'){
                $('#editevent_name_error').html(json.e_name);
                $('#edit_eventdesc_error').html(json.e_desc);
                $('#edit_eventstart_error').html(json.e_start);
                $('#edit_eventend_error').html(json.e_end);                                    
                $('#edit_eventloc_error').html(json.e_loc);                                    
                $('#edit_eventtype_error').html(json.e_type);                                    
                $('#edit_eventfblink_error').html(json.e_fblink);                                    
                $('#edit_eventinsta_error').html(json.e_instalink);                                    
                $('#edit_eventby_error').html(json.e_by);                                    
                
                
                }
        if(json.st =='success') {
               
                console.log(json);
                alert('Event updated successfully');
                window.location.href = "<?php echo site_url('events'); ?>";
                }
        }
        });
    });
});

 $(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
<script>
$('div.dataTables_filter input').addClass('form-control form-control-md');
    $(document).ready(function() {
        $('#event-table').DataTable();
		$('#event-table_filter input').removeClass('form-control-sm');
		$('#event-table_filter input').addClass('form-control-md');
		$('#event-table_length select').removeClass('form-control-sm');
		$('#event-table_length select').addClass('form-control-md'); 
    });
</script>
                