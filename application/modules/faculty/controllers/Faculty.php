<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Faculty extends MX_Controller
{
    public function __Construct() 
    {
        parent::__Construct();
        $this->load->model('mdl_faculty');
        //echo "<script>alert('hey');</script>";
    }
    public function index()
    {
       
        redirect('faculty/show_faculty');
    }
	
	function studenthome()
	{
		$this->load->view('vw_adminheader');
		$this->load->view('vw_adminmenu');
		$this->load->view('vw_body');
		$this->load->view('vw_adminfooter');
	}
    
     
    
    public function show_faculty()
     {
        
          $result = $this->mdl_faculty->get_faculty();
        $res['data'] = $result['data'];
		$this->load->view('vw_adminheader');
		$this->load->view('vw_adminmenu');
		//$this->load->view('vw_adminhome');
		  $this->load->view("vw_searchFaculty",$res);
		$this->load->view('vw_adminfooter');
     }
    /* public function student_page()
     {

          // Datatables Variables
          $draw = intval($this->input->get("draw"));
          $start = intval($this->input->get("start"));
          $length = intval($this->input->get("length"));


          $univ = $this->mdl_university->get_university();

          $data = array();

          foreach($univ->result() as $r) {

               $data[] = array(
                    $r->u_name
                    
                  
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $univ->num_rows(),
                 "recordsFiltered" => $univ->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
     }
    */
    /*function show_university($offset = 0)
    {
        $limit = 10;
        $result = $this->mdl_university->get_university($limit, $offset);
        $data['f_name'] = $result['faculty_table'];
        $data['rows'] = $result['rows'];
        //load pagination library
        $config = array();
        $config['base_url'] = base_url().'university/show_university';
        $config['total_rows'] = $data['rows'];
        $config['per_page'] = $limit;
        //Uri segment indicates pagination number
        $config['uri_segment'] = 3;
        $config['use_page_numbers'] = TRUE;
        //max links on a page will be shown
        $config['num_links'] = 5;
        //various pagination configuration
        $config['full_tag_open'] = '<div class="text-xs-right"><ul class="pagination text-right">';
        $config['full_tag_close'] = '</ul></div>';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['first_link'] = 'First';
        $config['last_tag_open'] = '<span class="last">';
        $config['last_tag_close'] = '</span>';
        $config['last_link'] = 'Last';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        
        $this->load->view('vw_adminheader');
        $this->load->view('vw_adminmenu');
        $this->load->view('vw_university', $data);
        $this->load->view('vw_adminfooter');

    } */
     function add_faculty()
    {
        $this->form_validation->set_rules('f_name', 'Faculty Name', 'trim|required');
       
        
        $data = null;
        if($this->form_validation->run() == false)
        {
             $data = array(
                'st'		    => 'fail',
                'f_name'     => form_error('f_name_error'),
                
                   );
		      echo json_encode($data);
        }
        else {
        $data = array(
            'f_name'  => $this->input->post('f_name'),
            'created' => date('Y-m-d'),
                );
        $this->mdl_faculty->add_faculty($data);
        $return_json = array('st'=> 'success');
        echo json_encode($return_json);
        
        }
    }
   function validate_email($email) 
   {
	   if(! preg_match("/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/", $email))	
	   {
		   $this->form_validation->set_message('validate_email', 'Please enter a valid Email.');			
		   return FALSE;		
	   }else{
		   return TRUE;
		   }	
	}
    
    function update_faculty()
    {
          //  echo json_encode(array('st'=>$_POST));
        
        $this->form_validation->set_rules('f_name', ' Faculty Name', 'trim|required');
        if($this->form_validation->run() == false)
        {
              $data = array(
                'st'		    => 'fail',
                'f_name'     => form_error('f_name_error'),
                  );
          
		      echo json_encode($data);
            // echo json_encode(array('st'=>'fail',));
        }
        else {
            $id= $this->input->post('f_id');
            $data = array(
            'f_name'     => $this->input->post('f_name'),
			//'modified'     => date('Y-m-d'),
            
                );
        $this->mdl_faculty->update_faculty($id,$data);
            
        $return_json = array('st'=> 'success','f_name'=> $this->input->post('f_name'));
        echo json_encode($return_json);
        }
    }
    
    function delete_faculty()
    {
         $d = array(
            'f_id' => $this->input->post('uid'),
            'f_name' => $this->input->post('uname'),
                );
        $p_id= $this->input->post('uid');
        $this->mdl_faculty->delete_faculty($p_id);
        echo json_encode($d);
    }
    
    
    
    
    
}
?>