<?php
class Admin extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('mdl_admin');
		//if (!isset($this->session->userdata['logged_in']))
		//{redirect('user_authentication','refresh');}
        
    }
    function index()
    {
        $this->load->view('vw_adminlogin');
	   //redirect('admin_home','refresh');
    }
    
    function adminloginsubmit()
    {
        
        //<!----valid login------->
        $data = array(
                'admin_name' => $this->input->post('adminname'),
                'admin_pass' => $this->input->post('adminpass')
                    );
       
        $result = $this->mdl_admin->adminlogin($data);
        if ($result == TRUE) //admin exists
        {
            $adminname = $this->input->post('adminname');
            $result = $this->mdl_admin->read_admin_information($adminname);
            if ($result != false)//could read admin info
            {
                //successful login
                $session_data = array(
                    'adminid'       => $result[0]->admin_id,//table field name
                    'adminname'     => $result[0]->admin_name,
                    //'adminemail'    => $result[0]->adminemail,
                    'adminpassword' => $result[0]->admin_pass,
                    //'adminpicture'  => $result[0]->adminpicture,
                    );

                // Add user data in session//
                $this->session->set_userdata('adminlogged_in', $session_data);
                redirect('admin/admin_home' ,'refresh');
                
                }
            else//couldn't read admin info
            {   //echo "res in else".$result; 
			}
                          
        }
        else //admin does nt exists
        {
            $this->load->view('vw_admin401');
            $this->load->view('vw_adminfooter');
            // redirect('admin/adminlogin','refresh');
        }
        
    }
    
    function admin_home()
    {
        $this->load->view('vw_adminheader');
        $this->load->view('vw_adminmenu');
        $this->load->view('vw_adminhome');
        $this->load->view('vw_adminfooter');
        
    }
    function sample()
    {
        $this->load->view('vw_full');
    }
	function logout()
	{
		$this->session->sess_destroy();
		$this->index();
	}
		
    
}
?>