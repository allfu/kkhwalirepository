<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Sponsors extends MX_Controller
{
    public function __Construct() 
    {
        parent::__Construct();
        $this->load->model('mdl_sponsors');
			if (!isset($this->session->userdata['adminlogged_in']))
		{redirect('admin','refresh');}
    }
    public function index()
    {
        redirect('sponsors/show_sponsors');
    }
	function show_sponsors()
    {        
        $res['data'] = $this->mdl_sponsors->get_spon();
      
		$this->load->view('vw_adminheader');
		$this->load->view('vw_adminmenu');
		$this->load->view("vw_searchSponsor",$res);
		$this->load->view('vw_adminfooter');
    }
	     
	function add_sponsor()
    {
		//echo json_encode($_POST);		
        $this->form_validation->set_rules('s_name', 'Sponsor name', 'trim|required');
        $this->form_validation->set_rules('s_desc', 'Sponsor description', 'trim|required');
        $this->form_validation->set_rules('s_tasks', 'Sponsor tasks', 'trim|required');
                
        $data = null;
        if($this->form_validation->run() == false)
        {
             $data = array(
                'st'		 => 'fail',
                'sponsor_name' => form_error('s_name'),
                'sponsor_desc' => form_error('s_desc'),                
                'sponsor_tasks' => form_error('s_tasks'),                
                    );
		      echo json_encode($data);
        }
        else {
		$filename = "default_logo.png";	 
		$path 	  = "assets/frontend/img/sponsorlogos/".$filename;	
		$types 	  = array('image/jpeg', 'image/gif','image/png','image/jpg');
		/*if (!in_array($_FILES['logo']['type'], $types))
			{
				//echo "<script>alert('File type not allowed')</script>";
			   $ret = array('st'=> 'fail','st_code' => 3);//3 - file type error
			   echo json_encode($ret);
			}*/
		$sourcePath   =  $_FILES['logo']['tmp_name'];
		$filename     =  rand(0,10000).$_FILES['logo']['name'];
		$targetPath   =  "assets/frontend/img/sponsorlogos/".$filename;
		move_uploaded_file($sourcePath, $targetPath);
		
        $data = array(
            's_name'   => $this->input->post('s_name'),
            's_desc'   => $this->input->post('s_desc'),
            'tasks'   => $this->input->post('s_tasks'),
			's_logo'   => $targetPath,
            'created'  => date('Y-m-d'),
                );
        $this->mdl_sponsors->add_spon($data);
        $return_json = array('st'=> 'success');
        echo json_encode($return_json);
        
        }
    }
	    
	function update_sponsor()
    {
        $this->form_validation->set_rules('editsponsor_name', 'Sponsor name', 'trim|required');
        $this->form_validation->set_rules('edit_sp_desc', 'Sponsor description', 'trim|required');
        $this->form_validation->set_rules('edit_tasks', 'Sponsor tasks', 'trim|required');
        if($this->form_validation->run() == false)
        {
              $data = array(
                'st'	=> 'fail',
                's_name'  => form_error('editsponsor_name'),
                's_desc'  => form_error('edit_sp_desc'),
                's_tasks'  => form_error('edit_tasks'),
                  );
          
		      echo json_encode($data);
            // echo json_encode(array('st'=>'fail',));
        }
        else {
				
			$filename = "default_logo.png";	 
			$path 	  = "assets/frontend/img/sponsorlogos/".$filename;	
			$types 	  = array('image/jpeg', 'image/gif','image/png','image/jpg');
			/*if (!in_array($_FILES['logo1']['type'], $types))
				{
					//echo "<script>alert('File type not allowed')</script>";
				   $ret = array('st'=> 'fail','st_code' => 3);//3 - file type error
				   echo json_encode($ret);
				}*/
			$sourcePath   =  $_FILES['logo1']['tmp_name'];
			$filename     =  rand(0,10000).$_FILES['logo1']['name'];
			$targetPath   =  "assets/frontend/img/sponsorlogos/".$filename;
			move_uploaded_file($sourcePath, $targetPath);
				
            $id= $this->input->post('editsponsor_id');
            $data = array(
            's_name'     => $this->input->post('editsponsor_name'),            
            's_desc'     => $this->input->post('edit_sp_desc'),
            'tasks'     => $this->input->post('edit_tasks'),
            's_logo'     => $targetPath,
            'modified'  => date('Y-m-d'),    
                );
        $this->mdl_sponsors->update_spondata($id,$data);
            
        $return_json = array('st'=> 'success','name'=> $this->input->post('editsponsor_name'));
        echo json_encode($return_json);
        }
    }
	
	function delete_sponsor()
    {
        $d = array(
            's_id' => $this->input->post('s_id'),
            's_name' => $this->input->post('s_name'),
             
                );
        $id= $this->input->post('s_id');
        $query = $this->mdl_sponsors->delete_spon($id);
        echo json_encode($d);
    }
	
	function undelete_sponsor()
	{
		
		$d = array(
            's_id' => $this->input->post('s_id'),
            's_name' => $this->input->post('s_name'),
                );
        $id= $this->input->post('s_id');
        $query = $this->mdl_sponsors->undelete_spon($id);
        echo json_encode($d);
	}
	
}
?>