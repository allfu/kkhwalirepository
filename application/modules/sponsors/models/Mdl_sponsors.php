<?php 
class Mdl_sponsors extends CI_Model
{
    function get_groups($limit, $offset) 
    {
        if($offset > 0) {
            $offset = ($offset - 1) * $limit;
        }
        $result['groups'] = $this->db->get('club_table', $limit, $offset);
        $result['rows'] = $this->db->count_all_results('club_table');
        return $result;
    }
    
    function get_spon()
    {
        $query= $this->db->get("sponsor_table");
        return $query->result();
    }
    
    function add_spon($data)
    {
        $this->db->insert('sponsor_table',$data);
        
    }
    function update_spondata($id,$data)
    {
        $this->db->where('s_id', $id);
        $q = $this->db->update('sponsor_table', $data);
        //alert("hieeee"+$q);
         //return $q->result();
    }
    
    function delete_spon($id)
    {
        $info=array('permit'=>0,'modified'=>date('Y-m-d'));
        $this->db->where('s_id', $id);
        $this->db->update('sponsor_table',$info );
		return true;
    }
	function undelete_spon($id)
	{
		$info=array('permit'=>1,'modified'=>date('Y-m-d'));
        $this->db->where('s_id', $id);
        $this->db->update('sponsor_table',$info );
		return true;
	}
}
?>