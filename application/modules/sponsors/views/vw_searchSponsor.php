<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12">
                <h1>Sponsor List</h1>
                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <!--<li><a href="#" data-toggle="modal" data-target="#add-product-modal"><i class="icon-plus4"></i> Add Client   </a></li>-->
                                        <li><button type="button" class="btn btn-info btn-block"
                                                    data-toggle="modal" data-target="#add-sponsor-modal">
                                            <i class="icon-plus4" style="color:white;"></i>   Add Sponsor</button>
                                        </li>
                                        <!--<li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                        <li><a data-action="reload"><i class="icon-reload"></i></a></li>-->
                                    </ul>
                                </div>
        <table id="sponsor-table" class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Sponsor Name</th>
                    <th>Logo</th>
                    <th>Description</th>
                    <!--<th>Product number</th>
                    <th>Product name</th>
					<th>Program logo</th>-->
                    <th>Tasks</th>
					<th>Created on</th>
                    <th>Permit</th>
                    <th>Actions</th>
                </tr>
                
            </thead>
            <tbody>
                <?php foreach($data as $row)    
				{  ?>
					<tr>
					<!--name-->
					<td class="text-middle"><?php echo $row->s_name; ?></td>
					
					<!--logo-->
					<td>
					<img src="<?php echo base_url().$row->s_logo;?>" style="width:50px; height:50px; min-height:10px;min-width:10px;" alt="Logo"/>
					</td>
					
					<!--desc-->
					<td>
					<?php echo $row->s_desc;?>
					</td>
					
					<!--prod no--
					<td class="text-middle">
					<?php //echo $row->prod_number; ?>
					</td>
					
					<!--prod name--
					<td class="text-middle">
					<?php //echo $row->prod_number; ?>
					</td>
					
					<!--prod img--
					<td>
					<img src="<?php //echo base_url().$row->prod_image;?>" style="width:50px; height:50px; min-height:10px;min-width:10px;"/>
					</td>
					<!--->
					
					<!--tasks-->
					<td class="text-middle">
					<?php echo $row->tasks; ?>
					</td>
					
					<td class="text-middle"><?php echo $row->created; ?></td>
					
					<td class="text-middle"><?php if($row->permit == '1')  { ?>
					<span class="tag tag-success">Active</span><?php } else { ?>
					<span class="tag tag-danger">Banned</span>
					<?php } ?>
					</td>
					
					<td><span class="btn-group">
						<!--<input type="button" class="btn btn-outline-info" value="View" data-toggle="modal" data-target="#view-modal"/>-->

						<input type="button" class="btn btn-outline-primary" value="Edit" 
						data-toggle="modal" data-target="#edit-sponsor-modal" 
						data-id="<?php echo $row->s_id;?>" data-name="<?php echo $row->s_name; ?>" data-logo="<?php echo $row->s_logo;?>" 
						data-desc="<?php echo $row->s_desc;?>"  data-tasks="<?php echo $row->tasks;?>" />
						<!--data-pid="<?php //echo $row->prod_number; ?>" data-pname="<?php //echo $row->prod_name; ?>" 
						data-pimg="<?php //echo $row->prod_image; ?>"-->

						<input type="button" class="btn btn-outline-danger" 
							   <?php 
								if($row->permit==0)  
								{ echo "value='Undelete'"; ?>						
							
								data-toggle="modal" data-target="#undelete-sponsor-modal" 
							   data-id1="<?php echo $row->s_id;?>" 
							   data-sname1="<?php echo $row->s_name; ?>" />
							   
								<?php  }
								
							   else{															   
								echo "value='Delete'"; ?>
								
							   data-toggle="modal" data-target="#delete-sponsor-modal" 
							   data-id2="<?php echo $row->s_id;?>" 
							   data-sname2="<?php echo $row->s_name; ?>" />
							   <?php } ?>
						</span>
					</td>
					
					</tr>
				<?php } ?>
            </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!--Add Modal-->
<div class="modal fade text-xs-left" id="add-sponsor-modal" tabindex="-1" role="dialog" aria-labelledby="addModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Add Sponsor Information</h4>
            </div>
            <form action="<?php //echo base_url().'clients/add_client';?>" method="post" id="addsponsor_form" name="addsponsor_form">
                <div class="modal-body">
                    <label>Sponsor Name:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="s_name" id="s_name" value="<?php echo set_value('s_name');?>"  />
                        <p id="sponsor_name_error" style="color:red;"></p>
                    </div>
					
					<label>Sponsor Description:</label>
                    <div class="form-group">
                        <input type="text-area" class="form-control" name="s_desc" id="s_desc" value="<?php echo set_value('s_desc');?>"  />
                        <p id="s_desc_error" style="color:red;"></p>
                    </div>
					
					<label>Sponsor Tasks:</label>
                    <div class="form-group">
                        <input type="text-area" class="form-control" name="s_tasks" id="s_tasks" value="<?php echo set_value('s_tasks');?>"  />
                        <p id="s_tasks_error" style="color:red;"></p>
                    </div>
					
					<label>Sponsor Logo:</label>
					<div class="form-group">
						<input type='file' onchange="readURL(this);" id="logo" name="logo"/>
						<img id="blah" src="#" alt="Your logo" />
						<p id="sponsor_logo_error" style="color:red;"></p>
					</div>
                    
                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="Cancel">
                    <input type="submit" class="btn btn-outline-primary" value="Submit" name="s1" id="s1"/>
                </div>
            </form>
        </div>
    </div>
</div>
<!--Edit Modal-->
<div class="modal fade text-xs-left" id="edit-sponsor-modal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Edit Sponsor Information</h4>
            </div>
            <form method="post" id="editsponsor_form" name="editsponsor_form" action="">
                <div class="modal-body">
                    <input type="hidden" name="editsponsor_id" id="editsponsor_id" value="<?php echo set_value('editsponsor_id');?>" /><!--value will be set through javascript-->
                    <label>Sponsor Name:</label>
                    <div class="form-group">
                        <input type="text" class="form-control" name="editsponsor_name" id="editsponsor_name" value="<?php echo set_value('editsponsor_name');?>" />
                    </div>
                    <p id="editsponsor_name_error" style="color:red;"></p>
					
					<label>Sponsor Description:</label>
                    <div class="form-group">
                        <input type="text-area" class="form-control" name="edit_sp_desc" id="edit_sp_desc" value="<?php echo set_value('edit_sp_desc');?>"  />
                        <p id="editsponsor_desc_error" style="color:red;"></p>
                    </div>
					
					<label>Sponsor Tasks:</label>
                    <div class="form-group">
                        <input type="text-area" class="form-control" name="edit_tasks" id="edit_tasks" value="<?php echo set_value('edit_tasks');?>"  />
                        <p id="editsponsor_tasks_error" style="color:red;"></p>
                    </div>
										
					<label>Sponsor Logo:</label>
                    <div class="form-group">
					<input type="text" id="logo1" name="logo1" value=""/>
					<input type='file' onchange="readURL1(this);" id="logo1" name="logo1" />
					<img id="blah1" name="blah1" src="" alt="Your logo" 
						 style="height:150px; width:150px; min-height:90px; min-width:90px;"/>
					</div>
                    <p id="editsponsor_logo_error" style="color:red;"></p>
					
					
                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="close">
                    <input type="submit" class="btn btn-outline-primary updatesponsor_submit" value="Submit" />
                </div>
            </form><p></p>
        </div>
    </div>
</div>
<!--Delete Modal-->
<div class="modal fade text-xs-left" id="delete-sponsor-modal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form action="<?php echo base_url().'sponsors/delete'; ?>" method="POST" class="delete-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Delete Sponsor</h4>
                </div>
                <div class="modal-body deletepara">
                    <p></p> <!--client information will go through here to jscript-->
                    <input type="hidden" name="del_sp_id" id="del_sp_id" value="" /><!--value will be set through javascript-->
                    <input type="hidden" name="del_sp_name" id="del_sp_name" value="" /><!--do  you really want to delete- set through javascript-->
                    <div id="result">
                        <div id="value"></div>
                        <div id="anothervalue"></div>
                    </div>
                </div>
                </form>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="No">
                    <input type="submit" class="btn grey btn-outline-danger delete-sponsor-submit" value="Yes">
                </div>
            </div>
    </div>
</div>
<!--UnDelete Modal-->
<div class="modal fade text-xs-left" id="undelete-sponsor-modal" tabindex="-1" role="dialog" aria-labelledby="undeleteModal" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form action="<?php echo base_url().'sponsors/delete'; ?>" method="POST" class="delete-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Activating sponsor</h4>
                </div>
                <div class="modal-body deletepara">
                    <p></p> <!--client information will go through here to jscript-->
                    <input type="hidden" name="undel_sp_id" id="undel_sp_id" value="" /><!--value will be set through javascript-->
                    <input type="hidden" name="undel_sp_name" id="undel_sp_name" value="" /><!--activating sponsor...- set through javascript-->
                    
                </div>
                </form>
                <div class="modal-footer">
                    <input type="reset" class="btn grey btn-outline-secondary" data-dismiss="modal" value="No">
                    <input type="submit" class="btn grey btn-outline-danger undelete-sponsor-submit" value="Yes">
                </div>
            </div>
    </div>
</div>

<script>
//DELETE SPONSOR SCRIPT
$('#delete-sponsor-modal').on('show.bs.modal', function(e) {
   
    //get data-id attribute of the clicked element
    var delspid = $(e.relatedTarget).data('id2');
    var delspname = $(e.relatedTarget).data('sname2');
    var modal = $(this);
   
    //populate the textboxes
    $(e.currentTarget).find('input[name="del_sp_id"]').val(delspid);
    document.getElementById('del_sp_name').value = delspname;
     modal.find('.modal-body p').html('Do you really want to delete - <b>' + delspname + '</b>?');
   
});

$(document).ready(function()	{
    $(".delete-sponsor-submit").click(function(event)	{
       event.preventDefault();
       
       var sid = document.getElementById('del_sp_id').value;
      var sname = document.getElementById('del_sp_name').value;
        //alert('deleting '+sname);
        
           jQuery.ajax({
               type: "POST",
               url: "<?php echo site_url('sponsors/delete_sponsor'); ?>",
               dataType: "json",
               data: {s_id: sid, s_name: sname},
               success: function(res)	{
                   console.log(res);
                   if(res)
                       {   
                           //alert('sponsor Deleted Successfully');
						   swal(
							  '',
							  'Sponsor deleted successfully!',
							  'success'
							);
                           //$('#delete-client-modal').modal().hide();
                           window.location.href = "<?php echo site_url('sponsors'); ?>";

                       }
                   }
               });
           });
       });
</script>
<script>
//UNDELETE SPONSOR SCRIPT
$('#undelete-sponsor-modal').on('show.bs.modal', function(e) {
   
    //get data-id attribute of the clicked element
    var undelspid1 = $(e.relatedTarget).data('id1');
    var undelspname1 = $(e.relatedTarget).data('sname1');
    var modal = $(this);
   
    //populate the textboxes
    $(e.currentTarget).find('input[name="undel_sp_id"]').val(undelspid1);
    document.getElementById('undel_sp_name').value = undelspname1;
     modal.find('.modal-body p').html('Activate - <b>' + undelspname1 + '</b>?');
   
});

$(document).ready(function()	{
    $(".undelete-sponsor-submit").click(function(event)	{
       event.preventDefault();       
       var sid = document.getElementById('undel_sp_id').value;
      var sname = document.getElementById('undel_sp_name').value;
       //alert('undelete '+sname);
        
           jQuery.ajax({
               type: "POST",
               url: "<?php echo site_url('sponsors/undelete_sponsor'); ?>",
               dataType: "json",
               data: {s_id: sid, s_name: sname},
               success: function(res)	{
                   console.log(res);
                   if(res)
                       {   
                           //alert('sponsor Deleted Successfully');
						   swal( 
							  '',
							  'Sponsor Activated successfully!',
							  'success'
							);
                           //$('#delete-client-modal').modal().hide();
                           window.location.href = "<?php echo site_url('sponsors'); ?>";

                       }
                   }
               });
           });
       });
</script>

<script type="text/javascript">
//ADD SPONSOR SCRIPT
$(function() { // <----------------missed the doc ready function
    //$('#addsponsor_form').submit(function(e) {
	$('#s1').click(function(e) {
        e.preventDefault(); // <------this will restrict the page refresh
		
        /*var form_data = {
            c_name: $('#sponsor_name').val(),
            c_uni: $('#grp_uni').val(),
            c_by: $('#grp_leader').val(),
            };*/
		
		var form = $('#addsponsor_form');
            var formData = new FormData(form[0]);
            console.log(formData);
		//alert(form);
        $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + 'sponsors/add_sponsor',
                data: formData,
				processData: false,
				contentType: false,
                success: function(res)  {
                    var json = JSON.parse(res);
                    console.log(json);
                     if(json.st== 'fail'){
                        $('#sponsor_name_error').html(json.sponsor_name);
                        $('#s_desc_error').html(json.sponsor_desc);                                                
                        $('#tasks_error').html(json.sponsor_tasks);                                                
                    }
					 if(json.st_code== 3){
						 swal(
						  '',
						  'File type not allowed!',
						  'error'
						)
					 }
                    if(json.st =='success') {
                        //alert('sponsor added successfully');
						swal(
						  '',
						  'Sponsor added successfully!',
						  'success'
						)
                        window.location.href = "<?php echo site_url('sponsors'); ?>";
                    }
                   
                }
        });

    });

});
</script>

<script>
//EDIT SPONSOR SCRIPT
//triggered when modal is about to be shown
   $('#edit-sponsor-modal').on('show.bs.modal', function (e) {
	var editspid   = $(e.relatedTarget).data('id');
    var editspname = $(e.relatedTarget).data('name');
    var editspdesc = $(e.relatedTarget).data('desc');
    var editsptasks = $(e.relatedTarget).data('tasks');
    var editsplogo = $(e.relatedTarget).data('logo');
    var logopath    = "<?php echo base_url();?>"+editsplogo;
	alert(editspid);
  var modal = $(this)
  //modal.find('modal-title').text('New message to ' + recipient)
  //modal.find('.modal-body input').val(editspid);
   $(e.currentTarget).find('input[name="editsponsor_id"]').val(editspid);
       document.getElementById('editsponsor_name').value  = editspname;
	   $(e.currentTarget).find('input[name="editsponsor_id"]').val(editspid);
    document.getElementById('editsponsor_name').value  = editspname;
    document.getElementById('edit_sp_desc').value    = editspdesc;
    document.getElementById('edit_tasks').value    = editsptasks;
    //$(e.currentTarget).find('input[name="logo1"]').val(editsplogo);
  
   $('#blah1').attr('src',logopath);	
});
    //get data-id attribute of the clicked element
    
	//alert(logopath); 
    //populate the textboxes
    //$(e.currentTarget).find('input[name="editsponsor_id"]').val(editspid);
   // document.getElementById('editsponsor_name').value  = editspname;
    //document.getElementById('edit_sp_desc').value    = editspdesc;
    //document.getElementById('edit_tasks').value    = editsptasks;
   // $(e.currentTarget).find('input[name="logo1"]').val(editsplogo);
   // $(e.currentTarget).find('img[name="blah1"]').val(editgrplogo); 
	//$('#blah1').attr('src',logopath);	
	//$('#edit-sponsor-modal').modal('show')

$(document).ready(function()	{
$(".updatesponsor_submit").click(function(e)	{
        e.preventDefault(); //this will restrict the page refresh
        
		var form = $('#editsponsor_form');
            var formData = new FormData(form[0]);
            console.log(formData);
        //alert(form_data.grp_id+" id "+form_data.grp_name);
    //var form1 = new FormData(this);
    //var form2 = $('#editclient_form').serialize();
    jQuery.ajax({
        type: "POST",
		processData: false,
		contentType: false,
        data:formData,
       // data: {gid: form_data.grp_id, sname:form_data.grp_name, guni:form_data.grp_uni, glead:form_data.grp_lead },
        url: "<?php echo site_url('sponsors/update_sponsor'); ?>",
        success: function(res)  {
            var json = JSON.parse(res);
            console.log(json);
             if(json.st== 'fail'){
                $('#editsponsor_name_error').html(json.s_name);
                $('#editsponsor_desc_error').html(json.s_desc);
                $('#editsponsor_tasks_error').html(json.s_tasks);
                }
            if(json.st =='success') {
                //alert('sponsor updated successfully');
				swal(
				  '',
				  'Sponsor updated successfully!',
				  'success'
				);
				//setTimeout(function, milliseconds);
                window.location.href = "<?php echo site_url('sponsors'); ?>";
                }
        }
        });
    });
});
</script>
<script>
    $(document).ready(function() {
        
        $('#sponsor-table').DataTable();
    });
</script>
<script>
     function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(150);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
<script>
     function readURL1(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah1')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(150);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>