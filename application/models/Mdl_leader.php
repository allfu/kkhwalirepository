<?php
Class Mdl_leader extends CI_Model 
{
	function getevents($username)
	{
		$q = $this->db->get_where('user_table',array('user_name'=>$username));
        $i = $q->row()->id;
		$g = $this->db->get_where('event_table',array('e_leader'=>$i,'permit'=>1));
        return $g->result();
		
		//$this->
	}
	
	 function getgroups($username)
    {		
        $q = $this->db->get_where('user_table',array('user_name'=>$username));
        $i = $q->row()->id;
                
        $g = $this->db->get_where('group_table',array('gstudid'=>$i,'type'=>2));/*type=2 means he is admin, so he sees only grps of which he is admin*/
        return $g->result();
		//print_r($g->result());
        
    }
	
	function add_event($data)
	{
		$this->db->insert('event_table',$data);
		return true;
		
	}
	function add_sponsor($data)
	{ 
		$this->db->insert('sponsor_event',$data);
		return true;
		
	}
	function getuserid($username)
	{
		$q = $this->db->get_where('user_table',array('user_name'=>$username));
        $i = $q->row()->id;
		return $i;
		
	}
	function update_event($data, $eid)
	{
		$this->db->where('e_id', $eid);
		$this->db->update('event_table', $data);
		return true;
	}
	
	function deleteevent($id)
	{
		$info=array('permit'=>0,'modified'=>date('Y-m-d'));
        $this->db->where('e_id', $id);
        $query = $this->db->update('event_table',$info );
		return $query;
	}
	
	function add_club($data)
	{
		$this->db->insert('club_table',$data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
		
	}
	
	function getallgroups()
	{
		/*$this->db->where('gstudid !=', $uid);
		$g = $this->db->get_where('group_table');
        return $g->result();*/
		$g = $this->db->get_where('club_table',array('permit'=>1));
        return $g->result();
		
	}
	function join_club($data2)
	{
		$this->db->insert('group_table',$data2);
	}
	
	
	function editevent($id)
	{
		$q = $this->db->get_where('event_table',array('e_id'=>$id));
		return $q->row();
	}
	
	function deletegrp($id)
	{
		$info=array('permit'=>0,'modified'=>date('Y-m-d'));
        $this->db->where('c_id', $id);
        $query = $this->db->update('club_table',$info );
		return $query;
	}
	function edit_group($gid,$data2)
	{
		$this->db->where('g_id', $cid);
		$this->db->update('group_table',$data2);
		
	}
	function edit_club($cid,$data)
	{ 
		$this->db->where('c_id', $cid);
		$this->db->update('club_table', $data);
		return true;
		
	}
	
	function publish_e($eid)
	{		
		$info=array('publish'=>1,'modified'=>date('Y-m-d'));
        $this->db->where('e_id', $eid);
        $query = $this->db->update('event_table',$info );
		return $query;
		
	}
	function unpublish_e($eid)
	{		
		$info=array('publish'=>0,'modified'=>date('Y-m-d'));
        $this->db->where('e_id', $eid);
        $query = $this->db->update('event_table',$info );
		return $query;
		
	}
	
	function getsearchdata($keyword, $username)
	{
		$q = $this->db->get_where('user_table', array('user_name'=>$username));
		$u = $q->row();
		$sql = "(SELECT e_id, e_name, e_desc,e_img,e_start,e_end,e_loc,e_type,e_fblink,e_instalink,e_by,e_leader,
					   publish, permit, created,modified,'event' as type FROM event_table 
        WHERE e_name LIKE '%".$keyword."%' OR e_desc LIKE '%".$keyword."%')";
		$query1= $this->db->query($sql);
    
        /*foreach($query1->result() as $row)
        {
            echo "<pre>";
            print_r($row);
            echo "</pre>";
            echo "////".$row->type;
        }
        echo "<br>single: ".$query1->row()->type;*/
		
		return $query1->result();
	}
	
	function getsponsors()
	{
		$q = $this->db->get_where('sponsor_table',array('permit'=>1));
		 return $q->result();
		
	}
	
	function members($groupid)
	{
		 $query =$this->db->get_where('group_table',array('gclubid'=> $groupid,'permit'=>1));
		 return $query->result();
	}
	
	function make_grp_admin($gid)
	{
		$info=array('type'=>2,'modified'=>date('Y-m-d'));
        $this->db->where('gid', $gid);
        $query = $this->db->update('group_table',$info );
		return $query;
	}
	
	function remove_grp_member($gid)
	{
		$info=array('permit'=>0,'modified'=>date('Y-m-d'));
        $this->db->where('gid', $gid);
        $query = $this->db->update('group_table',$info );
		return $query;
	}
}
?>