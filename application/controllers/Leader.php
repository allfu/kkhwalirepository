<?php

Class Leader extends CI_Controller {

public function __construct() {
parent::__construct();

// Load database
$this->load->model('login_database');
$this->load->model('mdl_student_reg');
$this->load->model('mdl_leader');
}

// Show leader signup:
public function index() {
//$this->load->view('login_form');
//$this->load->view('vw_student_login');
    //$this->leader_signup();
    $this->user_login();
}

// Show registration page
public function user_registration() {
$this->load->view('registration_form');
}

// Validate and store registration data in database
public function new_user_registration()
{
    // Check validation for user input in SignUp form
    $this->form_validation->set_rules('user_name', 'Username', 'trim|required');
    $this->form_validation->set_rules('first_name', 'Firstname', 'trim|required');
    $this->form_validation->set_rules('last_name', 'Lastname', 'trim|required');
    $this->form_validation->set_rules('contact', 'contact No', 'trim|required');
    $this->form_validation->set_rules('email', 'Email', 'trim|required|callback_validate_email');
    $this->form_validation->set_rules('website', 'Website', 'trim|required');
    $this->form_validation->set_rules('pass', 'Password', 'trim|required|min_length[6]');
    $this->form_validation->set_rules('re_pass', 'Confirm Password', 'required|trim|matches[pass]');
    $this->form_validation->set_rules('school', 'School Information', 'required|trim');
    $this->form_validation->set_rules('yr', 'Year of Study', 'required|trim');
    $this->form_validation->set_rules('matno', 'Matriculation No', 'required|trim');
    
    if ($this->form_validation->run() == FALSE) 
    {
         $data = array(
                'error'		    => 'fail',
                'user_name'     => form_error('user_name'),
                'first_name'    => form_error('first_name'),
                'last_name'     => form_error('last_name'),
                'contact'       => form_error('contact'),
                'email'         => form_error('email'),
                'website'       => form_error('website'),
                'pass'          => form_error('pass'),
                're_pass'       => form_error('re_pass'),
                'school'        => form_error('school'),
                'yr'            => form_error('yr'),
                'matno'            => form_error('matno'),
				
				
                );
		      echo json_encode($data);
        //$this->load->view('registration_form');
    } 
    else 
    {
        $data = array(
            'user_name'     => $this->input->post('user_name'),
            'firstname'     => $this->input->post('first_name'),
            'lastname'      => $this->input->post('last_name'),
            'user_email'    => $this->input->post('email'),
            'user_password' => $this->input->post('pass'),
            'contact_no'    => $this->input->post('contact'),
            'website'       => $this->input->post('website'),
            'school'        => $this->input->post('school'),
            'yr'          => $this->input->post('yr'),
            'matno'          => $this->input->post('matno'),
        );
        $result = $this->login_database->registration_insert($data);
        $msg=null;
        if ($result == TRUE) 
        { $msg = true/*"Registration successful"*/;}
        else 
        { $msg= false/*"Username already exists!"*/; }
        $return_json = array('error'=> 'success','msg'=>$msg);
        echo json_encode($return_json);
        /*
        if ($result == TRUE) {
        $data['message_display'] = 'Registration Successful !';
        $this->load->view('login_form', $data);
        } else {
        $data['message_display'] = 'Username already exist!';
        $this->load->view('registration_form', $data);
        }*/
        
    }
}
    function validate_email($email) 
   {
	   if(! preg_match("/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/", $email))	
	   {
		   $this->form_validation->set_message('validate_email', 'Invalid Email.');			
		   return FALSE;		
	   }else{
		   return TRUE;
		   }	
	}

// Check for user login process
public function user_login() {

$this->form_validation->set_rules('username', 'Username', 'trim|required');
$this->form_validation->set_rules('password', 'Password', 'trim|required');

if ($this->form_validation->run() == FALSE) {
if(isset($this->session->userdata['logged_in'])){
/*$this->load->view('admin_page');*/
//redirect('students','refresh');
$this->leader_home();
}else{
$this->load->view('login_form');
}
} else {
$data = array(
'username' => $this->input->post('username'),
'password' => $this->input->post('password')
);
$result = $this->login_database->login($data);
if ($result == TRUE) {

$username = $this->input->post('username');
$result = $this->login_database->read_user_information($username);
if ($result != false) {
$session_data = array(
'username' => $result[0]->user_name,
'email' => $result[0]->user_email,
'sessiontype'=> 2,
);
// Add user data in session
$this->session->set_userdata('logged_in', $session_data);
/*$this->load->view('admin_page');*/
$this->leader_home();
//redirect('students','refresh');
}
} else {
$data = array(
'error_message' => 'Invalid Username or Password'
);
$this->load->view('login_form', $data);
}
}
}

// Logout from admin page
public function logout() {

// Removing session data
$sess_array = array(
'username' => '',
'email'=> '',
'sessiontype'=>'',
);
$this->session->unset_userdata('logged_in', $sess_array);
$data['message_display'] = 'Successfully Logged out';
$this->load->view('login_form', $data);
}
    
function stud_reg()
{
    //echo json_encode($_POST);
    $this->form_validation->set_rules('phone_number', 'Phone no', 'trim|required|numeric');
    $this->form_validation->set_rules('matriculation_number', 'Matriculation no', 'trim|required|numeric');
    $this->form_validation->set_rules('university', 'University name', 'trim|required');
    $this->form_validation->set_rules('faculty', 'Faculty name', 'trim|required');
    $this->form_validation->set_rules('hall_name', 'Hall name', 'trim|required');
    $this->form_validation->set_rules('cca_name', 'CCA name', 'trim|required');
    $this->form_validation->set_rules('student_accommodation', 'School Accomodation', 'required|trim');
    $this->form_validation->set_rules('school_email', 'School email', 'required|trim');


    $data = null;
    if($this->form_validation->run() == false)
    {
         $data = array(
            'st'		            => 'fail',
            'phone_number'          => form_error('phone_number'),
            'matriculation_number'  => form_error('matriculation_number'),
            'university'             => form_error('university'),
            'faculty'               => form_error('faculty'),
            'hall_name'              => form_error('hall_name'),
            'cca_name'                => form_error('cca_name'),
            'student_accommodation'   => form_error('student_accommodation'),
            'school_email'            => form_error('school_email'),

               );
          echo json_encode($data);
    }
    else {
    $data = array(
        //*send fb values from view to db/model*/
        'contact_no'  => $this->input->post('phone_number'),
        'matno'  => $this->input->post('matriculation_number'),
        'school'  => $this->input->post('university'),
        'faculty'  => $this->input->post('faculty'),
        'hall_name'  => $this->input->post('hall_name'),
        'group_cca'  => $this->input->post('cca_name'),
        'accomodation'  => $this->input->post('student_accommodation'),
        'user_email'  => $this->input->post('school_email'),
        'created'  => date('Y-m-d'),
        'type' => 1,
            );
    $this->mdl_student_reg->add_student($data);
    $return_json = array('st'=> 'success');
    echo json_encode($return_json);

    }
    
}

function leader_signup()
{
	 $data['res'] = $this->login_database->getRecords();
	 //print_r($data['res']);
    $this->load->view('registration_form',$data);
        
}

function leader_home()
{
	if(isset($this->session->userdata['logged_in'])) 
	{
		$username = ($this->session->userdata['logged_in']['username']);
		$email = ($this->session->userdata['logged_in']['email']);
	}
	$data['events']=$this->mdl_leader->getevents($username);
    $this->load->view('vw_header2');
    $this->load->view('vw_student_leader_home',$data);
    //$this->load->view('vw_student_leader_localhost');
    $this->load->view('vw_footer');
    
}
function leader_sponsors()
{
	if(isset($this->session->userdata['logged_in'])) 
	{
		$username = ($this->session->userdata['logged_in']['username']);
		$email = ($this->session->userdata['logged_in']['email']);
	}
	$data['eve']=$this->mdl_leader->getevents($username);
	$data['spns']=$this->mdl_leader->getsponsors();
	
    $this->load->view('vw_header2');
    $this->load->view('leaderviews/vw_leader_sponser',$data);
 
    $this->load->view('vw_footer');
    
}
function my_groups()
{
	 //if (isset($this->session->userdata['logged_in'])) 
        //{
		$username = ($this->session->userdata['logged_in']['username']);
		$email = ($this->session->userdata['logged_in']['email']);
    //}
			   
        $data['grps']=$this->mdl_leader->getgroups($username);
		//print_r($data);
        
        $this->load->view('vw_header2');
        $this->load->view('vw_mygroups',$data);
        //$this->load->view('vw_student_leader_localhost');
        $this->load->view('vw_footer');
}

function create_event()
{
	//if (isset($this->session->userdata['logged_in'])) 
        //{
		$username = ($this->session->userdata['logged_in']['username']);
		$email = ($this->session->userdata['logged_in']['email']);
    //}
			   
    $data['grps']=$this->mdl_leader->getgroups($username);
	//print_r($data);
	
	$this->load->view('vw_header2');
    $this->load->view('vw_leader_create_event',$data);
    $this->load->view('vw_footer');
	
}
 function seeuid()
 {
 if(isset($this->session->userdata['logged_in'])) {
				$username = ($this->session->userdata['logged_in']['username']);
				$email = ($this->session->userdata['logged_in']['email']);
	}
	$data['events']=$this->mdl_leader->getevents($username);
    $this->load->view('vw_header2');
    $this->load->view('vw_leaderhome-bckup',$data);
    //$this->load->view('vw_student_leader_localhost');
    $this->load->view('vw_footer');
 }
 

function eventsubmit()
{
	//please check console log, all data is here, extract it
	/*$data = array(
		'form_data' => $_POST,
		'file' => $_FILES,
		'status' => 'controller check',
	);
	echo json_encode($data);*/
		$username = $this->session->userdata['logged_in']['username'];
	
	 
	 $uid=$this->mdl_leader->getuserid($username);
	
	//Example Code
	 //form input validation
		$update=null;
        $this->form_validation->set_rules('e_fblink','Fb link','trim|required');
        $this->form_validation->set_rules('e_instalink','Instagram link','trim|required');
        $this->form_validation->set_rules('e_name','Event name','trim|required');
        $this->form_validation->set_rules('e_by','CCA','trim|required');
        $this->form_validation->set_rules('e_desc','Description','trim|required|max_length[200]');
        $this->form_validation->set_rules('e_start','Start Date','trim|required|max_length[200]');
        $this->form_validation->set_rules('e_end','End Date','trim|required|max_length[200]');
        $this->form_validation->set_rules('e_type','Type','required');
        $this->form_validation->set_rules('e_loc','Location','required');
        if (empty($_FILES['file']['name']))
			{                        //check file is selected
            $this->form_validation->set_rules('file','File','required');
			$this->form_validation->set_rules('file1','Event Logo','required');
        }
        if($this->form_validation->run() == FALSE)  {               //form validation false
            $data = array(
                'status' => validation_errors(),
                'status_code' => 2
            );
            echo json_encode($data);
        }  
		else    
		{    
                $sourcePath = $_FILES['file']['tmp_name']; //filename="file" 
                $sourcePath1 = $_FILES['file1']['tmp_name']; //filename="file" 
                $targetPath = "assets/uploads/".rand(0,10000).$_FILES['file']['name']; //targetpath with random number as prefix
                $targetPath1 = "assets/uploads/".rand(0,10000).$_FILES['file1']['name']; //targetpath with random number as prefix
                move_uploaded_file($sourcePath,$targetPath); //move file to target folder
                move_uploaded_file($sourcePath1,$targetPath1); //move file to target folder
                $data1 = array( //take inputs
                    'e_name' => $this->input->post('e_name'),
                    'e_desc' => $this->input->post('e_desc'),
                    'e_start' => $this->input->post('e_start'),
                    'e_end' => $this->input->post('e_end'),
					/*e_start=>$this->input->post(),
					 e_end=>$this->input->post,*/
                    'e_by' => $this->input->post('e_by'),
                    'e_fblink' => $this->input->post('e_fblink'),
                    'e_instalink' => $this->input->post('e_instalink'),
                    'e_type' => $this->input->post('e_type'),
                    'e_loc' => $this->input->post('e_loc'),
                    'e_img' => $targetPath,
					'e_logo' => $targetPath1,
					'e_leader' => $uid,
                    'created' => date('Y-m-d H:i:s'),
                );
           
                //echo json_encode(array('status' => $data1));
            $update = $this->mdl_leader->add_event($data1);          //send data1 array to model
		  // $update=1;
            if($update) {                                               //if return array true
             $data2 = array(	
				'form_data' => $data1,
                'status' => 'Event Updated',
                'status_code' => 1,
                'data' => $update,
            );
            echo json_encode($data2);
            }   
			else    
			{
                 $data = array(
				'form_data' => $data1,
                'status' => 'Failed to Update',
                'status_code' => 0,
				'errors'=>validation_errors(),
            );
            echo json_encode($data);
            }
        }
    //} 
}
function sponsor_submit()
{
	
	   // echo json_encode(array('data' => $_POST));
		//$username = $this->session->userdata['logged_in']['username'];
	    //$uid=$this->mdl_leader->getuserid($username);
	    //$eid =  $this->input->post('e_id');
	    //$update=null;
        
		$this->form_validation->set_rules('sponsor[]','Sponsor','required' );
        
        if($this->form_validation->run() == FALSE)  
		{               //form validation false
            $data = array(
                'status' => validation_errors(),
                'status_code' => 2
            );
            echo json_encode($data);
        }  
		else    
		{    
			  $spnsr = $this->input->post('sponsor[]');
			  $event_id =$this->input->post('e_id');
			  $event = array();
			  //$spnsr = json_encode($spnsr);
			 /* $event = array(
				'spn_id' => $spnsr,
				'eve_id' => $event_id,
				'created' => date('Y-m-d'),
			  );  */
				foreach($spnsr as $row) 
				{
				$event= array(
				'spn_id' => $row,
				'eve_id' => $event_id,
				'created'=> date('Y-m-d H:i:s'),
							);
				$update = $this->mdl_leader->add_sponsor($event);
				//echo json_encode(array('data' => $update));
              } 
				//echo json_encode(array('update' => $update));
                     //send data1 array to model
		    if($update) {                                               //if return array true
             $data2 = array(	
				//'form_data' => $data1,
                'status' => 'Sponsor sent for Approval',
                'status_code' => 1,
                'data' => $update,
            );
            echo json_encode($data2);
            }   
			else    
			{
              $data = array(
				'form_data' => $data1,
                'status' => 'Failed to Submit',
                'status_code' => 0,
				'errors'=>validation_errors(),
            );
              echo json_encode($data);
            } 
			}
}
function edit_event($id)
{
	$username = $this->session->userdata['logged_in']['username'];
	$uid=$this->mdl_leader->getuserid($username);
	$data['grps']=$this->mdl_leader->getgroups($username);
	$data['event'] = $this->mdl_leader->editevent($id);
	$this->load->view('vw_header2');
    $this->load->view('vw_leader_edit_event',$data);
    $this->load->view('vw_footer');
	
}

function editsubmit()
{
	/*$data = array(
		'form_data' => $_POST,
		'file' => $_FILES,
		'status' => 'controller check',
	);
	echo json_encode($data);*/
	$username = $this->session->userdata['logged_in']['username'];
	$uid=$this->mdl_leader->getuserid($username);
	$eid =  $this->input->post('e_id');
	//Example Code
	 //form input validation
		$update=null;
        $this->form_validation->set_rules('e_fblink','Fb link','trim|required');
        $this->form_validation->set_rules('e_instalink','Instagram link','trim|required');
        $this->form_validation->set_rules('e_name','Event name','trim|required');
        $this->form_validation->set_rules('e_desc','Description','trim|required');
        $this->form_validation->set_rules('e_type','Type','required');
        $this->form_validation->set_rules('e_loc','Location','required');
		$this->form_validation->set_rules('e_by','CCA','trim|required');
        //if (empty($_FILES['file']['name'])) {                        //check file is selected
            //$this->form_validation->set_rules('file','File','required');
			
        //}
        if($this->form_validation->run() == FALSE)  {               //form validation false
            $data = array(
                'status' => validation_errors(),
                'status_code' => 2
            );
            echo json_encode($data);
        }  
		else    
		{    //valid data      
            //echo json_encode(array('data' => $_FILES['file']['name']));
            //form validation true
            /*if (0 < $_FILES['file']['error']) {
                $data = array(
                    'status' => 'File uploading error',
                    'status_code' => 3
                );
                echo json_encode($data);
				}   
				else    {*/				
				if(!empty($_FILES['file']['name'])){
                $sourcePath = $_FILES['file']['tmp_name']; //filename="file" 
                $targetPath = "assets/uploads/".rand(0,10000).$_FILES['file']['name']; //targetpath with random number as prefix
                move_uploaded_file($sourcePath,$targetPath); //move file to target folder
                $data1 = array( //take inputs
                    'e_name' => $this->input->post('e_name'),
                    'e_desc' => $this->input->post('e_desc'),
					/*e_start=>$this->input->post(),
					 e_end=>$this->input->post,*/
                    'e_by' => $this->input->post('e_by'),
                    'e_fblink' => $this->input->post('e_fblink'),
                    'e_instalink' => $this->input->post('e_instalink'),
                    'e_type' => $this->input->post('e_type'),
                    'e_loc' => $this->input->post('e_loc'),
                    'e_img' => $targetPath,
					'e_leader' => $uid,
                    'created' => date('Y-m-d H:i:s'),
                );
				}
				else{
					$data1 = array( //take inputs
                    'e_name' => $this->input->post('e_name'),
                    'e_desc' => $this->input->post('e_desc'),
					/*e_start=>$this->input->post(),
					 e_end=>$this->input->post,*/
                    'e_by' => $this->input->post('e_by'),
                    'e_fblink' => $this->input->post('e_fblink'),
                    'e_instalink' => $this->input->post('e_instalink'),
                    'e_type' => $this->input->post('e_type'),
                    'e_loc' => $this->input->post('e_loc'),
                    //'e_img' => $targetPath,
					'e_leader' => $uid,
                    'created' => date('Y-m-d H:i:s'),
                );
				}
                //echo json_encode(array('status' => $data1));
            $update = $this->mdl_leader->update_event($data1,$eid);          //send data1 array to model
		  // $update=1;
            if($update) {                                               //if return array true
             $data2 = array(	
				'form_data' => $data1,
                'status' => 'Event Updated',
                'status_code' => 1,
                'data' => $update,
            );
            echo json_encode($data2);
            }   
			else    
			{
                 $data = array(
				'form_data' => $data1,
                'status' => 'Failed to Update',
                'status_code' => 0,
				'errors'=>validation_errors(),
            );
            echo json_encode($data);
            }
        }
    //} 
}
function image_submit()
 {
     $username = $this->session->userdata['logged_in']['username'];
     $this->form_validation->set_rules('c_uni','School Name','trim|required');
     $this->form_validation->set_rules('c_name','Group Name','trim|required');
     if($this->form_validation->run() == false) {
         $data = array(
            'status_code' => 0,
             'status' => validation_errors(),
         );
         echo json_encode($data);
     }  else    {
	$response = array();
	$uid= $this->mdl_leader->getuserid($username); 
	$data=array(
	'c_uni'=>$_POST['c_uni'],
	'c_name'=>$_POST['c_name'],
	'c_desc'=>$_POST['c_desc'],
	'c_by'=>$uid,
	'created'=>date('Y-m-d'),
	);
	
	$path1 = "clublogos/";	
	$valid_formats = array("jpg", "png","jpeg");
    $lastid = $this->mdl_leader->add_club($data);
	 if(!empty($lastid)) {
		$data2 = array(
            'gstudid' =>$uid,
            'gclubid' => $lastid,
            'type'    => 2,
            'created' => date('Y-m-d'),
            );
        $this->mdl_leader->join_club($data2);
		if (isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") 
		{
		for($i=0;$i<count($_FILES["images"]["name"]);$i++)
		{
			$name = $_FILES['images']['name'][$i];
			$size = $_FILES['images']['size'][$i];
			if (strlen($name)) 
			{
				list($txt, $ext) = explode(".", $name);
				if (in_array($ext, $valid_formats)) 
				{
					if ($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg') 
					{
						if ($size < (1024 * 1024)) 
						{ // Image size max 1 MB
							$actual_image_name = time().$i. "." . $ext;
							$img =  'assets/frontend/img/clublogos/' . $actual_image_name;
							$tmp = $_FILES['images']['tmp_name'][$i];
							if (move_uploaded_file($tmp,$img)) 
							{
								$data=array(
								'club_id'=>$lastid,
								'images'=>$img,
								);
								
								
								if($this->db->insert('slider_images',$data))
								{
									//echo $img;
                                     /*$data = array(
                                         'status_code' => 1,
                                         'status' => 'Successful',
                                     );
                                     echo json_encode($data);*/
								}
							}  else {
							 $data = array(
                                         'status_code' => 0,
                                         'status' => 'Failed',
                                     );
                                     echo json_encode($data);
						}
                        } else {
							 $data = array(
                                         'status_code' => 4,
                                         'status' => 'Image is bigger than 1MB',
                                     );
                                     echo json_encode($data);
					}
            
				} else {
                                    $data = array(
                                         'status_code' => 5,
                                         'status' => 'Invalid Image Format',
                                     );
                                     echo json_encode($data);
			} 
                } else {
                                    $data = array(
                                         'status_code' => 6,
                                         'status' => 'Please Select Image',
                                     );
                                     echo json_encode($data);
		}
		
	 }
	 }
	 $this->session->set_userdata('lastid', $lastid);
         $data = array(
            'status_code' => 1,
             'status' => 'Successful',
         );
         echo json_encode($data);
	  }
     }
     }
 }
function delete_event($eid)
{
	$r=null;
	$r = $this->mdl_leader->deleteevent($eid);
	if($r)
	{
		//echo "<script>alert('redirect');</script>";
		echo "<script> swal({
                                html: parsed.status,
                                type: 'success'
                        })</script>";
		redirect('leader/leader_home','refresh');		
	}
	else
	{ 
		echo "<script> swal({
                                html: parsed.status,
                                type: 'error',
                            });</script>";
	}
	
}
 function create_group()
 {
	$this->load->view('vw_header2');
    $this->load->view('vw_create_group');
    $this->load->view('vw_footer');
 }
 function edit_group($gid,$uniid)
 {
	$data['groupdata']= $gid;
	$data['unidata']= $uniid;
	$data['members'] = $this->mdl_leader->members($gid);
	$this->load->view('vw_header2');
    $this->load->view('vw_edit_grp_frm',$data);
    $this->load->view('vw_footer');
 }
 
 function group_submit()
 {
	 //echo json_encode($_POST);
	 //echo json_encode($_FILES);
	 
	$username = $this->session->userdata['logged_in']['username'];
	$uid      = $this->mdl_leader->getuserid($username);
	$this->form_validation->set_rules('c_uni', 'School Name', 'trim|required');
	$this->form_validation->set_rules('c_name', 'Group Name', 'trim|required');
	$this->form_validation->set_rules('c_desc', 'Group Description', 'trim|required|max_length[200]');
	
	 /*if (empty($_FILES['logo']['name'])) {                        //check file is selected
            $this->form_validation->set_rules('logo','Image','required');
        }*/
    $data = null;
	if($this->form_validation->run() == false)
	{
		 $data = array(
			'st'	  => 'fail',
			'st_code' => 1, /*1- validation errors*/
			'c_uni'   => form_error('c_uni'),
			'c_name'  => form_error('c_name'),
			'c_desc'  => form_error('c_desc'),
			);
		  echo json_encode($data);
	}
	else {
	$filename = "default_logo.png";	 
	$path = "assets/frontend/img/clublogos/".$filename;	
		/*if no img uploaded, this wil be def value to pass in array, so tat ders no err msg
		also, default.jpg file always exits in image folder */
		/* upload banner code*/    
    
		$types = array('image/jpeg', 'image/gif','image/png','image/jpg');
            
        
        if (!in_array($_FILES['logo']['type'], $types))
        {
            
           $ret = array('st'=> 'fail','st_code' => 3);//3 - file type error
            echo json_encode($ret);
        }
      
       
            $sourcePath   = $_FILES['logo']['tmp_name'];
            $filename     =  rand(0,10000).$_FILES['logo']['name'];
            $targetPath   =  "assets/frontend/img/clublogos/".$filename;
            move_uploaded_file($sourcePath, $targetPath);
			
		//}
	$data = array(
		'c_uni'    => $this->input->post('c_uni'),
		'c_name'   => $this->input->post('c_name'),
		'c_desc'   => $this->input->post('c_desc'),
		'c_by'     => $uid,
		'c_logo'   => $targetPath,
		'created'  => date('Y-m-d'),
			);
	$insertedclubid = $this->mdl_leader->add_club($data);
	$data2 = array(
		'gstudid' =>$uid,
		'gclubid' => $insertedclubid,
		'type' 	  => 2,
		'created' => date('Y-m-d'),
				);
	$this->mdl_leader->join_club($data2);
	$return_json = array('st'=> 'success','st_code' => 2);//2 - success
	echo json_encode($return_json);
	
	}
 }
 function editgroup_submit()
 {
	 //echo json_encode(array('data' => $_POST,'Files' => $_FILES));
	$username = $this->session->userdata['logged_in']['username'];
	$uid= $this->mdl_leader->getuserid($username);
	$this->form_validation->set_rules('c_uni', 'School Name', 'trim|required');
	$this->form_validation->set_rules('c_name', 'Group Name', 'trim|required');
	$this->form_validation->set_rules('c_desc', 'Group Description', 'trim|required|max_length[200]');
	/* if(empty($_FILES['logo']['name']))	{
		$this->form_validation->set_rules('logo', 'Logo Error', 'required');
	} */
	$data = null;
	$types = array('image/jpeg', 'image/gif','image/png','image/jpg');
	/*  if (!in_array($_FILES['logo']['type'], $types))
        {
            //echo "<script>alert('File type not allowed')</script>";
           $ret = array(
					'st'=> 'fail',
					'st_code' => 3
					);//3 - file type error
            echo json_encode($ret);
        } */
	
	
	if($this->form_validation->run() == false)
	{
		 $data = array(
			'st'	  => 'fail',
			'st_code' => 1, /*1- validation errors*/
			'c_uni'   => form_error('c_uni'),
			'c_name'  => form_error('c_name'),
			'c_desc'  => form_error('c_desc'),
			'form_error' => validation_errors()
			);
		  echo json_encode($data);
	}
	else {
		$targetPath = $this->input->post('logo');
		//check if logo is changed
		if(!empty($_FILES['logo']['name'])){	
				$sourcePath   = $_FILES['logo']['tmp_name'];
				$filename     =  rand(0,10000).$_FILES['logo']['name'];
				$targetPath   =  "assets/frontend/img/clublogos/".$filename;
				move_uploaded_file($sourcePath, $targetPath);
				$data = array(
					'c_uni'    => $this->input->post('c_uni'),
					'c_name'   => $this->input->post('c_name'),
					'c_desc'   => $this->input->post('c_desc'),
					'c_by'     => $uid,
					'c_logo'   => $targetPath,
					'modified' => date('Y-m-d'),
						);
			}
			else
			{
				$data = array(
					'c_uni'    => $this->input->post('c_uni'),
					'c_name'   => $this->input->post('c_name'),
					'c_desc'   => $this->input->post('c_desc'),
					'c_by'     => $uid,
					//'c_logo'   => $this->input->post('logo1'),
					'modified' => date('Y-m-d'),
						);
			}
	$cid = $this->input->post('groupid');
	$this->mdl_leader->edit_club($cid,$data);	
	
   // print_r (edit_group($gid,$data2));
	$return_json = array('st'=> 'success');
	echo json_encode($return_json);
	
	} 
 }
 function join_group()
 {
	$username = $this->session->userdata['logged_in']['username'];
	$uid=$this->mdl_leader->getuserid($username);
	$data['allgrps']=$this->mdl_leader->getallgroups();
	$data['uid']=$uid;
	
	$this->load->view('vw_header2');
    $this->load->view('vw_other_groups',$data);
    $this->load->view('vw_footer');
 }
 function join_newgroup($clubid)
 {
	$username = $this->session->userdata['logged_in']['username'];
	$uid=$this->mdl_leader->getuserid($username);
	$data5=array(
		'gstudid' =>$uid,
		'gclubid' => $clubid,
		'type'	  => 1,
		'created' => date('Y-m-d'),
	);
	$this->mdl_leader->join_club($data5);
	redirect('leader/join_group','refresh');	 
 }
 
 function delete_group($id)
 {
	$g=null;
	$g = $this->mdl_leader->deletegrp($id);
	if($g)
	{
		echo "<script> swal({
                                html: parsed.status,
                                type: 'success'
                        })</script>";
		redirect('leader/my_groups','refresh');
		
	}
	else
	{ 
		echo "<script> swal({
                                html: parsed.status,
                                type: 'error',
                            });</script>";
	} 
 }
 
 function publish_event($eid)
 {
	$p = $this->mdl_leader->publish_e($eid);
	redirect('leader/leader_home','refresh');
	 
	 
 }
  function unpublish_event($eid)
 {
	$p = $this->mdl_leader->unpublish_e($eid);
	redirect('leader/leader_home','refresh');
 }
 
 function search()
 {
	$username = null;
	if(isset($this->session->userdata['logged_in'])) 
	{
		$username = ($this->session->userdata['logged_in']['username']);
		$email    = ($this->session->userdata['logged_in']['email']);
	}
	$keyword  = $this->input->post('txtsearch');
	$keyword1 = "is";
	$result['events']   = $this->mdl_leader->getsearchdata($keyword,$username);
	 
	$this->load->view('vw_header2');
    $this->load->view('vw_leader_search',$result);
    //$this->load->view('vw_student_leader_localhost');
    $this->load->view('vw_footer');
	
	//$data['events']=$this->mdl_leader->getevents($username);
 }
 
 function search1()
 {
	 $keyword = "is";
        $sql = "(SELECT e_id, e_name, e_desc,'event' as type FROM event_table 
        WHERE e_name LIKE '%".$keyword."%' OR e_desc LIKE '%".$keyword."%')
        UNION
        (SELECT c_id,c_name, c_desc,'club' as type FROM club_table WHERE c_name LIKE '%" . 
           $keyword . "%' OR c_desc LIKE '%" . $keyword ."%')";

    //mysql_query($query);
    $query1= $this->db->query($sql);
    
        foreach($query1->result() as $row)
        {
            echo "<pre>";
            print_r($row);
            echo "</pre>";
            echo "////".$row->type;
        }
        echo "<br>single: ".$query1->row()->type;
 }
 
function add_groupadmin($gid,$groupid,$uniid)
 {
	$query =$this->mdl_leader->make_grp_admin($gid);
	$this->edit_group($groupid,$uniid);
	 
 }
 
 function remove_member($gid,$groupid,$uniid)
 {
	$query =$this->mdl_leader->remove_grp_member($gid);
	$this->edit_group($groupid,$uniid);
 }

}
?>