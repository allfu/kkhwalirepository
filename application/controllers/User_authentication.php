<?php
Class User_Authentication extends CI_Controller {

public function __construct() {
parent::__construct();
 $this->clear_cache();

// Load form helper library
$this->load->helper('form');

// Load form validation library
$this->load->library('form_validation');

// Load session library
$this->load->library('session');

// Load database
$this->load->model('login_database');


}
 function clear_cache()
    {
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
    }

// Show login page
public function index() {
//$this->load->view('login_form');

$this->load->view('vw_student_login');
}

// Show registration page
public function user_registration() 
{
	$data['res'] = $this->login_database->getRecords();
	$this->load->view('leaderviews/registration_form',$data);
}

// Validate and store registration data in database
public function new_user_registration()
{
    // Check validation for user input in SignUp form
    $this->form_validation->set_rules('user_name', 'Username', 'trim|required');
    $this->form_validation->set_rules('first_name', 'Firstname', 'trim|required');
    $this->form_validation->set_rules('last_name', 'Lastname', 'trim|required');
    $this->form_validation->set_rules('contact', 'contact No', 'trim|required');
    $this->form_validation->set_rules('email', 'Email', 'trim|required|callback_validate_email');
    $this->form_validation->set_rules('website', 'Website', 'trim|required');
    $this->form_validation->set_rules('cca[]', 'CCA', 'trim|required');
    $this->form_validation->set_rules('pass', 'Password', 'trim|required|min_length[6]');
    $this->form_validation->set_rules('re_pass', 'Confirm Password', 'required|trim|matches[pass]');
    $this->form_validation->set_rules('school', 'School Information', 'required|trim');
    $this->form_validation->set_rules('yr', 'Year of Study', 'required|trim');
	$this->form_validation->set_rules('matno', 'Matriculation No', 'required|trim');
    
    if ($this->form_validation->run() == FALSE) 
    {
         $data = array(
                'error'		    => 'fail',
                'user_name'     => form_error('user_name'),
                'first_name'    => form_error('first_name'),
                'last_name'     => form_error('last_name'),
                'contact'       => form_error('contact'),
                'email'         => form_error('email'),
                'website'       => form_error('website'),
                'cca'       	=> form_error('cca[]'),
                'pass'          => form_error('pass'),
                're_pass'       => form_error('re_pass'),
                'school'        => form_error('school'),
                'yr'            => form_error('yr'),
				'matno'         => form_error('matno'),
                );
		      echo json_encode($data);
        //$this->load->view('registration_form');
    } 
    else 
    {
        $data = array(
            'user_name'     => $this->input->post('user_name'),
            'firstname'     => $this->input->post('first_name'),
            'lastname'      => $this->input->post('last_name'),
            'user_email'    => $this->input->post('email'),
            'user_password' => $this->input->post('pass'),
            'contact_no'    => $this->input->post('contact'),
			//'group_cca'		=> " ",/*$this->input->post('cca'),*/
            'website'       => $this->input->post('website'),
            'school'        => $this->input->post('school'),
            'yr'          	=> $this->input->post('yr'),
			'type'			=> 2,
        );
        $result = $this->login_database->registration_insert($data);
		$groups = $this->input->post('cca[]');
		
		
		$club=array();
		foreach($groups as $row) {
			$club= array(
			'gstudid' => $result ,
			'gclubid' => $row,
			'type' 	  =>1,
			'created' => date('Y-m-d H:i:s'),
						);
		$this->login_database->registergroups($club);
			}
        $msg=null;
        if (isset($result))
        { $msg = true/*"Registration successful"*/;}
        else 
        { $msg= false/*"Username already exists!"*/; }
        $return_json = array('error'=> 'success','msg'=>$msg, 'clubs'=>$result);
        echo json_encode($return_json);
        /*
        if ($result == TRUE) {
        $data['message_display'] = 'Registration Successful !';
        $this->load->view('login_form', $data);
        } else {
        $data['message_display'] = 'Username already exist!';
        $this->load->view('registration_form', $data);
        }*/
        
    }
}
    function validate_email($email) 
   {
	   if(! preg_match("/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/", $email))	
	   {
		   $this->form_validation->set_message('validate_email', 'Invalid Email.');			
		   return FALSE;		
	   }else{
		   return TRUE;
		   }	
	}

// Check for user login process
function user_login() {

$this->form_validation->set_rules('username', 'Username', 'trim|required');
$this->form_validation->set_rules('password', 'Password', 'trim|required');

if ($this->form_validation->run() == FALSE) {
if(isset($this->session->userdata['logged_in'])){
/*$this->load->view('admin_page');*/
redirect('students/student_home','refresh');
}else{
$this->load->view('login_form');
}
} else {
$data = array(
'username' => $this->input->post('username'),
'password' => $this->input->post('password'),
'type'=> 2,
);
$result = $this->login_database->login($data);
if ($result == TRUE) {

$username = $this->input->post('username');
$result = $this->login_database->read_user_information($username);
if ($result != false) {
$session_data = array(
'username' => $result[0]->user_name,
'email' => $result[0]->user_email,
'sessiontype' => 2,
);
// Add user data in session
$this->session->set_userdata('logged_in', $session_data);
/*$this->load->view('admin_page');*/
//redirect('students','');
redirect('leader/leader_home');

}
} else {
$data = array(
'error_message' => 'Invalid Username or Password'
);
$this->load->view('login_form', $data);
}
}
}

// Logout from admin page
public function logout() {

// Removing session data
$sess_array = array(
'username' => ''
);
$this->session->unset_userdata('logged_in', $sess_array);
$data['message_display'] = 'Successfully Logout';
$this->load->view('login_form', $data);
}
function stud_reg()
{
    //echo json_encode($_POST);
    //$this->form_validation->set_rules('phone_number', 'Phone no', 'trim|required|numeric');
    $this->form_validation->set_rules('matriculation_number', 'Matriculation no', 'trim|required');
    $this->form_validation->set_rules('university', 'University name', 'trim|required');
    $this->form_validation->set_rules('faculty', 'Faculty name', 'trim|required');
    $this->form_validation->set_rules('hall_name', 'Hall name', 'trim|required');
    $this->form_validation->set_rules('cca_name', 'CCA name', 'trim|required');
    //$this->form_validation->set_rules('student_accommodation', 'School Accomodation', 'required|trim');
   // $this->form_validation->set_rules('school_email', 'School email', 'required|trim');


    $data = null;
    if($this->form_validation->run() == false)
    {
         $data = array(
            'st'		            => 'fail',
            //'phone_number'          => form_error('phone_number'),
            'matriculation_number'  => form_error('matriculation_number'),
            'university'             => form_error('university'),
            'faculty'               => form_error('faculty'),
            'hall_name'              => form_error('hall_name'),
            'cca_name'                => form_error('cca_name'),
            //'student_accommodation'   => form_error('student_accommodation'),
           // 'school_email'            => form_error('school_email'),

               );
          echo json_encode($data);
    }
    else {
    $data = array(
        //*send fb values from view to db/model*/
        //'contact_no'  => $this->input->post('phone_number'),
        'matno'  => $this->input->post('matriculation_number'),
        'school'  => $this->input->post('university'),
        'faculty'  => $this->input->post('faculty'),
        'hall_name'  => $this->input->post('hall_name'),
        'group_cca'  => $this->input->post('cca_name'),
        //'accomodation'  => $this->input->post('student_accommodation'),
        //'school_email'  => $this->input->post('school_email'),
		'user_email'  => $this->input->post('school_email'),
		'user_name' => $this->input->post('user_name'),
		'firstname' => $this->input->post('firstname'),
		'lastname' => $this->input->post('lastname'),
		'fb_pic' => $this->input->post('fb_pic'),
		'fb_id' => $this->input->post('fb_id'),
        //'created'  => date('Y-m-d'),
        'type' => 1,
            );
			$this->load->model('mdl_student_reg');
    $i = $this->mdl_student_reg->add_student($data);
	if($i){
    $return_json = array('st'=> 'success','i'=>$data);
    echo json_encode($return_json);
	}
	


    }
    
}
function leader()
{
    $this->load->view('registration_form');
        
}

}

?>