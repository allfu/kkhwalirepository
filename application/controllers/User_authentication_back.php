<?php
Class User_Authentication extends CI_Controller {

public function __construct() {
parent::__construct();

// Load form helper library
$this->load->helper('form');

// Load form validation library
$this->load->library('form_validation');

// Load session library
$this->load->library('session');

// Load database
$this->load->model('login_database');
}

// Show login page
public function index() {
$this->load->view('login_form');
}

// Show registration page
public function user_registration() {
$this->load->view('registration_form');
}

// Validate and store registration data in database
public function new_user_registration()
{
    // Check validation for user input in SignUp form
    $this->form_validation->set_rules('user_name', 'Username', 'trim|required');
    $this->form_validation->set_rules('first_name', 'Firstname', 'trim|required');
    $this->form_validation->set_rules('last_name', 'Lastname', 'trim|required');
    $this->form_validation->set_rules('contact', 'contact No', 'trim|required');
    $this->form_validation->set_rules('email', 'Email', 'trim|required|callback_validate_email');
    $this->form_validation->set_rules('website', 'Website', 'trim|required');
    $this->form_validation->set_rules('pass', 'Password', 'trim|required|min_length[6]');
    $this->form_validation->set_rules('re_pass', 'Confirm Password', 'required|trim|matches[pass]');
    $this->form_validation->set_rules('school', 'School Information', 'required|trim');
    $this->form_validation->set_rules('yr', 'Year of Study', 'required|trim');
    
    if ($this->form_validation->run() == FALSE) 
    {
         $data = array(
                'error'		    => 'fail',
                'user_name'     => form_error('user_name'),
                'first_name'    => form_error('first_name'),
                'last_name'     => form_error('last_name'),
                'contact'       => form_error('contact'),
                'email'         => form_error('email'),
                'website'       => form_error('website'),
                'pass'          => form_error('pass'),
                're_pass'       => form_error('re_pass'),
                'school'        => form_error('school'),
                'yr'            => form_error('yr'),
                );
		      echo json_encode($data);
        //$this->load->view('registration_form');
    } 
    else 
    {
        $data = array(
            'user_name'     => $this->input->post('user_name'),
            'firstname'     => $this->input->post('first_name'),
            'lastname'      => $this->input->post('last_name'),
            'user_email'    => $this->input->post('email'),
            'user_password' => $this->input->post('pass'),
            'contact_no'    => $this->input->post('contact'),
            'website'       => $this->input->post('website'),
            'school'        => $this->input->post('school'),
            'yr'          => $this->input->post('yr'),
        );
        $result = $this->login_database->registration_insert($data);
        $msg=null;
        if ($result == TRUE) 
        { $msg = true/*"Registration successful"*/;}
        else 
        { $msg= false/*"Username already exists!"*/; }
        $return_json = array('error'=> 'success','msg'=>$msg);
        echo json_encode($return_json);
        /*
        if ($result == TRUE) {
        $data['message_display'] = 'Registration Successful !';
        $this->load->view('login_form', $data);
        } else {
        $data['message_display'] = 'Username already exist!';
        $this->load->view('registration_form', $data);
        }*/
        
    }
}
    function validate_email($email) 
   {
	   if(! preg_match("/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/", $email))	
	   {
		   $this->form_validation->set_message('validate_email', 'Invalid Email.');			
		   return FALSE;		
	   }else{
		   return TRUE;
		   }	
	}

// Check for user login process
public function user_login() {

$this->form_validation->set_rules('username', 'Username', 'trim|required');
$this->form_validation->set_rules('password', 'Password', 'trim|required');

if ($this->form_validation->run() == FALSE) {
if(isset($this->session->userdata['logged_in'])){
/*$this->load->view('admin_page');*/
redirect('students','refresh');
}else{
$this->load->view('login_form');
}
} else {
$data = array(
'username' => $this->input->post('username'),
'password' => $this->input->post('password')
);
$result = $this->login_database->login($data);
if ($result == TRUE) {

$username = $this->input->post('username');
$result = $this->login_database->read_user_information($username);
if ($result != false) {
$session_data = array(
'username' => $result[0]->user_name,
'email' => $result[0]->user_email,
);
// Add user data in session
$this->session->set_userdata('logged_in', $session_data);
/*$this->load->view('admin_page');*/
redirect('students','refresh');
}
} else {
$data = array(
'error_message' => 'Invalid Username or Password'
);
$this->load->view('login_form', $data);
}
}
}

// Logout from admin page
public function logout() {

// Removing session data
$sess_array = array(
'username' => ''
);
$this->session->unset_userdata('logged_in', $sess_array);
$data['message_display'] = 'Successfully Logout';
$this->load->view('login_form', $data);
}

function error()
{
	$this->load->view('vw_admin401');

}
}

?>